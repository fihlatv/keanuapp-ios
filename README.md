# Keanu

[![Version](https://img.shields.io/cocoapods/v/Keanu.svg?style=flat)](https://cocoapods.org/pods/Keanu)
[![License](https://img.shields.io/cocoapods/l/Keanu.svg?style=flat)](https://cocoapods.org/pods/Keanu)
[![Platform](https://img.shields.io/cocoapods/p/Keanu.svg?style=flat)](https://cocoapods.org/pods/Keanu)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Keanu is available through [CocoaPods](https://cocoapods.org). To install
it, add the following to your Podfile:

```ruby
target 'YourApp' do
    pod 'Keanu'
end

target 'YourShareExtension' do
    pod 'KeanuExtension'
end
```

## Usage

You can use Keanu as a base for your own Matrix client.
Keanu in itself is a complete client, though white-labeled. It's mostly black-on-white and doesn't
really mention itself anywhere.

Beware, though, that it's quite opinionated. E.g. it will always try to switch on encryption in
newly created rooms.

To get an idea how to bootstrap Keanu, please consult the Example, which actually is a fully
working client but actually contains just a few files needed for configuration and bootstrapping.


## Environment

The environment you'll obtain if you inherit Keanu as a dependency is rather large. It's very 
opinionated and contains a lot of stuff.

If you're just interested in a few parts, the suggestion is, to copy stuff into your own project,
rather than having Keanu as a dependency.

You may create an issue in our bugtracker, or better send a pull request at https://gitlab.com/keanuapp/keanuapp-ios, 
if you want to have certain parts detached from the rest in e.g. a Subspec. 
(The `FriendsManager` may be a good candidate for that.)

Here is a (maybe outdated, non-complete) list of things you inherit, should you decide to depend on Keanu:

- CocoaPods from KeanuCore (stuff which is shared between the main app and the share extension):
  - MatrixSDK
  - MatrixSDK/SwiftSupport
  - MatrixKit
  - CrossroadRegex
  - Localize
  - FontBlaster
  
- Fonts in KeanuCore: 
  - icomoon
  - MaterialIcons-Regular
  
- CocoaPods from Keanu (the main app):
  - Eureka
  - PureLayout
  - QRCode
  - BarcodeScanner
  - MaterialComponents/Snackbar
  - libPhoneNumber-iOS
  - INSPhotoGallery
  - FLAnimatedImage
  - SearchTextField
  
- CocoaPods from KeanuExtension (the share extension):
  - MBProgressHUD

Interresting Matrix specfic classes to look at and maybe borrow when Keanu is too big for you:
- `Config`
- `FriendsManager`
- `BaseAppDelegate`
- `PushManager`
- `UrlHandler`
- `RoomManager`

Domain-independent tools:
- `UIView+Keanu` (Animated show/hide of views leveraging height constraints to give up
  UI real estate on hide.)
- `AlertHelper` (Reduces a lot of boilerplate regarding alerts.)
- `WorkingOverlay` (A UIView overlay to show while waiting on network response or similar 
  to block users from touching anything)
- `Formatters` (A selection of date and number formatters.)
- `AvatarGenerator`
- `Scrubber` (Cleans the user storage. App will be like freshly installed.)
- `UtiHelper` (Convert from Apple's UTI to MIME type.)


## Distinctions

Why did we reinvent the wheel? What makes Keanu different from Riot?

- Fully written in Swift 4.2.
- We're mostly not using MatrixKit (which is the white-labeled part of Riot), besides the 
  `MXKAccountManager` which works very well and takes a huge burden from us.
- Better friends handling: There's a `FriendsManager` and a `Friends` object and some 
  `MXUser` and `MXKAccount` extensions. The `FriendsManager` keeps a list of friends stored
  as a serialized object in a plist file. So, when you close a room with a friend, you're not losing
  the contact details. Users can also rename their friends (locally only!) to be able to identify
  opaque Matrix ID's more easily.
- Tries to be less annoying with constantly added new devices/encryption keys of your chat 
  contacts. More trust-on-first-sight, Signal-style, less getting in the users way.
- More complete white-label solution: Tries to keep as much as possible into the pods, so
  the actual inheriting app can be as slim as possible.

## Overriding

- You can override text strings by leveraging the localization mechanism. See the example app's
  `AppDelegate` on how to inject usage of your own app bundle's localizations.

- You can override all storyboards by just providing your own with the same name in your app 
  bundle. Keanu should pick these up automatically, if available.
  
- Override view controllers: Possible through overriding the storyboards, as most view controllers
are instantiated from there. Also, check out the `Theme` protocol for more info (you can return your own theme from the `AppDelegate`).
  
- Override chat bubble creation: Check out `Keanu/Room/RoomDataSource` to override/augment
  chat bubble processing.
  
- Overriding other stuff: Your milage my vary. If you discover something, you'd like to override,
  which currently cannot be done efficiently, we're happy to accept pull requests or issue tracker
  reports at https://gitlab.com/keanuapp/keanuapp-ios !

## Author

Guardian Project

## License

Keanu is available under the Apache License, Version 2.0. See the LICENSE file for more info.
