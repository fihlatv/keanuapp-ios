//
//  AppDelegate.swift
//  Keanu
//
//  Created by Benjamin Erhart on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import UserNotifications
import KeanuCore
import Keanu

@UIApplicationMain
class AppDelegate: BaseAppDelegate {

    override func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

// For debugging purposes: These are the folders, where all the stuff is stored.
// You can have a look at it, while running in the simulator!
//
//        let fm = FileManager.default
//
//        if let appGroupId = Config.appGroupId,
//            let container = fm.containerURL(forSecurityApplicationGroupIdentifier: appGroupId) {
//            print(container)
//        }
//        print(fm.urls(for: .libraryDirectory, in: .userDomainMask))

        setUp()

        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    override func application(_ application: UIApplication, performFetchWithCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        setUp()

        // App entry point. Setup needs to be done.
        super.application(application, performFetchWithCompletionHandler: completionHandler)
    }

    override func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable : Any], fetchCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        setUp()

        super.application(application, didReceiveRemoteNotification: userInfo,
                          fetchCompletionHandler: completionHandler)
    }

    /**
     Inject configuration and set up localization.
    */
    private func setUp() {
        KeanuCore.setUp(with: Config.self)

        // We use the provided localization by the Pod. Use your own bundle here,
        // (e.g. Bundle.main - the default arg), if you want to inject your own localization.
        KeanuCore.setUpLocalization(fileName: "Localizable", bundle: Bundle(for: BaseAppDelegate.self))
        
        StickerManager.stickersFolderPath = Bundle.main.resourcePath! + "/Stickers"
    }
}
