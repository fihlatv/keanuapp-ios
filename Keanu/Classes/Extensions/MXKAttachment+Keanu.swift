//
//  MXKAttachment+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 08.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore

extension MXKAttachment {
    
    /**
     Shortcut for checking if the attachment has been downloaded (i.e. a file exists at the cache path)
     */
    public var isDownloaded: Bool {
        return cacheFilePath != nil && FileManager.default.fileExists(atPath: cacheFilePath!)
    }
    
    public func downloadData(success:@escaping (Data?)->(), failure:@escaping (Error?)->(), progress:@escaping (Int)->()) -> MXMediaLoader? {
        
        var observer:NSObjectProtocol?
        var successCalled:Bool = false
        
        self.getData({ (data) in
            if let observer = observer {
                NotificationCenter.default.removeObserver(observer)
            }
            successCalled = true
            progress(100)
            success(data)
        }) { (error) in
            // Hmm, MXMediaLoader.dealloc will call MXMediaLoader.cancel which will call MXMediaLoader.setState which will post a notification which will call handler in MXKAttachment which will call the failure handler here! So don't do anything if we already are in success state...
            guard !successCalled else {return}
            
            if let observer = observer {
                NotificationCenter.default.removeObserver(observer)
            }
            progress(0)
            failure(error)
        }
        self.getData(success, failure: failure)
        let loader = MXMediaManager.existingDownloader(withIdentifier: downloadId)
        if loader != nil {
            observer = NotificationCenter.default.addObserver(forName: Notification.Name.mxMediaLoaderStateDidChange, object: loader, queue: OperationQueue.main, using: { (notification) in
                guard let loader = notification.object as? MXMediaLoader else { return }
                if let statisticsDict = loader.statisticsDict,
                    let current = statisticsDict[kMXMediaLoaderCompletedBytesCountKey] as? Float,
                    let total = statisticsDict[kMXMediaLoaderTotalBytesCountKey] as? Float {
                    let percentage = Int((100.0 * current) / total)
                    progress(percentage)
                }
            })
        }
        return loader
    }
    
    public func getMediaLocalFile(success:@escaping (String?, Bool)->(), failure:@escaping (Error?)->(), progress:@escaping (Int)->()) {
        
        var observer:NSObjectProtocol?
        var successCalled:Bool = false

        if isEncrypted {
            decrypt(toTempFile: { (file) in
                if let observer = observer {
                    NotificationCenter.default.removeObserver(observer)
                }
                successCalled = true
                progress(100)
                success(file, true)
            }) { (error:Error?) in
                // Hmm, MXMediaLoader.dealloc will call MXMediaLoader.cancel which will call MXMediaLoader.setState which will post a notification which will call handler in MXKAttachment which will call the failure handler here! So don't do anything if we already are in success state...
                guard !successCalled else {return}
                
                if let observer = observer {
                    NotificationCenter.default.removeObserver(observer)
                }
                progress(0)
                failure(error)
            }
        } else {
            prepare({
                if let observer = observer {
                    NotificationCenter.default.removeObserver(observer)
                }
                successCalled = true
                progress(100)
                success(self.cacheFilePath, false)
            }) { (error) in
                // Hmm, MXMediaLoader.dealloc will call MXMediaLoader.cancel which will call MXMediaLoader.setState which will post a notification which will call handler in MXKAttachment which will call the failure handler here! So don't do anything if we already are in success state...
                guard !successCalled else {return}
                
                if let observer = observer {
                    NotificationCenter.default.removeObserver(observer)
                }
                progress(0)
                failure(error)
            }
        }
        let loader = MXMediaManager.existingDownloader(withIdentifier: downloadId)
        if loader != nil {
            observer = NotificationCenter.default.addObserver(forName: Notification.Name.mxMediaLoaderStateDidChange, object: loader, queue: OperationQueue.main, using: { (notification) in
                guard let loader = notification.object as? MXMediaLoader else { return }
                if let statisticsDict = loader.statisticsDict,
                    let current = statisticsDict[kMXMediaLoaderCompletedBytesCountKey] as? Float,
                    let total = statisticsDict[kMXMediaLoaderTotalBytesCountKey] as? Float {
                    let percentage = Int((100.0 * current) / total)
                    progress(percentage)
                }
            })
        }
    }

}
