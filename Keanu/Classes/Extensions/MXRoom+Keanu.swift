//
//  MXRoom+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 25.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

extension MXRoom {

    /**
     Return my user in the room.
     */
    public var myUser: MXMyUser? {
        get {
            return mxSession?.myUser
        }
    }
    
    /**
     Return my power level in the room.
     */
    public var myPowerLevel: Int {
        get {
            return powerLevel(myUser?.userId)
        }
    }
    
    /**
     Check if the given user has admin rights in the room.
     */
    public func isAdmin(userId: String) -> Bool {
        return powerLevel(userId) >= 100
    }
    
    /**
     Check if the given user has moderator rights in the room.
     */
    public func isModerator(userId: String) -> Bool {
        return powerLevel(userId) >= 50
    }
    
    /**
     Private helper to check if the logged in user has enough power to send an
     event of the given type.

     - parameter type: The type of event.
     - returns: true if the user can send events of type 'type'.
     */
    private func canSendEvent(ofType type: String) -> Bool {
        return myPowerLevel >= powerLevels?
            .__minimumPowerLevelForSendingEvent(asStateEvent: type) ?? Int.max
    }
    
    /**
     Check if the logged in user has enough power to change the room name.

     - returns: true if the user can modify the room name.
     */
    public func canModifyName() -> Bool {
        return canSendEvent(ofType: kMXEventTypeStringRoomName)
    }

    /**
     Check if the logged in user has enough power to change the room topic.

     - returns: true if the user can modify the room topic.
     */
    public func canModifyTopic() -> Bool {
        return canSendEvent(ofType: kMXEventTypeStringRoomTopic)
    }

    /**
     Check if the logged in user has enough power to enable encryption for a room.

     - returns: true if the user can enable encryption for the room.
     */
    public func canEnableEncryption() -> Bool {
        return canSendEvent(ofType: kMXEventTypeStringRoomEncryption)
    }
    
    /**
     Check if the logged in user has enough power to invite someone to the room.

     - returns: true if the user can invite others to the room.
     */
    public func canSendInvites() -> Bool {
        return myPowerLevel >= powerLevels?.invite ?? Int.max
    }
    
    /**
     Check if the logged in user has enough power to ban a given user from the room.

     - parameter userId: The user id to ban.
     - returns: true if the logged in user can ban the given user from the room.
     */
    public func canBanUser(userId: String) -> Bool {
        let myPowerLevel = self.myPowerLevel

        return myPowerLevel >= powerLevels?.ban ?? Int.max
            && myPowerLevel > powerLevel(userId)
    }
    
    /**
     Check if the logged in user has enough power to kick a given user from the room.

     - parameter userId: The user id to kick.
     - returns: true if the logged in user can kick the given user from the room.
     */
    public func canKickUser(userId: String) -> Bool {
        let myPowerLevel = self.myPowerLevel

        return myPowerLevel >= powerLevels?.kick ?? Int.max
            && myPowerLevel > powerLevel(userId)
    }
    
    /**
     Check if the logged in user has enough power to make a given user moderator for the room.

     - parameter userId: The user id to promote.
     - returns: true if the logged in user can make the given user a moderator for
        the room AND the given user is not already a moderator or admin.
     */
    public func canMakeUserModerator(userId: String) -> Bool {
        let myPowerLevel = self.myPowerLevel

        return myPowerLevel >= 50
            && myPowerLevel > powerLevel(userId)
    }
    
    /**
     Check if the logged in user has enough power to make a given user admin for the room.

     - parameter userId: The user id to promote.
     - returns: true if the logged in user can make the given user admin for the
        room AND the given user is not already an admin.
     */
    public func canMakeUserAdmin(userId: String) -> Bool {
        let myPowerLevel = self.myPowerLevel

        return myPowerLevel >= 100
            && myPowerLevel > powerLevel(userId)
    }

    /**
     Check if the logged in user has enough power to revoke moderator power for
     a given user in the room.

     - parameter userId: The user id to demote.
     - returns: true if the logged in user can revoke moderator power for the
        given user AND the given user is a moderator.
     */
    public func canUnmakeUserModerator(userId: String) -> Bool {
        let myPowerLevel = self.myPowerLevel
        let userPowerLevel = powerLevel(userId)
        return myPowerLevel > 50
            && userPowerLevel == 50
    }

    public func canEditCanonicalAlias() -> Bool {
        return canSendEvent(ofType: kMXEventTypeStringRoomCanonicalAlias)
    }

    /**
     Shortcut for `dangerousSyncState?.powerLevels`
    */
    fileprivate var powerLevels: MXRoomPowerLevels? {
        get {
            return dangerousSyncState?.powerLevels
        }
    }

    /**
     Look up the power level of a given user.

     - parameter userId: The Matrix ID.
     - returns: the power level of a given user or `Int.min`, if none found.
    */
    fileprivate func powerLevel(_ userId: String?) -> Int {
        if let userId = userId {
            return powerLevels?.powerLevelOfUser(withUserID: userId) ?? Int.min
        }

        return Int.min
    }
    
    //MARK: Notification settings
    public enum NotificationLevel {
        case None
        case Mentions
        case All
    }
    
    private func getPushRule() -> MXPushRule? {
        if let rules = self.mxSession?.notificationCenter?.rules?.global?.room as? [MXPushRule] {
            return rules.first { $0.ruleId == self.roomId }
        }
        return nil
    }
    
    private func getOverridePushRule() -> MXPushRule? {
        if let rules = self.mxSession?.notificationCenter?.rules?.global?.override as? [MXPushRule] {
            // the rule id is the room Id
            return rules.first { $0.ruleId == self.roomId }
        }
        return nil
    }
    
    public var notificationLevel: NotificationLevel {
        get {
            // Check whether an override rule has been defined with the roomm id as rule id.
            // This kind of rule is created to mute the room
            if let overrideRule = getOverridePushRule() {
                for ruleAction in overrideRule.actions as? [MXPushRuleAction] ?? [] {
                    if ruleAction.actionType == MXPushRuleActionTypeDontNotify {
                        for ruleCondition in overrideRule.conditions as? [MXPushRuleCondition] ?? [] {
                            if ruleCondition.kindType == MXPushRuleConditionTypeEventMatch,
                                let key = ruleCondition.parameters["key"] as? String,
                                let pattern = ruleCondition.parameters["pattern"] as? String,
                            key == "room_id",
                                pattern == self.roomId,
                                overrideRule.enabled {
                                return .None
                            }
                        }
                    }
                }
            }
            if let rule = getPushRule() {
                for ruleAction in rule.actions as? [MXPushRuleAction] ?? [] {
                    if ruleAction.actionType == MXPushRuleActionTypeDontNotify,
                        rule.enabled {
                        return .Mentions
                    }
                }
            }
            return .All
        }
        set {
            // Ignore cases when nothing really changes
            guard newValue != notificationLevel else {return}
            
            let nc = mxSession.notificationCenter
            
            switch newValue {
            case .None:
                if notificationLevel == .Mentions, let rule = getPushRule() {
                    nc?.removeRule(rule)
                }

                guard let roomId = roomId else {
                    break
                }

                if let overrideRule = getOverridePushRule() {
                    // Does this rule have a "no notify" action on it?
                    if let _ = overrideRule.actions.first(where: { (o) -> Bool in
                        if let action = o as? MXPushRuleAction, action.actionType == MXPushRuleActionTypeDontNotify {
                            return true
                        }
                        return false
                    }) {
                        // Yes, just enable the rule
                        nc?.enableRule(overrideRule, isEnabled: true)
                    } else {
                        // No such action. Remove rule and create a new.
                        nc?.removeRule(overrideRule)
                        nc?.addOverrideRule(withId: self.roomId, conditions: [["kind":"event_match","key":"room_id","pattern" : roomId]], notify: false, sound: false, highlight: false)
                    }
                } else {
                    // No rule defined, add one
                    nc?.addOverrideRule(withId: self.roomId, conditions: [["kind":"event_match","key":"room_id","pattern" : roomId]], notify: false, sound: false, highlight: false)
                }
                break
            case .Mentions:
                // Check whether an override rule must be removed first
                if notificationLevel == .None, let overrideRule = getOverridePushRule() {
                    nc?.removeRule(overrideRule)
                }
                
                // The user wants to have push only for highlighted notifications
                if let rule = getPushRule() {
                    // Does this rule have a "no notify" action on it?
                    if let _ = rule.actions.first(where: { (o) -> Bool in
                        if let action = o as? MXPushRuleAction, action.actionType == MXPushRuleActionTypeDontNotify {
                            return true
                        }
                        return false
                    }) {
                        // Yes, just enable the rule
                        nc?.enableRule(rule, isEnabled: true)
                    } else {
                        // No such action. Remove rule and create a new.
                        nc?.removeRule(rule)
                        nc?.addRoomRule(self.roomId, notify: false, sound: false, highlight: false)
                    }
                } else {
                    // No rules defined, so add a "mentions only" one
                    nc?.addRoomRule(self.roomId, notify: false, sound: false, highlight: false)
                }
                break
            case .All:
                if notificationLevel == .None, let overrideRule = getOverridePushRule() {
                    nc?.removeRule(overrideRule)
                }
                if notificationLevel == .Mentions, let rule = getPushRule() {
                    nc?.removeRule(rule)
                }
                break
            }
        }
    }
}
