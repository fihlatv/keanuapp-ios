//
//  ChatListViewController.swift
//  Keanu
//
//  Created by N-Pex on 26.09.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

open class ChatListViewController: UIViewController, ChatListProtocol, RoomSummariesDataSourceDelegate,
UITableViewDelegate, ChooseFriendsDelegate {
    
    // MARK: Storyboard outlets
    @IBOutlet weak open var tableView: UITableView!

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    // MARK: Properties
    private var dataSource: ChatListDataSource?
    open var acceptInviteAction: UITableViewRowAction?
    open var declineInviteAction: UITableViewRowAction?
    open var deleteRoomAction: UITableViewRowAction?
    open var archiveRoomAction: UITableViewRowAction?
    open var unarchiveRoomAction: UITableViewRowAction?
    open var archiveSegmentedControl: UISegmentedControl!

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Chats".localize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add, target: self, action: #selector(chooseFriendsForNewRoom(_:)))
        
        // Install a segmented control as titleView. This controls whether we show "archived"
        // rooms or "non-archived" rooms. Basically archiving is a custom (i.e. not built in
        // to matrix sdk) way to hide rooms.
        archiveSegmentedControl = UISegmentedControl(items: [
            "Active".localize(), // Tab for active chats
            "Archive".localize() // Tab for archived chats
            ])
        archiveSegmentedControl.selectedSegmentIndex = 0
        archiveSegmentedControl.addTarget(self, action: #selector(archivedSegmentedControlValueChanged(_:)), for: .valueChanged)
        navigationItem.titleView = archiveSegmentedControl
        
        // Set ourselves as delegate, we want to provide row actions
        tableView.delegate = self
        
        // Set default row height. Actually just used when the table view is empty.
        tableView.rowHeight = 80
        
        // Register cell types
        tableView.register(AvatarAnd3LabelsCell.nib, forCellReuseIdentifier: AvatarAnd3LabelsCell.defaultReuseId)
        tableView.register(ChatListInviteCell.nib, forCellReuseIdentifier: ChatListInviteCell.defaultReuseId)
        
        // Create action objects used by the tableview
        // Action for accepting an invite.
        acceptInviteAction = UITableViewRowAction(style: .normal, title: "Accept".localize()) { (action, indexPath) in
            if let roomSummary = self.dataSource?.roomSummary(at: indexPath) {
                self.acceptInviteForRoom(roomId: roomSummary.roomId)
            }
        }
        // Action for declining an invite.
        declineInviteAction = UITableViewRowAction(style: .destructive, title: "Decline".localize()) { (action, indexPath) in
            if let roomSummary = self.dataSource?.roomSummary(at: indexPath) {
                self.declineInviteForRoom(roomId: roomSummary.roomId)
            }
        }
        // Action for deleting a room.
        deleteRoomAction = UITableViewRowAction(style: .destructive, title: "Delete".localize()) { (action, indexPath) in
            guard let room = self.dataSource?.roomSummary(at: indexPath)?.room else {return}
            UIApplication.shared.leaveRoom(room: room, callback: nil)
        }
        // Action for unarchiving a room.
        unarchiveRoomAction = UITableViewRowAction(style: .normal, title: "Unarchive".localize()) { (action, indexPath) in
            if let roomSummary = self.dataSource?.roomSummary(at: indexPath) {roomSummary.isArchived = false
                self.dataSource?.reload()
            }
        }
        // Action for archiving a room.
        archiveRoomAction = UITableViewRowAction(style: .normal, title: "Archive".localize()) { (action, indexPath) in
            if let roomSummary = self.dataSource?.roomSummary(at: indexPath) {
                roomSummary.isArchived = true
                self.dataSource?.reload()
            }
        }
    }
    
    deinit {
        dataSource = nil
    }
    
    open func createDataSource() -> ChatListDataSource? {
        return ChatListDataSource()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        // Create the table view data source
        dataSource = createDataSource()
        dataSource?.delegate = self
        dataSource?.displayArchive = (archiveSegmentedControl.selectedSegmentIndex == 1)
        tableView.dataSource = dataSource
        super.viewWillAppear(animated)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let ds = tableView.dataSource as? NoFriendsDataSource {
            ds.delegate = nil
        }
        tableView.dataSource = nil
        dataSource = nil
    }
    
    // MARK: ChatDataSourceDelegate
    open func createCell(at indexPath: IndexPath, for roomSummary: MXRoomSummary) -> UITableViewCell? {
        
        // Is this an invite cell or a "normal" chat cell?
        if roomSummary.isInvite {
            if let inviteCell = tableView.dequeueReusableCell(withIdentifier:
                ChatListInviteCell.defaultReuseId, for: indexPath) as? ChatListInviteCell {
                inviteCell.delegate = self
                return inviteCell.apply(roomSummary)
            }
        } else if let chatListCell = tableView.dequeueReusableCell(withIdentifier:
            AvatarAnd3LabelsCell.defaultReuseId, for: indexPath) as? AvatarAnd3LabelsCell {
            return chatListCell.apply(roomSummary)
        }
        return nil
    }

    /**
     Check if we should show the active/archive segmented control. If we have no archived rooms we hide it. Make sure that if we hide it, we are on the first segment, i.e. "active".
     */
    private func checkShowArchived() {
        archiveSegmentedControl.isHidden = !(dataSource?.hasArchivedRooms ?? false)
        navigationItem.titleView = (archiveSegmentedControl.isHidden) ? nil : archiveSegmentedControl
        if archiveSegmentedControl.isHidden, archiveSegmentedControl.selectedSegmentIndex != 0 {
            DispatchQueue.main.async {
                self.archiveSegmentedControl.selectedSegmentIndex = 0
                self.archivedSegmentedControlValueChanged(self.archiveSegmentedControl)
            }
        }
    }
    
    public func didChangeAllRows() {
        tableView.reloadData()
        checkShowArchived()
        showNoFriendsIfNoItems()
    }
    
    public func didChangeRows(at indexPaths: [IndexPath]) {
        tableView.reloadData()
        checkShowArchived()
        showNoFriendsIfNoItems()
    }
    
    private func showNoFriendsIfNoItems() {
        guard let ds = dataSource else { return }
        if ds.tableView(tableView, numberOfRowsInSection: 0) == 0, FriendsManager.shared.friends.count == 0 {
            let dsNoItems = NoFriendsDataSource()
            dsNoItems.delegate = self
            tableView.dataSource = dsNoItems
        } else if tableView.dataSource == nil || tableView.dataSource is NoFriendsDataSource {
            tableView.dataSource = ds
        }
    }
    
    // MARK: UITableViewDelegate
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView.dataSource is NoFriendsDataSource {
            UIApplication.shared.openAddFriendsViewController()
            return
        }
        
        if let roomSummary = dataSource?.roomSummary(at: indexPath), !roomSummary.isArchived, let room = roomSummary.room {
            
            // Only allow room opening if we have joined the room
            if roomSummary.membership == .join || roomSummary.membership == .invite {
                openRoom(room)
            }
        }
    }
    
    open func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if let roomSummary = dataSource?.roomSummary(at: indexPath) {
            var actions:[UITableViewRowAction] = []
            if roomSummary.isInvite {
                // Invites can be accepted or rejected
                actions.appendOptional(acceptInviteAction)
                actions.appendOptional(declineInviteAction)
            } else {
                // Normal chats can be deleted or archived/unarchived depending on state
                actions.appendOptional(deleteRoomAction)
                if roomSummary.isArchived {
                    actions.appendOptional(unarchiveRoomAction)
                } else {
                    actions.appendOptional(archiveRoomAction)
                }
            }
            return actions
        }
        return nil
    }
    
    func didJoinRoom(_ roomSummary:MXRoomSummary) {
    }
    
    func didFailToJoinRoom(_ roomSummary:MXRoomSummary, with error:Error?) {
    }
    
    // MARK: ChooseFriendsDelegate
    
    /**
     User returned from `ChooseFriendsViewController` and selected a list of
     friends.
     
     List should contain *at least* one friend as per contract!

     Will use the session (the account), where the first friend is attached to.
     
     - parameter friends: List of `MXUser` objects the user selected.
    */
    public func friendsChosen(_ friends: [Friend]) {
        guard friends.count > 0 else {return}
        UIApplication.shared.openRoom(friends)
    }

    /**
     Create an empty, excrypted, room, to which you can later add members.
     */
    public func createEmptyRoom(session: MXSession) {
        let vc = UIApplication.shared.theme.createRoomViewController()
        vc.createEmptyRoom(session: session)

        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Storyboard bindable actions

    @IBAction func archivedSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        dataSource?.displayArchive = (sender.selectedSegmentIndex == 1)
        tableView.reloadData()
    }
    
    
    // MARK: Actions
    
    /**
     Open the given room.
     */
    open func openRoom(_ room: MXRoom?) {
        let vc = UIApplication.shared.theme.createRoomViewController()
        vc.room = room

        navigationController?.pushViewController(vc, animated: true)
    }
    
    /**
     Show the `ChooseFriendsViewController` to choose friends for a new room.
    */
    @IBAction func chooseFriendsForNewRoom(_ sender: Any) {
        let vc = UIApplication.shared.theme.createChooseFriendsViewController()
        vc.delegate = self
        vc.multiselect = true

        present(UINavigationController(rootViewController: vc), animated: true)
    }
}

extension ChatListViewController: ChatListInviteCellDelegate {
    func acceptInviteForRoom(roomId: String) {
        workingOverlay.isHidden = false
        UIApplication.shared.joinRoom(roomId) { (success, room) in
            self.workingOverlay.isHidden = true
            if success,
                let chatListViewController = UIApplication.shared.popToChatListViewController() {
                chatListViewController.openRoom(room)
            }
        }
    }
    
    func declineInviteForRoom(roomId: String) {
        workingOverlay.isHidden = false
        UIApplication.shared.declineRoomInvite(roomId: roomId) { (success) in
            self.workingOverlay.isHidden = true
        }
    }
}

extension ChatListViewController: NoFriendsDataSourceDelegate {
    public func shouldCloseDataSource(_ noFriendsDataSource: NoFriendsDataSource) {
        noFriendsDataSource.delegate = nil
        
        // Set the real DataSource on the tableView
        tableView.dataSource = self.dataSource
        tableView.reloadData()
    }
}
