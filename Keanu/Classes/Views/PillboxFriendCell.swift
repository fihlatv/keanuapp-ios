//
//  PillboxFriendCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 25.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

public protocol PillboxFriendCellDelegate: class {
    func remove(_ friend: NSObject)
}

open class PillboxFriendCell: UICollectionViewCell {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }
    
    @IBOutlet open weak var avatarImg: AvatarView!
    @IBOutlet open weak var nameLb: UILabel!
    @IBOutlet open weak var removeBt: UIButton!
    
    open weak var friend: NSObject?
    open weak var delegate: PillboxFriendCellDelegate?
    
    override open func layoutSubviews() {
        layer.cornerRadius = frame.height / 2
        
        super.layoutSubviews()
    }

    /**
     Applies a `Friend` to this cell.
     
     - parameter friend: The friend to apply.
     - parameter delegate: The delegate for the remove callback. Optional.
     - returns: self for convenience.
     */
    open func apply(_ friend: Friend, _ delegate: PillboxFriendCellDelegate? = nil) -> PillboxFriendCell {
        self.delegate = delegate
        self.friend = friend
        avatarImg.load(friend: friend)
        nameLb.text = friend.name
        
        // If we don't have a delegate attached, the originating view controller
        // probably listens to #didSelectItemAt or similar events.
        // So make this button unusable, in order to forward tap events to the
        // cell itself.
        removeBt.isUserInteractionEnabled = delegate != nil
        
        return self
    }

    @IBAction open func remove() {
        if let friend = friend {
            delegate?.remove(friend)
        }
    }
}
