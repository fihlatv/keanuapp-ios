//
//  ActionButtonCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 25.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

open class ActionButtonCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    public static var height: CGFloat = 85

    @IBOutlet weak var iconLb: UILabel!
    @IBOutlet weak var actionLb: UILabel!

    /**
     Applies an icon and an action label to this cell.

     - parameter icon: Should be 1 character, will use the MaterialIcons font.
     - parameter action: The text label content.
     - parameter color: The icon background color. (text will be white!) Defaults to `.darkGray`.
     - returns: self for convenience.
     */
    @discardableResult
    public func apply(_ icon: String?, _ action: String?, _ color: UIColor = .darkGray) -> ActionButtonCell {
        iconLb.text = icon
        iconLb.backgroundColor = color
        actionLb.text = action
        
        return self
    }

    /**
     Applies the "add friend" icon and text.

     - returns: self for convenience.
     */
    @discardableResult
    public func addFriend() -> ActionButtonCell {
        return apply(.person_add, "Add Friend".localize())
    }

    /**
     Applies the "create room" icon and text.

     - returns: self for convenience.
     */
    @discardableResult
    public func createRoom() -> ActionButtonCell {
        return apply(.group_add, "Create a Group".localize())
    }
    
    /**
     Applies the "photo stream" icon and text.

     - returns: self for convenience.
     */
    @discardableResult
    public func createPhotoStream() -> ActionButtonCell {
        return apply(.photo, "Photo Stream".localize())
    }

    /**
     Applies the "story stream" icon and text.

     - returns: self for convenience.
     */
    @discardableResult
    public func createStoryStream() -> ActionButtonCell {
        return apply(.playlist_play, "Story Stream".localize())
    }
}
