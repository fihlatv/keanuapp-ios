//
//  StreamDataSource.swift
//  Keanu
//

import KeanuCore
import MatrixSDK

open class StreamEvent: NSObject, Comparable {
    open var room: MXRoomSummary?
    open var roomState: MXRoomState?
    open var event: MXEvent?
    
    enum GeneralError: Error {
        case Error
    }
    
    // Order by timestamps
    public static func < (lhs: StreamEvent, rhs: StreamEvent) -> Bool {
        guard let event1 = lhs.event, let event2 = rhs.event else { return true }
        if event1.originServerTs == event2.originServerTs {
            return event1.eventId.compare(event2.eventId) == .orderedAscending
        }
        return event1.originServerTs >= event2.originServerTs
    }
}

protocol StreamDataSourceDelegate: class {
    /**
     Called to update loading progress.
     */
    func didUpdateProgress(source:StreamDataSource)
    
    /**
     Called when the datasource is initialized, i.e. all events from the history are read from local storage.
     */
    func didInitialize(source:StreamDataSource)
    
    /**
     Called when an event is added, removed or changed.
     */
    func didChange(source:StreamDataSource, deleted:[IndexPath]?, added:[IndexPath]?, changed:[IndexPath]?, live:Bool, disableScroll:Bool)
    
    /**
     Implemented to filter out events. This method will be called with any event read by the datasource. Return "true" to include it, "false" to filter it from the data source completely.
     */
    func shouldInclude(source:StreamDataSource, event:MXEvent) -> Bool
}

class StreamDataSource: AggregatedRoomSummaries {
    public var events:SortedArray<StreamEvent> = SortedArray<StreamEvent>()
    private var eventEventMap:[String:StreamEvent] = [:]
    private var eventListeners:[MXSession:Any] = [:]
    private var reactionListeners:[String:Any] = [:]
    
    /**
     When loading events, don't waste time by sorting them into the "events" array. Just append them here and move them over as a batch.
     */
    private var tempEvents:[StreamEvent] = []
    
    /**
     Number of items to process (i.e. that we have in the store(s)
     */
    public var progressTotal: UInt = 0
    
    /**
     Number of items processed
     */
    public var progressDone: UInt = 0
    
    /**
     Whether or not this datasource has read in all events from room history (local storage).
     */
    public var initialized: Bool = false
    
    public weak var delegate: StreamDataSourceDelegate?
    
    private var internalQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Stream loading queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    /**
     nil for ALL rooms, otherwise only consider events for this room.
     */
    private var room:MXRoom?
    
    /**
     Set this to only include messages from this room. If this is nil, the datasource considers ALL rooms.
     */
    public init(room:MXRoom?) {
        self.room = room
        super.init()
        
        DispatchQueue.main.async { [weak self] in
            // Post a "loaded" callback to queue. Will be called after all initialization is done in super.
            guard let self = self else {return}
            self.internalQueue.addOperation { [weak self] in
                if let self = self {
                    // Move all temp events to sorted array
                    self.events.insert(contentsOf: self.tempEvents)
                    self.tempEvents.removeAll()
                    
                    self.initialized = true
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else {return}
                        self.delegate?.didInitialize(source: self)
                    }
                }
            }
        }
    }
    
    deinit {
        invalidate()
    }
    
    /**
     Invalidate the data source. This will stop listeners and observers.
     */
    open func invalidate() {
        delegate = nil
        internalQueue.cancelAllOperations()
        for (session, listener) in eventListeners {
            session.removeListener(listener)
        }
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
        eventListeners = [:]
        events.removeAll()
        eventEventMap.removeAll()
    }
    
    open func indexOf(event: MXEvent) -> Int? {
        if let streamEvent = eventEventMap[event.eventId] {
            return events.firstIndex(of: streamEvent)
        }
        return nil
    }
    
    override func didAddRoomSummary(_ roomSummary: MXRoomSummary) {
        // Room set?
        if let room = room, room.roomId != roomSummary.roomId {
            return // Not a room we are considering
        }
        
        if eventListeners[roomSummary.mxSession] == nil {
            // Register a listener for this session
            //
            eventListeners[roomSummary.mxSession] = roomSummary.mxSession.listenToEvents(nil) { [weak self] (event, direction, roomState) in
                guard let self = self else {return}
                if let room = self.room, room.roomId != event.roomId {
                    return // Not a room we are considering
                }
                
                self.progressDone += 1
                DispatchQueue.main.async {
                    self.delegate?.didUpdateProgress(source: self)
                }
                guard event.eventType == .roomMessage else {return}
                
                // Ignore events from rooms we are not joined to (if any should appear...)
                if roomSummary.isInvite {
                    return
                }
                
                if let delegate = self.delegate, !delegate.shouldInclude(source: self, event: event) {
                    return // Filter this
                }
                
                DispatchQueue.main.async {
                    if self.eventEventMap[event.eventId] != nil {
                        // Already present, mark as updated
                        self.onEventUpdated(event.eventId)
                    } else if
                        !event.isLocalEvent(),
                        let room = self.roomSummaryFromRoomId(event.roomId),
                        let localEchoEvent = room.room.pendingLocalEchoRelated(to: event),
                        let streamEvent = self.eventEventMap[localEchoEvent.eventId] {
                        // The "real" event that replaces a local echo! Id might have changed(?), so update our mapping here.
                        self.eventEventMap.removeValue(forKey: localEchoEvent.eventId)
                        self.eventEventMap[event.eventId] = streamEvent
                        event.userData = localEchoEvent.userData
                        streamEvent.event = event
                        streamEvent.roomState = roomState as? MXRoomState
                        // TODO - call didChange here(?)
                    } else {
                        // Add to list
                        let streamEvent = StreamEvent()
                        streamEvent.room = self.roomSummaryFromRoomId(event.roomId)
                        streamEvent.roomState = roomState as? MXRoomState
                        streamEvent.event = event
                        self.eventEventMap[event.eventId] = streamEvent
                        if (self.initialized) {
                            let index = self.events.insert(streamEvent)
                            self.delegate?.didChange(source:self, deleted: nil, added: [IndexPath(row: index, section: 0)], changed: nil, live: direction == .forwards, disableScroll: false)
                        } else {
                            self.tempEvents.append(streamEvent)
                        }
                    }
                }
            }
        }
        
        roomSummary.room.liveTimeline { [weak self] (timeline) in
            guard let self = self else {return}
            guard let timeline = timeline else {return}
            timeline.resetPagination()
            
            // Update totals for progress
            self.progressTotal += timeline.remainingMessagesForBackPaginationInStore()
            DispatchQueue.main.async {
                self.delegate?.didUpdateProgress(source: self)
            }
            
            self.internalQueue.addOperation({
                if timeline.canPaginate(.backwards) {
                    timeline.paginate(timeline.remainingMessagesForBackPaginationInStore(), direction: .backwards, onlyFromStore: true) { (response) in
                        // done
                    }
                }
            })
        }
        
        // Add a reaction listener
        //
        let listener =  roomSummary.mxSession.aggregations.listenToReactionCountUpdate(inRoom: roomSummary.roomId) { [weak self](changes:[String : MXReactionCountChange]) in
            guard let self = self else {return}
            for change in changes {
                self.onEventUpdated(change.key)
            }
        }
        reactionListeners[roomSummary.roomId] = listener
    }
    
    override func didRemoveRoomSummary(_ roomSummary: MXRoomSummary) {
        if let listener = reactionListeners[roomSummary.roomId] {
            roomSummary.mxSession.aggregations.removeListener(listener)
            reactionListeners.removeValue(forKey: roomSummary.roomId)
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            
            // Remove messages
            var indexPathsRemoved:[IndexPath] = []
            for i in 0..<self.events.count {
                if let s = self.events[i].room, s.roomId == roomSummary.roomId {
                    indexPathsRemoved.append(IndexPath(row: i, section: 0))
                }
            }
            self.events = self.events.filter { (streamEvent) -> Bool in
                if let s = streamEvent.room, s.roomId == roomSummary.roomId {
                    return false
                }
                return true
            }
            self.delegate?.didChange(source: self, deleted: indexPathsRemoved, added: nil, changed: nil, live: false, disableScroll: true)
        }
    }
    
    override func didChangeRoomSummary(_ roomSummary: MXRoomSummary) {
    }
    
    override func didChangeAllRoomSummaries() {
    }
    
    override func shouldAddRoomSummary(_ roomSummary: MXRoomSummary) -> Bool {
        if roomSummary.isArchived {
            return false
        }
        if roomSummary.membership == .join || roomSummary.membership == .invite {
            return true
        }
        return false
    }
    
    func roomSummaryFromRoomId(_ roomId: String) -> MXRoomSummary? {
        return self.roomSummaries.first(where: { $0.roomId == roomId})
    }
    
    func onEventUpdated(_ eventId: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            if
                let streamEvent = self.eventEventMap[eventId],
                let index = self.events.index(of: streamEvent) {
                if self.initialized {
                    self.delegate?.didChange(source: self, deleted: nil, added: nil, changed: [IndexPath(row: index, section: 0)], live: false, disableScroll: true)
                }
            }
        }
    }
}
