//
//  PhotoStreamHandler.swift
//  Keanu
//
//  Created by N-Pex on 18.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import MatrixSDK

/**
 A class that is responsible for loading all images in a room (or in ALL rooms) so that they can be displayed e.g. in a PhotoStreamViewController (as a grid of thumbnails) or in a PhotosViewController (as swipable zoomable images). See the __fetchImagesAsync__ function for more info.
 */
public class PhotoStreamHandler: NSObject {
    
    var internalQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Image loading queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    public var images:[PhotoStreamImage] = []
    var initialImage:PhotoStreamImage?
    var canceled = false
    
    deinit {
        internalQueue.cancelAllOperations()
        for image in images {
            image.releaseImages()
        }
    }
    
    public func cancelFetching() {
        canceled = true
        internalQueue.cancelAllOperations()
        images = []
        initialImage = nil
    }
    
    /**
     Load images from the given room, or for ALL rooms if __room__ is nil.
     - parameter room: The room to load images for. If room is nil, load images for ALL rooms in all active accounts.
     - parameter initialEvent: Optional event that corresponds to the initial photo to show.
     - parameter onStart: Optional callback for when the load operation starts. Can be used to show a "WorkingOverlay" or similar.
     - parameter onEnd: Optional callback for when the load operation is complete. Will be called with the array of images loaded, and optional initial image.
     */
    open func fetchImagesAsync(room: MXRoom?, initialEvent: MXEvent?,
                               onStart: (()->())?, onEnd: (([PhotoStreamImage],PhotoStreamImage?)->())?) {
        cancelFetching()
        canceled = false
        
        onStart?()
        
        DispatchQueue.global(qos: .background).async {
            
            var roomsToEnumerate:[MXSession:[MXRoom]] = [:]
            if let singleRoom = room {
                if let session = room?.mxSession {
                    roomsToEnumerate[session] = [singleRoom]
                }
            } else {
                // Add ALL rooms in all active accounts
                MXKAccountManager.shared()?.activeAccounts?.forEach({ (account) in
                    roomsToEnumerate[account.mxSession] = account.mxSession.rooms
                })
            }
            
            
            // Enumerate all room messages in the store on the background thread.
            //
            let types:[String] = [kMXEventTypeStringRoomMessage, kMXEventTypeStringRoomEncrypted]
            for (session, rooms) in roomsToEnumerate {
                for room in rooms {
                    var timeline: MXEventTimeline? = nil
                    if let enumerator = room.enumeratorForStoredMessagesWithType(in: types) {
                        while !self.canceled, let events = enumerator.nextEventsBatch(10) {
                            events.forEach({ (e) in
                                if e.isEncrypted {
                                    if timeline == nil {
                                        timeline = room.timeline(onEvent: e.eventId)
                                    }
                                    if let timeline = timeline {
                                        session.decryptEvent(e, inTimeline: timeline.timelineId)
                                    }
                                }
                                if e.isMediaAttachment(), e.isImageAttachment() {
                                    let image = PhotoStreamImage(event: e, session: session, operationQueue: self.internalQueue)
                                    self.images.append(image)
                                    if e.eventId == initialEvent?.eventId {
                                        self.initialImage = image
                                    }
                                }
                            })
                        }
                    }
                }
            }
            
            //TODO - sort on date!
            //            self.images.sort(by: { (item1, item2) -> Bool in
            //                return item1.date().compare(item2.date()) == .orderedAscending
            //            })
            
            
            
            DispatchQueue.main.async {
                if !self.canceled {
                    onEnd?(self.images, self.initialImage)
                }
            }
        }
    }
    
}
