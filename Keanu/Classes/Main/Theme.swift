//
//  Theme.swift
//  Keanu
//
//  Created by N-Pex on 06.02.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

/**
 Protocol to handle theming of base Keanu. In this context "theming" is to mean replace view controllers with custom subclasses to do custom setup etc.
 - Tag: Theme
 */
public protocol Theme {


    // MARK: Onboarding

    /**
     Create a `WelcomeViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `WelcomeViewController`.
     */
    func createWelcomeViewController() -> WelcomeViewController

    /**
     Create a `AddAccountViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `AddAccountViewController`.
     */
    func createAddAccountViewController() -> AddAccountViewController

    /**
     Create a `EnablePushViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `EnablePushViewController`.
     */
    func createEnablePushViewController() -> EnablePushViewController


    // MARK: Main

    /**
     Create a `MainViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `MainViewController`.
     */
    func createMainViewController() -> MainViewController

    /**
     Create a `ChatListViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `ChatListViewController`.
     */
    func createChatListViewController() -> ChatListViewController

    /**
     Create a `DiscoverViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `DiscoverViewController`.
     */
    func createDiscoverViewController() -> DiscoverViewController

    /**
     Create a `MeViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `MeViewController`.
     */
    func createMeViewController() -> MeViewController


    // MARK: Room

    /**
     Create a `RoomViewController` that will later be pushed on the navigation stack.

     - returns: A new instance of a `RoomViewController`.
     */
    func createRoomViewController() -> RoomViewController

    /**
     Create a `RoomSettingsViewController`.

     - returns: A new instance of a `RoomSettingsViewController`.
     */
    func createRoomSettingsViewController() -> RoomSettingsViewController

    /**
     Create a `ProfileViewController`.

     - returns: A new instance of a `ProfileViewController`.
     */
    func createProfileViewController() -> ProfileViewController

    /**
     Create a `StoryViewController`.

     - returns: A new instance of a `StoryViewController`.
     */
    func createStoryViewController() -> StoryViewController

    /**
     Create a `StoryAddMediaViewController`.

     - returns: A new instance of a `StoryAddMediaViewController`.
     */
    func createStoryAddMediaViewController() -> StoryAddMediaViewController

    /**
     Create a `StoryGalleryViewController`.

     - returns: A new instance of a `StoryGalleryViewController`.
     */
    func createStoryGalleryViewController() -> StoryGalleryViewController

    /**
     Create a `StoryEditorViewController`.

     - returns: A new instance of a `StoryEditorViewController`.
     */
    func createStoryEditorViewController() -> StoryEditorViewController

    /**
     Create a `StickerPackViewController`.

     - returns: A new instance of a `StickerPackViewController`.
     */
    func createStickerPackViewController() -> StickerPackViewController

    /**
     Create a `PickStickerViewController`.

     - returns: A new instance of a `PickStickerViewController`.
     */
    func createPickStickerViewController() -> PickStickerViewController


    // MARK: Friends

    /**
     Create a `ChooseFriendsViewController`.

     - returns: A new instance of a `ChooseFriendsViewController`.
     */
    func createChooseFriendsViewController() -> ChooseFriendsViewController

    /**
     Create an `AddFriendViewController`.

     - returns: A new instance of an `AddFriendViewController`.
     */
    func createAddFriendViewController() -> AddFriendViewController

    /**
     Create a `ShowQrViewController`.

     - returns: A new instance of a `ShowQrViewController`.
     */
    func createShowQrViewController() -> ShowQrViewController

    /**
     Create a `QrScanViewController`.

     - returns: A new instance of a `QrScanViewController`.
     */
    func createQrScanViewController() -> QrScanViewController

    /**
     Create a `VerificationViewController`.

     - returns: A new instance of a `VerificationViewController`.
     */
    func createVerificationViewController() -> VerificationViewController


    // MARK: Discover

    /**
     Create a `PhotoStreamViewController`.

     - returns: A new instance of a `PhotoStreamViewController`.
     */
    func createPhotoStreamViewController() -> PhotoStreamViewController
    
    // MARK: Misc UI
    
    /**
     Create a chat message bubble of the given type, for the given rect.
     */
    func createChatBubble(type: BubbleViewType, rect: CGRect) -> CGPath?
}
