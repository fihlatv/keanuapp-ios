//
//  MainViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 16.03.20.
//

import UIKit

open class MainViewController: UITabBarController {

    override open func viewDidLoad() {
        super.viewDidLoad()

        let theme = UIApplication.shared.theme

        viewControllers = [
            item(theme.createChatListViewController(), "ic_message_white", 1),
            item(theme.createChooseFriendsViewController(), "ic_group_white", 2),
            item(theme.createDiscoverViewController(), "ic_discover_white", 3),
            item(theme.createMeViewController(), "ic_face_white", 4)]
    }

    /**
     Create a `UINavigationController` with the given root view controller using the given named image
     as `UITabBarItem` and with the given tag.

     - parameter vc: The root view controller.
     - parameter image: The image name to use for the `UITabBarItem`.
     - parameter tag: The tag ID to use for the `UITabBarItem`. (Typically not important but it should be some unique number.
     - returns: a `UINavigationController` containing the `vc` as root view controller.
     */
    open func item(_ vc: UIViewController, _ image: String, _ tag: Int) -> UINavigationController {
        let navC = UINavigationController(rootViewController: vc)

        navC.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: image, in: Bundle(for: type(of: self)), compatibleWith: nil),
            tag: tag)

        return navC
    }
}
