//
//  RecaptchaAndTermsViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 15.10.18.
//  Copyright © 2018 - 2019 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 Protocol to receive the secret from the solved Recaptcha.
*/
protocol RecaptchaAndTermsViewControllerDelegate {
    
    /**
     The delegate receives this, when the user successfully solved the Recaptcha and accepted all terms.
     
     The `RecaptchaAndTermsViewController` will dismiss itself right after it.
     
     - parameter secret: The eventual secret from the solved Recaptcha.
    */
    func success(_ secret: String?)
}

/**
 `UIViewController` which only purpose it is to show a Recaptcha in a `WKWebView`.
 
 Will call `RecaptchaViewControllerDelegate#setSecret(String)` upon the user
 successfully solving the recaptcha and dismisses itself again.
 
 The user can click a "Cancel" button in a navigation bar to dismiss this
 view controller without solving the Recaptcha.
 */
class RecaptchaAndTermsViewController: UINavigationController {

    private let successDelegate: RecaptchaAndTermsViewControllerDelegate?
    private var vcs = [UIViewController]()
    private var index = 0

    var secret: String?

    init(_ homeServer: String?, _ publicKey: String?, _ terms: MXLoginTerms?, delegate: RecaptchaAndTermsViewControllerDelegate) {
        successDelegate = delegate

        super.init(nibName: nil, bundle: nil)

        if let homeServer = homeServer, let publicKey = publicKey {
            vcs.append(RecaptchaViewController(homeServer, publicKey))
        }

        let policies = terms?.policiesData(forLanguage: Locale.preferredLanguages.first, defaultLanguage: "en") ?? []

        for policy in policies {
            vcs.append(PolicyViewController(policy))
        }

        setViewController(vcs.first)
    }

    required init?(coder: NSCoder) {
        successDelegate = coder.decodeObject(forKey: "successDelegate") as? RecaptchaAndTermsViewControllerDelegate

        super.init(coder: coder)
    }

    override func popViewController(animated: Bool) -> UIViewController? {
        if index < vcs.count - 1 {
            index += 1

            setViewController(vcs[index])

            return vcs[index - 1]
        }

        successDelegate?.success(secret)

        dismiss(animated: true)

        return vcs[index]
    }

    private func setViewController(_ viewController: UIViewController?) {
        var viewControllers = [UIViewController]()

        if let viewController = viewController {
            viewControllers.append(viewController)
        }

        setViewControllers(viewControllers, animated: true)

        viewControllers.first?.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
    }

    @objc private func cancel() {
        dismiss(animated: true)
    }
}
