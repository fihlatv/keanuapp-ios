//
//  AuthenticationViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 12.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import Eureka
import AFNetworking

public protocol AuthenticationViewControllerDelegate {

    func didLogIn(with userId: String)
}

open class AuthenticationViewController: FormViewController, RecaptchaAndTermsViewControllerDelegate {

    /**
     Authentication types
     */
    public enum AuthType: UInt32 {

        /**
         Type used to sign up.
         */
        case register = 1

        /**
         Type used to sign in.
         */
        case login = 2

        /**
         Type used to restore an existing account by reseting the password.
         */
        case forgotPassword = 3
    }

    
    // MARK: Constants
    
    static let defaultHomeServerUrl = "https://\(Config.defaultHomeServer)"
    static let defaultIdServerUrl = "https://\(Config.defaultIdServer)"

    
    // MARK: public properties
    
    /**
     Safe some boilerplate by reuse of `MXKAuthenticationViewControllerDelegate`.
     Let's see, if that will do. :-)
    */
    open var delegate: AuthenticationViewControllerDelegate?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    // MARK: private properties
    
    private let authType: AuthType
    private var client: MXRestClient?
    private var currentOp: MXHTTPOperation?
    private var authSession: MXAuthenticationSession?

    private var nextStage: MXLoginFlowType? {
        if let next = authSession?.flows?.first?.stages?.filter({ !(authSession?.completed?.contains($0) ?? false) }).first {
            return MXLoginFlowType(identifier: next)
        }

        return nil
    }

    private var recaptchaSecret: String?
    private var termsAccepted = false
    
    /**
     Since we are trimming the user name, we remember what the user actually entered and use that as the display name for the account.
     */
    private var userEnteredName: String?

    /**
     The username.
     
     This is basically a proxy for `form.rowBy(tag: "username")` which also trims.
     */
    private var username: String {
        get {
            if let username = (form.rowBy(tag: "username") as? AccountRow)?.value {
                let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
                var name = username.filter { allowedCharacters.contains($0) }
                if name.count == 0 {
                    // No characters left, generate a random one!
                    name = generateUserName()
                }
                return name
            }
            
            return ""
        }
        set {
            if let username = form.rowBy(tag: "username") as? AccountRow {
                username.value = newValue
                username.reload()
            }
        }
    }

    /**
     The password.
     
     This is a proxy for `form.rowBy(tag: "password")`.
     */
    private var password: String {
        get {
            if let password = (form.rowBy(tag: "password") as? PasswordRow)?.value {
                return password
            }
            
            return ""
        }
        set {
            if let password = form.rowBy(tag: "password") as? PasswordRow {
                password.value = newValue
                password.reload()
            }
        }
    }
    
    private var homeServer: URL {
        get {
            if let homeServer = (form.rowBy(tag: "home_server") as? URLRow)?.value {
                return homeServer
            }
            
            return URL(string: AuthenticationViewController.defaultHomeServerUrl)!
        }
        set {
            if let homeServer = form.rowBy(tag: "home_server") as? URLRow {
                homeServer.value = newValue
                homeServer.reload()
            }
        }
    }

    private var idServer: URL {
        get {
            if let idServer = (form.rowBy(tag: "id_server") as? URLRow)?.value {
                return idServer
            }
            
            return URL(string: AuthenticationViewController.defaultIdServerUrl)!
        }
        set {
            if let idServer = form.rowBy(tag: "id_server") as? URLRow {
                idServer.value = newValue
                idServer.reload()
            }
        }
    }

    private var useKeyBackup: Bool {
        get {
            return (form.rowBy(tag: "key_backup") as? SwitchRow)?.value ?? true
        }
        set {
            if let useKeyBackup = form.rowBy(tag: "key_backup") as? SwitchRow {
                useKeyBackup.value = newValue
                useKeyBackup.reload()
            }
        }
    }

    /**
     The key backup password provided by the user. Will return `nil` *also*, if actually just empty.
     */
    private var keyBackupPassword: String? {
        get {
            if let password = (form.rowBy(tag: "key_backup_password") as? PasswordRow)?.value,
                !password.isEmpty
            {
                return password
            }

            return nil
        }
        set {
            if let password = form.rowBy(tag: "key_backup_password") as? PasswordRow {
                password.value = newValue
                password.reload()
            }
        }
    }

    private let checkRowValidity = { (cell: BaseCell, row: BaseRow) in
        if #available(iOS 13.0, *) {
            // Handle dark mode
            cell.backgroundColor = row.isValid ? .secondarySystemGroupedBackground : .orange
        }
        else {
            cell.backgroundColor = row.isValid ? .white : .orange
        }
    }


    public init(_ type: AuthType) {
        self.authType = type
        super.init(style: .grouped)
    }
    
    required public init?(coder: NSCoder) {
        authType = coder.decodeObject(forKey: "type") as? AuthType ?? .login

        super.init(coder: coder)
    }
    
    override open func encode(with coder: NSCoder) {
        coder.encode(authType, forKey: "type")

        super.encode(with: coder)
    }
    
    
    // MARK: UIViewController

    override open func viewDidLoad() {
        super.viewDidLoad()
        
        form
        +++ Section()
            
            <<< LabelRow("errors") {
                $0.hidden = true
            }
            .cellUpdate() { cell, row in
                cell.textLabel?.textColor = .orange
                cell.textLabel?.numberOfLines = 0
            }
            
            <<< AccountRow("username") {
                $0.placeholder = "User Name".localize()
                $0.add(rule: RuleRequired())
            }
            .onCellHighlightChanged() { _, row in
                // Check username availability on registration.
                if self.authType == .register {
                    if !self.username.isEmpty {
                        self.client?.isUserNameInUse(self.username) { used in
                            if used {
                                self.showError("This username is already taken.".localize())
                            }
                            else {
                                self.showError(false)
                            }
                        }
                    }
                }
            }
            .onRowValidationChanged(checkRowValidity)
            
        if authType == .register {
            form.first!.footer = HeaderFooterView(stringLiteral:
                "Think of a unique nickname that you don't use anywhere else and doesn't contain personal information."
                    .localize())
            
            form
            +++ Section(footer:
                "Make sure your password is a unique password you don't use anywhere else."
                    .localize())
        }
        
        form.last!
            <<< PasswordRow("password") {
                $0.placeholder = "Password".localize()
                $0.add(rule: RuleRequired())
                
                if authType == .register {
                    $0.add(rule: RuleMinLength(minLength: 8))
                }
            }
            .cellSetup() { cell, row in
                cell.accessoryType = .detailButton
            }
            .onRowValidationChanged(checkRowValidity)
        
        if authType == .register {
            form.last!
                <<< PasswordRow("confirm") {
                    $0.placeholder = "Confirm".localize()
                    $0.add(rule: RuleRequired())
                    $0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                    
                    if authType == .register {
                        $0.add(rule: RuleMinLength(minLength: 8))
                    }
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form
        +++ Section()
        <<< SwitchRow("advanced_options") {
                $0.title = "Show Advanced Options".localize()
                $0.value = false
            }
            .onChange() { _ in self.restartSession() }
   
        form
        +++ Section("Server".localize()) {
            $0.hidden = "$advanced_options == false"
        }
            <<< URLRow("home_server") {
                $0.title = "Home Server".localize()
                $0.placeholder = AuthenticationViewController.defaultHomeServerUrl
                $0.value = URL(string: AuthenticationViewController.defaultHomeServerUrl)
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

            <<< SwitchRow("customIdServer") {
                $0.title = "Use Custom Identity Server".localize()
                $0.value = false
            }

            <<< URLRow("id_server") {
                $0.title = "Identity Server".localize()
                $0.placeholder = AuthenticationViewController.defaultIdServerUrl
                $0.value = URL(string: AuthenticationViewController.defaultIdServerUrl)
                $0.hidden = "$customIdServer == false"
                $0.add(rule: RuleURL())
            }
            .onCellHighlightChanged() { _,_  in self.restartSession() }

        +++ Section(header: "Key Backup".localize(),
                    footer: "Our secure server remembers your encryption keys for you, so you can view your message history on all of your devices. Your account password is used unless you set a custom one.".localize()
        ) {
            $0.hidden = "$advanced_options == false"
        }

        <<< SwitchRow("key_backup") {
            $0.title = "Key Backup".localize()
            $0.value = true
        }

        <<< SwitchRow("own_key_backup_password") {
            $0.title = "Choose Password".localize()
            $0.value = false
            $0.hidden = "$advanced_options == false || $key_backup == false"
        }

        <<< PasswordRow("key_backup_password") {
            $0.placeholder = "Key Backup Password".localize()
            $0.hidden = "$advanced_options == false || $key_backup == false || $own_key_backup_password == false"

            if authType == .register {
                $0.add(rule: RuleMinLength(minLength: 8))
            }
        }
        .cellSetup() { cell, row in
            cell.accessoryType = .detailButton
        }
        .onRowValidationChanged(checkRowValidity)

        <<< PasswordRow("key_backup_confirm") {
            $0.placeholder = "Confirm".localize()
            $0.hidden = "$advanced_options == false || $key_backup == false || $own_key_backup_password == false"
            $0.add(rule: RuleEqualsToRow(form: form, tag: "key_backup_password"))

            if authType == .register {
                $0.add(rule: RuleMinLength(minLength: 8))
            }
        }
        .cellSetup() { cell, row in
            cell.accessoryType = .detailButton
        }
        .onRowValidationChanged(checkRowValidity)

        if authType == .register {
            navigationItem.title = "Sign Up".localize()
        } else {
            navigationItem.title = "Sign In".localize()
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(onDone))
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        restartSession()
    }
    

    // MARK: UITableViewDelegate
    
    /**
     The accessory button should only be on the password fields, so we assume that one was tapped.
     
     Toggles password visibility on tap.
     */
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PasswordCell {
            cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
        }
    }

    
    // MARK: RecaptchaAndTermsViewControllerDelegate
    
    /**
     User returned successfully from the Recaptcha solving and terms accepting.
     
     We can directly progress with sign in/up.
     
     - parameter secret: The secret from the solved Recaptcha.
    */
    func success(_ secret: String?) {
        recaptchaSecret = secret
        termsAccepted = true
        
        onDone()
    }

    
    // MARK: Actions

    /**
     The user pressed the "Sign In"/"Sign Up" button, which should have been
     possible only, if it is enabled because all credential fields are
     sufficiently filled.
    */
    @objc func onDone() {
        guard form.validate().isEmpty,
            let nextStage = nextStage else {
            return
        }

        print("[AuthenticationViewController] #onDone: nextStage=\(nextStage)")

        var auth = [String: Any]()

        switch nextStage {
        case .password:
            auth["type"] = kMXLoginFlowTypePassword
            auth["identifier"] = [
                "type": kMXLoginIdentifierTypeUser,
                "user": username,
            ]
            auth["password"] = password

        case .recaptcha:
            if (recaptchaSecret ?? "").isEmpty {
                return showRecaptchaAndTerms()
            }

            auth["type"] = kMXLoginFlowTypeRecaptcha
            auth["response"] = recaptchaSecret ?? ""

        case .dummy:
            auth["type"] = kMXLoginFlowTypeDummy

        case .other(kMXLoginFlowTypeTerms):
            if !termsAccepted {
                return showRecaptchaAndTerms()
            }

            auth["type"] = kMXLoginFlowTypeTerms

        default:
            return showUnsupportedError()
        }
        
        // Save the actual username entered, so we can use that as display name
        if let username = (form.rowBy(tag: "username") as? AccountRow)?.value {
            userEnteredName = username.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        let callback = opCompletion() { (data: [String: Any]) in

            // For an unkown reason MXJSONModel.model(fromJSON:) isn't exposed to Swift anymore.
            if let loginResponse = MXLoginResponse.models(fromJSON: [data])?.first as? MXLoginResponse {
                self.onSuccesfulSignIn(MXCredentials(
                    loginResponse: loginResponse,
                    andDefaultCredentials: self.client?.credentials))
            }
        }
        
        if authType == .login {
            currentOp?.cancel()
            
            if auth.keys.contains("password") {
                auth["initial_device_display_name"] = AuthenticationViewController.generateDeviceName()
            }
            
            currentOp = client?.login(parameters: auth, completion: callback)
        }
        else if authType == .register {
            currentOp?.cancel()
            
            auth["session"] = authSession?.session ?? ""
            
            let parameters: [String : Any] = [
                "auth": auth,
                "username": username,
                "password": password,
                "bind_email": false,
                "bind_msisdn": false,
                "initial_device_display_name": AuthenticationViewController.generateDeviceName()
            ]
            
            currentOp = client?.register(parameters: parameters, completion: callback)
        }
    }
    
    
    // MARK: Private methods
    
    /**
     We don't want to expose the device name (UIDevice.name) so generate one wich is leaking less information  when registering with Matrix.

     Non-private for testing reasons only!

     - returns: Something like "Keanu iPhone".
     */
    public class func generateDeviceName() -> String {
        return "\(Bundle.main.displayName) \(UIDevice.current.model)"
    }

    /**
     If the user enters only non-alpha characters, generate a random username.
     */
    private func generateUserName() -> String {
        let username = Bundle.main.displayName + "-" + String.randomNumericString(length: 6)

        // Return filtered one (app name can contain illegal chars!)
        let allowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=_-./"
        return username.filter { allowedCharacters.contains($0) }
    }

    
    /**
     Starts a new session with the currently set home server.
     
     You should call this on scene appearance or when the user changes the
     home server.
    */
    private func restartSession() {
        var changed = false
        
        if homeServer.absoluteString != client?.homeserver {
            changed = true
            client = MXRestClient(homeServer: homeServer, unrecognizedCertificateHandler: nil)
        }
        
        if idServer.absoluteString != client?.identityServer {
            changed = true
            client?.identityServer = idServer.absoluteString
        }

        if changed {
            currentOp?.cancel()
            authSession = nil
            showError(false)

            let callback = opCompletion() { (session: MXAuthenticationSession) in
                self.authSession = self.filterAuthSession(session)
                
                if self.authSession == nil {
                    self.showUnsupportedError()
                }
            }

            if authType == .login {
                currentOp = client?.getLoginSession(completion: callback)
            }
            else if authType == .register {
                currentOp = client?.getRegisterSession(completion: callback)
            }
        }
    }
    
    /**
     Encapsulates network request handling.
     
     - Shows "working" overlay as long as the request goes.
     - Displays errors to the user in a `LabelRow` above the username.
     - Calls through to your provided success callback, if there is no error
       and there actually is a returned object.
     - If there is a 401 error, that's most probably due to a multi-stage authentication, so if that happens,
       the next stage request is triggered instead of anything else.
     
     - parameter success: Callback in case of successful request.
     - parameter value: The data returned by the server.
     - returns: Completion callback suitable for some `MXRestClient` methods.
    */
    private func opCompletion<T>(_ success: @escaping (_ value: T) -> Void)
        -> ((_ response: MXResponse<T>) -> Void) {
            
        workingOverlay.isHidden = false

        return { response in
            self.currentOp = nil

            let userInfo = (response.error as NSError?)?.userInfo

            // We get 401's when the authentication is multi-stage and we didn't complete all stages.
            if (userInfo?[AFNetworkingOperationFailingURLResponseErrorKey] as? HTTPURLResponse)?.statusCode == 401 {
                if let data = userInfo?[MXHTTPClientErrorResponseDataKey] as? [AnyHashable: Any],
                    let session = MXAuthenticationSession(fromJSON: data) {

                    self.authSession = self.filterAuthSession(session)

                    if self.nextStage != nil {

                        // Do next stage.
                        DispatchQueue.main.async { self.onDone() }

                        return
                    }
                }
            }

            self.workingOverlay.isHidden = true
            
            if response.isFailure || response.error != nil || response.value == nil {
                if response.error != nil {
                    self.showError(response.error!)
                }
                else if response.value == nil {
                    self.showError("Server didn't return anything!".localize())
                }
                else {
                    self.showError()
                }
            }
            else {
                success(response.value!)
            }
        }
    }

    /**
     Adds an account to the account manager, if not already in it.
     
     Informs the delegate about successful sign in.
     
     - parameter credentials: The `MXCredentials` as returned by the server.
     */
    private func onSuccesfulSignIn(_ credentials: MXCredentials) {
        print("[\(String(describing: type(of: self)))] credentials accessToken=\(credentials.accessToken ?? "nil") deviceId=\(credentials.deviceId ?? "nil") homeServer=\(credentials.homeServer ?? "nil") homeServerName=\(credentials.homeServerName() ?? "nil") userId=\(credentials.userId ?? "nil")")

        guard let manager = MXKAccountManager.shared() else {
            return // Something's very very wrong if this happens.
        }

        // Sanity check: check whether the user is not already logged in with this ID.
        var account: MXKAccount? = manager.account(forUserId: credentials.userId)
        account?.isDisabled = false // Existing account might be disabled.

        if account == nil {
            // Report the new account in account manager.
            account = MXKAccount(credentials: credentials)

            if let account = account {
                account.identityServerURL = client?.identityServer

                manager.addAccount(account, andOpenSession: true)
            }
        }

        if let account = account {
            // Need to wait for a session before setting cross signing and display name!
            account.performOnSessionReady { [weak self] session in
                if let password = self?.password {
                    // Opportunistically try to enable cross signing, if not done, yet.
                    // Fail silently, if not possible here.
                    // User will be asked to enable cross-signing on device verification, otherwise.
                    VerificationManager.shared.enableCrossSigning(session, password)
                }

                if self?.useKeyBackup ?? false,
                    let password = self?.keyBackupPassword ?? self?.password
                {
                    // Opportunistically try to restore the latest backup or create a new key backup,
                    // if not enabled, yet.
                    // The password is either a user-provided password which is an
                    // explicit password for the key backup, or, if none provided,
                    // the user account password.
                    // Fail silently, if not possible here.
                    // User needs to enable key backup in my-device scene, otherwise,
                    // or will automatically be connected to key backup, when doing
                    // an interactive verification with a device which already has it.
                    VerificationManager.shared.restoreOrAddNewBackup(session, password)
                }

                // Use whatever the user entered as display name.
                // (Should be filled on register, only.)
                if let displayName = self?.userEnteredName, !displayName.isEmpty {
                    account.setUserDisplayName(displayName, success: nil, failure:nil)
                }
            }
        }

        // The delegate should probably dismiss us now.
        delegate?.didLogIn(with: username)
    }

    /**
     Shows an error, explaining that this app doesn't support that server's authentication process.
     */
    private func showUnsupportedError() {
        let tx1: String

        if self.authType == .login {
            tx1 = "This app currently doesn't support this server's sign in process."
                .localize()
        }
        else {
            tx1 = "This app currently doesn't support this server's sign up process."
                .localize()
        }

        let tx2 = "Please choose another server!".localize()

        showError("\(tx1)\n\(tx2)")
    }
    
    /**
     Shows an `Error`s localized description to the user.
     
     - parameter error: The occured `Error` to show.
    */
    private func showError(_ error: Error) {
        showError(error.localizedDescription, true)
    }
    
    /**
     Shows an error text to the user.
     
     - parameter error: The error text to show.
    */
    private func showError(_ error: String) {
        showError(error, true)
    }
    
    /**
     Shows or hides an error to the user.
     
     If `toggle` is true, a standard text will be displayed.
     
     - parameter toggle: Show if true, hide if false.
    */
    private func showError(_ toggle: Bool) {
        showError(nil, toggle)
    }
    
    /**
     Shows an error text to the user, if `toggle` is true.
     
     - parameter error: The error text to show. If `nil`, a standard text will
        be displayed. Default is `nil`.
     - parameter toggle: Show if true, hide if false. Default is `true`.
     */
    private func showError(_ error: String? = nil, _ toggle: Bool = true) {
        if let errors = form.rowBy(tag: "errors") {
            if toggle {
                errors.title = error ?? "No details available.".localize()
                errors.reload()
                errors.hidden = false
            }
            else {
                errors.hidden = true
            }

            errors.evaluateHidden()
        }
    }
    
    /**
     Shows `RecaptchaAndTermsViewController` modaly.
    */
    private func showRecaptchaAndTerms() {
        let homeServer = client?.homeserver
        let recaptchaKey = (recaptchaSecret ?? "").isEmpty
            ? (authSession?.params?[kMXLoginFlowTypeRecaptcha] as? [String: String])?["public_key"]
            : nil
        let terms = termsAccepted
            ? nil
            : MXLoginTerms(fromJSON: authSession?.params[kMXLoginFlowTypeTerms] as? [AnyHashable : Any])

        present(RecaptchaAndTermsViewController(homeServer, recaptchaKey, terms, delegate: self), animated: true)
    }

    /**
     - parameter type: An array of `MXLoginFlowType`s as `String`s.
     - returns: if a given list of flow types is supported by this implementation or not.
     */
    private func isSupported(_ types: [String]?) -> Bool {
        if let types = types {
            for type in types {
                if !isSupported(type) {
                    return false
                }
            }
            
            
            return true
        }
        
        return false
    }
    
    /**
     - parameter type: A `MXLoginFlowType` as `String`.
     - returns: if a given flow type is supported by this implementation or not.
     */
    private func isSupported(_ type: String) -> Bool {
        switch MXLoginFlowType(identifier: type) {
        case .dummy, .password, .recaptcha, .other(kMXLoginFlowTypeTerms):
            return true
        default:
            print("[AuthenticationViewController] Login flow type \"\(type)\" is not supported.")
            return false
        }
    }

    /**
     - parameter authSession: An `MXAuthenticationSession` object, hopefully
     containing information about the login flows the server supports.
     - returns: `nil`, if no flow is supported or the server didn't provide a
     list of supported flows, the `authSession` object if all the flows are
     supported, or a copy of the `authSession` object which only contains the
     supported flows.
     */
    private func filterAuthSession(_ authSession: MXAuthenticationSession) -> MXAuthenticationSession? {
        
        var flows = [MXLoginFlow]()
        
        for flow in authSession.flows {
            if let type = flow.type {
                if isSupported(type) {
                    if flow.stages == nil || flow.stages.isEmpty {
                        flow.stages = [type]
                        flows.append(flow)
                    }
                    else {
                        if isSupported(flow.stages) {
                            flows.append(flow)
                        }
                    }
                }
            }
            else {
                if isSupported(flow.stages) {
                    flows.append(flow)
                }
            }
        }
        
        if flows.isEmpty {
            return nil
        }
        
        if flows.count == authSession.flows.count {
            return authSession
        }
        
        let cleanedSession = MXAuthenticationSession()
        cleanedSession.completed = authSession.completed
        cleanedSession.session = authSession.session
        cleanedSession.params = authSession.params
        cleanedSession.flows = flows
        
        return cleanedSession
    }
}
