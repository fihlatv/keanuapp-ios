//
//  WelcomeViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 13.03.20.
//

import UIKit

open class WelcomeViewController: UIViewController {

    @IBAction open func showNext() {
        navigationController?.pushViewController(
            UIApplication.shared.theme.createAddAccountViewController(),
            animated: true)
    }
}
