//
//  AddAccountViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

open class AddAccountViewController: UIViewController, AuthenticationViewControllerDelegate,
EnablePushViewControllerDelegate {

    open var calledInline = false

    var lastType = AuthenticationViewController.AuthType.login

    lazy var signInVc: AuthenticationViewController = {
        let vc = AuthenticationViewController(.login)
        vc.delegate = self
        
        return vc
    }()

    lazy var signUpVc: AuthenticationViewController = {
        let vc = AuthenticationViewController(.register)
        vc.delegate = self
        
        return vc
    }()

    open override func viewDidLoad() {
        super.viewDidLoad()

        // Button and title only shown when called from MeViewController.

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(done))

        navigationItem.title = "Add Account".localize()
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.setNavigationBarHidden(!calledInline, animated: animated)
    }


    // MARK: Actions

    /**
     Callback for "Create an Account" button.
     */
    @IBAction func signUp() {
        lastType = .register
        navigationController?.pushViewController(signUpVc, animated: true)
    }
    
    /**
     Callback for "Sign into Existing Account" button.
     */
    @IBAction func signIn() {
        lastType = .login
        navigationController?.pushViewController(signInVc, animated: true)
    }


    // MARK: EnablePushViewControllerDelegate

    /**
     If we were called inline, dismiss ourselves.

     If we were started from the app delegate, call that to jump to the main
     flow.
     */
    @IBAction open func done() {
        if calledInline {
            self.navigationController?.dismiss(animated: true)
        }
        else {
            navigationController?.popToRootViewController(animated: true)

            // Leave the Onboarding scene flow, switch to the main flow instead.
            UIApplication.shared.startMainFlow(forwardIfNoFriends: true)
        }
    }


    // MARK: AuthenticationViewControllerDelegate

    public func didLogIn(with userId: String) {
        // If we don't have push permissions, ask for them.
        PushManager.shared.hasPushPermissions { [weak self] hasPermissions in
            if !hasPermissions {
                let vc = UIApplication.shared.theme.createEnablePushViewController()
                vc.delegate = self

                self?.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self?.done()
            }
        }
    }
}
