//
//  BaseTheme.swift
//  Keanu
//
//  Created by N-Pex on 06.02.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

open class BaseTheme: NSObject, Theme {

    /**
     Singleton instance.
     */
    public static let shared = BaseTheme()

    open func createWelcomeViewController() -> WelcomeViewController {
        return WelcomeViewController(nibName: String(describing: WelcomeViewController.self),
                                     bundle: Bundle(for: type(of: self)))
    }

    open func createAddAccountViewController() -> AddAccountViewController {
        return AddAccountViewController(nibName: String(describing: AddAccountViewController.self),
                                        bundle: Bundle(for: type(of: self)))
    }

    open func createEnablePushViewController() -> EnablePushViewController {
        return EnablePushViewController(nibName: String(describing: EnablePushViewController.self),
                                        bundle: Bundle(for: type(of: self)))
    }
    
    public func createMainViewController() -> MainViewController {
        return MainViewController()
    }

    public func createChatListViewController() -> ChatListViewController {
        return ChatListViewController(nibName: String(describing: ChatListViewController.self),
                                bundle: Bundle(for: type(of: self)))
    }

    public func createDiscoverViewController() -> DiscoverViewController {
        return DiscoverViewController()
    }

    public func createMeViewController() -> MeViewController {
        return MeViewController()
    }

    open func createRoomViewController() -> RoomViewController {
        let vc = RoomViewController()
        vc.hidesBottomBarWhenPushed = true
        return vc
    }

    public func createRoomSettingsViewController() -> RoomSettingsViewController {
        return RoomSettingsViewController(nibName: String(describing: RoomSettingsViewController.self),
                                          bundle: Bundle(for: type(of: self)))
    }

    open func createProfileViewController() -> ProfileViewController {
        return ProfileViewController()
    }

    public func createStoryViewController() -> StoryViewController {
        return StoryViewController(nibName: String(describing: StoryViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func createStoryAddMediaViewController() -> StoryAddMediaViewController {
        return StoryAddMediaViewController(nibName: String(describing: StoryAddMediaViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func createStoryGalleryViewController() -> StoryGalleryViewController {
        return StoryGalleryViewController(nibName: String(describing: StoryGalleryViewController.self),
                                   bundle: Bundle(for: type(of: self)))
    }

    public func createStoryEditorViewController() -> StoryEditorViewController {
        return StoryEditorViewController(nibName: String(describing: StoryEditorViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }

    public func createStickerPackViewController() -> StickerPackViewController {
        return StickerPackViewController()
    }

    public func createPickStickerViewController() -> PickStickerViewController {
        return PickStickerViewController(nibName: String(describing: PickStickerViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }

    public func createChooseFriendsViewController() -> ChooseFriendsViewController {
        return ChooseFriendsViewController(nibName: String(describing: ChooseFriendsViewController.self),
                                           bundle: Bundle(for: type(of: self)))
    }

    public func createAddFriendViewController() -> AddFriendViewController {
        return AddFriendViewController(nibName: String(describing: AddFriendViewController.self),
                                       bundle: Bundle(for: type(of: self)))
    }

    public func createShowQrViewController() -> ShowQrViewController {
        return ShowQrViewController(nibName: String(describing: ShowQrViewController.self),
                                    bundle: Bundle(for: type(of: self)))
    }

    public func createQrScanViewController() -> QrScanViewController {
        return QrScanViewController(nibName: String(describing: QrScanViewController.self),
                                    bundle: Bundle(for: type(of: self)))
    }

    public func createVerificationViewController() -> VerificationViewController {
        return VerificationViewController(nibName: String(describing: VerificationViewController.self),
                                          bundle: Bundle(for: type(of: self)))
    }

    public func createPhotoStreamViewController() -> PhotoStreamViewController {
        return PhotoStreamViewController(nibName: String(describing: PhotoStreamViewController.self),
                                         bundle: Bundle(for: type(of: self)))
    }
    
    public func createChatBubble(type: BubbleViewType, rect: CGRect) -> CGPath? {
        return BubbleLayer.createPath(type: type, frame: rect, lineWidth: [BubbleViewType.incoming, BubbleViewType.outgoing].contains(type) ? 2 : 0)
    }
}
