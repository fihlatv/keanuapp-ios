//
//  AvatarPicker.swift
//  Keanu
//
//  Created by N-Pex on 17.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import Photos
import MobileCoreServices

/**
 A class that handles picking an image from the photo library and setting it as an avatar for the given account.
 */
open class AvatarPicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /**
     Singleton instance. Will load the friends list from disk on first access.
     */
    public static let shared: AvatarPicker = {
        return AvatarPicker()
    }()
    
    weak var account: MXKAccount?
    weak var avatar: AvatarView?
    var viewController: UIViewController?

    private lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [UtiHelper.typeImage]
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .popover
        
        return imagePicker
    }()
    
    /**
     Display an image picker for the account in the view controller given. Also, when done, update the given AvatarView (if any) with the new image.
     */
    open class func showPickerFor(account: MXKAccount, avatarView: AvatarView?, inViewController viewController: UIViewController) {
        shared.account = account
        shared.avatar = avatarView
        shared.viewController = viewController
        shared.showPicker()
    }
    
    private func showPicker() {
        guard let avatar = avatar, let _ = account, let vc = viewController else { return }
        imagePicker.popoverPresentationController?.sourceView = avatar
        imagePicker.popoverPresentationController?.sourceRect = avatar.bounds
        
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            vc.present(imagePicker, animated: true)
            
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization() { newStatus in
                if newStatus == .authorized {
                    vc.present(self.imagePicker, animated: true)
                }
            }
            
        case .restricted:
            AlertHelper.present(
                vc,
                message: "Sorry, you are not allowed to view the photo library.".localize(),
                title: "Access Restricted".localize())
            
        case .denied:
            AlertHelper.present(
                vc, message:
                "Please go to the Settings app to grant this app access to your photo library, if you want to upload photos or videos."
                    .localize(),
                title: "Access Denied".localize())
        @unknown default:
            break
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
        info: [UIImagePickerController.InfoKey : Any]) {
        
        if  let account = account,
            var image = (info[.editedImage] ?? info[.originalImage]) as? UIImage {
            
            image = MXKTools.forceImageOrientationUp(image)
            
            // Upload picture
            let uploader = MXMediaManager.prepareUploader(
                withMatrixSession: account.mxSession, initialRange: 0, andRange: 1)
            
            uploader?.uploadData(
                image.jpegData(compressionQuality: 0.5),
                filename: nil, mimeType: UtiHelper.mimeJpeg,
                success: { url in
                    
                    // Set as avatar.
                    account.setUserAvatarUrl(url, success: {
                        self.avatar?.load(account: account)
                        
                    }, failure: { error in
                        if let vc = self.viewController {
                            AlertHelper.present(vc, message: error?.localizedDescription)
                        }
                    })
            },
                failure: { error in
                    if let vc = self.viewController {
                        AlertHelper.present(vc, message: error?.localizedDescription)
                    }
            })
        }
        
        picker.dismiss(animated: true)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
