//
//  Link3pidViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 18.09.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import Eureka

/**
 Scene to link a third-party ID to a Matrix account.

 Currently supported are e-mail addresses and mobile phone numbers.
 */
open class Link3pidViewController: FormViewController {

    open weak var account: MXKAccount?

    private typealias MediumSelection = SelectableSection<ListCheckRow<String>>

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    private lazy var clientSecret: String = {
        return MXTools.generateSecret()
    }()

    private var sendAttempt: UInt = 0

    private var sid: String?

    override open func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Link another ID".localize()

        form
            +++ MediumSelection("Type".localize(), selectionType: .singleSelection(enableDeselection: false)) {
                $0.tag = "type"
            }

            <<< ListCheckRow<String>("type_email") {
                $0.title = MyProfileViewController.label(for3PidMedium: kMX3PIDMediumEmail)
                $0.selectableValue = kMX3PIDMediumEmail
                $0.value = kMX3PIDMediumEmail
            }

            <<< ListCheckRow<String>("type_phone") {
                $0.title = MyProfileViewController.label(for3PidMedium: kMX3PIDMediumMSISDN)
                $0.selectableValue = kMX3PIDMediumMSISDN
            }

            +++ Section() {
                $0.tag = "id_section"
            }

            <<< LabelRow("error") {
                $0.hidden = true
            }
            .cellUpdate { cell, row in
                cell.textLabel?.textColor = .systemRed
                cell.textLabel?.numberOfLines = 0
            }

            <<< EmailRow("email") {
                $0.placeholder = "john.doe@example.com"

                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnDemand

                $0.hidden = .function(["type_email", "type_phone"]) {
                    ($0.sectionBy(tag: "type") as? MediumSelection)?.selectedRow()?.value != kMX3PIDMediumEmail
                }
                $0.disabled = $0.hidden
            }

            <<< PhoneRow("phone") {
                $0.placeholder = "+1234567890"

                $0.add(rule: RuleRequired())
                $0.add(rule: RuleClosure<String>(closure: {
                    return self.msisdn($0)?.count ?? 0 < 4
                        ? ValidationError(msg: "A mobile phone number must have at least 4 digits.".localize())
                        : nil
                }))

                $0.validationOptions = .validatesOnDemand

                $0.hidden = .function(["type_email", "type_phone"]) {
                    ($0.sectionBy(tag: "type") as? MediumSelection)?.selectedRow()?.value != kMX3PIDMediumMSISDN
                }
                $0.disabled = $0.hidden
            }

            +++ ButtonRow("add_button") {
                $0.title = "Add".localize()
            }
            .onCellSelection(requestValidation)

            <<< IntRow("token") {
                $0.title = "Validation Code".localize()

                $0.hidden = true
                $0.disabled = true

                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnDemand
            }
            .cellUpdate { cell, row in
                cell.titleLabel?.textColor = row.isValid ? .keanuLabel : .systemRed
            }

            <<< ButtonRow("continue_button") {
                $0.title = "Continue".localize()

                $0.hidden = true
            }
    }

    /**
     First step in linking a third-party ID.

     Evaluate form, and if successful, either request e-mail validation or SMS validation.

     When that was successful, Show instructions to the user and wait for user
     to continue process.

     - parameter cell: The calling button cell.
     - parameter row: The calling button row.
    */
    private func requestValidation(_ cell: ButtonCell, _ row: ButtonRow) {
        if let error = form.validate().first {
            return setError(error.msg)
        }

        setError(nil)

        let medium = (form.sectionBy(tag: "type") as? MediumSelection)?.selectedRow()?.value

        if medium == kMX3PIDMediumEmail {
            guard let valueRow = form.rowBy(tag: "email") as? EmailRow,
                let email = valueRow.value?.trimmingCharacters(in: .whitespacesAndNewlines) else {
                // This should not happen, as validation already took place.
                return
            }

            workingOverlay.isHidden = false

            account?.mxSession?.identityService?.requestEmailValidation(
                email, clientSecret: clientSecret, sendAttempt: sendAttempt,
                nextLink: nil)
            { response in
                self.sendAttempt += 1
                self.sid = response.value

                self.workingOverlay.isHidden = true

                if let error = response.error {
                    return self.setError(error.localizedDescription)
                }

                valueRow.disabled = true
                valueRow.evaluateDisabled()

                row.hidden = true
                row.evaluateHidden()

                if let section = self.form.sectionBy(tag: "id_section") {
                    section.footer = HeaderFooterView(title:
                        "An e-mail was sent to the given address.\n\nPlease follow the instructions in it and return here, when completed, to press \"Continue\".".localize())
                    section.reload()
                }

                if let cont = self.form.rowBy(tag: "continue_button") as? ButtonRow {
                    cont.hidden = false
                    cont.evaluateHidden()
                    cont.onCellSelection({ _, _ in
                        self.add3pid()
                    })
                }
            }
        }
        else if medium == kMX3PIDMediumMSISDN {
            guard let valueRow = form.rowBy(tag: "phone") as? PhoneRow,
                let phone = msisdn(valueRow.value) else {
                // This should not happen, as validation already took place.
                return
            }

            workingOverlay.isHidden = false

            account?.mxSession?.identityService?.requestPhoneNumberValidation(
                phone, countryCode: "", clientSecret: clientSecret,
                sendAttempt: sendAttempt, nextLink: nil)
            { response in
                self.sendAttempt += 1

                self.workingOverlay.isHidden = true

                guard let (sid, msisdn) = response.value else {
                    return self.setError(response.error?.localizedDescription)
                }

                self.sid = sid

                row.value = msisdn

                valueRow.disabled = true
                valueRow.evaluateDisabled()

                row.hidden = true
                row.evaluateHidden()

                if let section = self.form.sectionBy(tag: "id_section") {
                    section.footer = HeaderFooterView(title:
                        "An SMS was sent to the given number.\n\nEnter the received validation code in the below field and press \"Continue\".".localize())
                    section.reload()
                }

                if let token = self.form.rowBy(tag: "token") {
                    token.hidden = false
                    token.evaluateHidden()

                    token.disabled = false
                    token.evaluateDisabled()
                }

                if let cont = self.form.rowBy(tag: "continue_button") as? ButtonRow {
                    cont.hidden = false
                    cont.evaluateHidden()

                    cont.onCellSelection({ _, _ in
                        self.validateSmsToken()
                    })
                }
            }
        }
    }

    /**
     Last step in linking a third-party ID:

     Report the successful link between the Matrix ID and 3pid to the identity server(s).

     When successful, leave this scene, else show error.

     The scene before should auto-update it's list of 3pids on #viewDidAppear.
    */
    private func add3pid() {
        guard let sid = sid else {
            return
        }

        workingOverlay.isHidden = false

        account?.mxRestClient.addThirdPartyIdentifier(sid, clientSecret: clientSecret, bind: true) { response in
            if let error = response.error {
                self.workingOverlay.isHidden = true

                return self.setError(error.localizedDescription)
            }

            self.navigationController?.popViewController(animated: true)
        }
    }

    /**
     Second step for MSISDN linking.

     Sends the user-provided token, which she must have received via SMS to the
     server. If successful, continues with step 3: #add3pid.
    */
    private func validateSmsToken() {
        if form.validate().count > 0 {
            // Don't call #setError, because that row is far away from the token
            // row, so the user wouldn't understand that.
            // Instead, the token row becomes red.
            return
        }

        guard let token = (form.rowBy(tag: "token") as? IntRow)?.value,
            let sid = sid else {
            return
        }

        workingOverlay.isHidden = false

        account?.mxSession?.identityService?.submit3PIDValidationToken(
            String(token), medium: kMX3PIDMediumMSISDN, clientSecret: clientSecret,
            sid: sid) { response in
            if let error = response.error {
                self.workingOverlay.isHidden = true

                return self.setError(error.localizedDescription)
            }

            self.add3pid()
        }
    }

    /**
     Shows the row tagged "error" with the provided string, if the provided string
     is non-empty, else hides it.

     - parameter msg: The error message.
    */
    private func setError(_ msg: String?) {
        guard let errorRow = self.form.rowBy(tag: "error") else {
            return
        }

        errorRow.title = msg

        errorRow.hidden = Condition(booleanLiteral: msg?.isEmpty ?? true)
        errorRow.evaluateHidden()

        errorRow.updateCell()
    }

    /**
     Remove all characters from a string besides the ones which are valid in
     MSISDNS, which are digits and the + sign.

     - parameter value: A string possibly containing a MSISDN.
     - return: The MSISDN found.
    */
    private func msisdn(_ value: String?) -> String? {
        return value?.filter({ "+0123456789".contains($0) })
    }
}
