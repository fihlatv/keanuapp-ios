//
//  MyDevicesViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 26.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

open class MyDevicesViewController: DevicesViewController {

    open weak var session: MXSession?

    private var thisDevicesId: String?

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    /**
     Delete action for table list row. Deletes a device after confirmation.
     */
    private lazy var deleteAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .destructive,
            title: "Delete".localize())
        { [weak self] action, indexPath in
            if self?.devices.count ?? 0 > indexPath.row,
                let userId = self?.session?.myUser?.userId,
                let device = self?.devices[indexPath.row] {

                self?.workingOverlay.isHidden = false

                self?.session?.matrixRestClient?.getSession(toDeleteDevice: device.deviceId) { response in
                    if response.isSuccess,
                        let authSession = response.value,
                        let session = authSession.session {

                        var passwordSupported = false

                        for flow in authSession.flows {
                            if flow.type == kMXLoginFlowTypePassword || flow.stages.contains(kMXLoginFlowTypePassword) {
                                passwordSupported = true
                                break
                            }
                        }

                        if passwordSupported {
                            self?.present(PasswordViewController.instantiate(
                                title: "Delete".localize(),
                                message: "To delete the device \"%\", please provide your password.\n"
                                    .localize(value: device.displayName),
                                okLabel: "Delete".localize()) { vc in
                                    guard let password = vc.password else {
                                        self?.workingOverlay.isHidden = true
                                        return
                                    }

                                    self?.session?.matrixRestClient?.deleteDevice(
                                        device.deviceId,
                                        authParameters: [
                                            "session": session,
                                            "user": userId,
                                            "password": password,
                                            "type": kMXLoginFlowTypePassword,
                                    ]) { response in
                                        guard let self = self else {
                                            return
                                        }

                                        if response.isFailure {
                                            AlertHelper.present(self, message:
                                                "Device deletion failed: %"
                                                    .localize(value: response.error?.localizedDescription ?? "Unkown server error".localize()))
                                        }
                                        else {
                                            self.reloadDevices()
                                        }

                                        self.workingOverlay.isHidden = true
                                    }
                            }, animated: true)
                        }
                        else if let self = self {
                            AlertHelper.present(self, message:
                                "Cannot delete devices because password-based authentication is not supported by the server.".localize())
                            self.workingOverlay.isHidden = true
                        }
                    }
                    else if let self = self {
                        AlertHelper.present(self, message:
                            "Cannot delete devices at the moment: %"
                                .localize(value: response.error?.localizedDescription ?? "Unkown server error".localize()))
                        self.workingOverlay.isHidden = true
                    }
                }
            }

            DispatchQueue.main.async {
                self?.tableView.setEditing(false, animated: true)
            }
        }

        return action
    }()

    override open func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(KeyBackupCell.nib, forCellReuseIdentifier: KeyBackupCell.defaultReuseId)

        navigationItem.title = session?.myUser.friendlyName
    }

    open override func devicesUpdated(_ notification: Notification) {
        if let userId = session?.myUser?.userId,
            (notification.userInfo as? [String: [MXDeviceInfo]])?.keys.contains(userId) ?? false {

            reloadDevices()
        }
    }

    open override func deviceUpdated(_ notification: Notification) {
        if let userId = session?.myUser?.userId,
            let device = notification.object as? MXDeviceInfo,
            device.userId == userId {

            reloadDevices()
        }
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : devices.count
    }

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row > 0 {
            return KeyBackupCell.height
        }

        return super.tableView(tableView, heightForRowAt: indexPath)
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0,
                let session = session,
                let cell = dequeueHeaderCell(indexPath) {

                return cell.apply(session.myUser, session, countKeys())
            }
            else if indexPath.row == 1,
                let cell = tableView.dequeueReusableCell(withIdentifier: KeyBackupCell.defaultReuseId,
                                                     for: indexPath) as? KeyBackupCell {
                return cell.apply(session)
            }
        }
        else {
            if let cell = dequeueDeviceCell(indexPath) {

                return cell.apply(device: devices[indexPath.row],
                                  session: session,
                                  isThisDevice: thisDevicesId != nil && devices[indexPath.row].deviceId == thisDevicesId)
            }
        }

        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    override open func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // Header and this device cannot be deleted.
        if indexPath.row == 0 {
            return []
        }

        return [deleteAction]
    }

    /**
     Sorter for `MXDeviceInfo` classes.

     - Sort order:
     - This device
     - "Unkown" (new) devices
     - Verified devices
     - Unverified devices
     - Blocked devices
     - Second sort order:
     - Ascending by device ID
     */
    override open func deviceSorter(_ a: MXDeviceInfo, _ b: MXDeviceInfo) -> Bool {
        if a.deviceId == thisDevicesId {
            return true
        }

        if b.deviceId == thisDevicesId {
            return false
        }

        return super.deviceSorter(a, b)
    }

    open override func reloadDevices() {
        if let userId = session?.myUser?.userId,
            let crypto = session?.crypto {

            thisDevicesId = crypto.store?.deviceId()

            if let devices = crypto.store?.devices(forUser: userId) {
                self.devices = devices.map() { $1 }.sorted(by: deviceSorter(_:_:))
            }

            crypto.downloadKeys([userId], forceDownload: true, success: { [weak self] map, crossSigningInfo in
                guard let self = self else {
                    return
                }

                self.devices.removeAll(keepingCapacity: true)

                for deviceId in map?.deviceIds(forUser: userId) ?? [] {
                    if let device = map?.object(forDevice: deviceId, forUser: userId) {
                        self.devices.append(device)
                    }
                }

                self.devices.sort(by: self.deviceSorter(_:_:))

                self.tableView.reloadData()
            }, failure: nil)
        }
    }
}
