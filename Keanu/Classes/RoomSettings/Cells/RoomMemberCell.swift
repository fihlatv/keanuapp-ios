//
//  RoomMemberCell.swift
//  Keanu
//
//  Created by N-Pex on 24.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

/**
 A cell used to display room members.
 */
open class RoomMemberCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @objc @IBOutlet open weak var avatarImage:AvatarView!
    @objc @IBOutlet open weak var titleLabel:UILabel!
    @objc @IBOutlet open weak var detailLabel:UILabel!
    @objc @IBOutlet open weak var avatarImageCheckmark:UIView!
    @objc @IBOutlet open weak var unknownDevicesIndicator:UIView!

    open func render(member:MXRoomMember, room:MXRoom) {
        let friend = FriendsManager.shared.getOrCreate(member, room.mxSession)
        avatarImage.load(friend: friend)
        titleLabel.text = friend.name
        detailLabel.text = friend.friendlyPresence

        if member.membership == .invite {
            detailLabel.text = "Invited".localize()
        }
        
        avatarImageCheckmark.isHidden = !room.isModerator(userId: member.userId)

        if room.myUser?.userId == member.userId {
            // String to append to the current user (name) in room members view.
            titleLabel.text?.append(" (You)".localize())
        }

        // If we have any devices in "unknown" state for the user, show a small indicator.
        if let devices = room.mxSession?.crypto.deviceList.storedDevices(forUser: member.userId), devices.contains(where: {
            !($0.trustLevel?.isVerified ?? false) && $0.trustLevel?.localVerificationStatus == .unknown
        }) {
            unknownDevicesIndicator.isHidden = false
        }
        else {
            unknownDevicesIndicator.isHidden = true
        }
    }
    
    open override func prepareForReuse() {
        super.prepareForReuse()

        avatarImage.image = nil
        avatarImageCheckmark.isHidden = true
    }
}
