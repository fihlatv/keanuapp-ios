//
//  RoomMembersDataSource.swift
//  Keanu
//
//  Created by N-Pex on 19.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

/**
 Data source for all room members.
 
 Use the delegate to customize cell rendering and table view animations.
 */
open class RoomMembersDataSource: NSObject, UITableViewDataSource {
    
    public weak var delegate: RoomMembersDataSourceDelegate?
    public var section: UInt = 0
    
    /**
     The members data that is displayed by this data source.
     */
    var roomMembers = [MXRoomMember]()
    
    /**
     Mapping from userId to MXRoomMember. Used when updating list to see if a member is added etc.
     */
    var roomMemberMapping = [String: MXRoomMember]()
    
    var room: MXRoom
    weak var roomTimeline: MXEventTimeline?
    var roomListener: Any?
    
    // Notification center listener objects
    open var observers: [NSObjectProtocol] = []
    
    init(room: MXRoom) {
        self.room = room
        super.init()
        
        // Add listener to room events, so we can dynamically update the table view when members are joined/invited/kicked etc.
        room.liveTimeline { [weak self] timeline in
            guard let self = self, let timeline = timeline else {
                return
            }
            self.roomTimeline = timeline
            self.roomListener = timeline.listenToEvents([.roomMember, .roomThirdPartyInvite, .roomPowerLevels])
            { [weak self] event, direction, state in
                guard let self = self,
                    direction == .forwards else {
                        return
                }

                switch event.eventType {
                case .roomMember:
                    // Note: use timeline.state, not state. The "state" param contains the state BEFORE the event.
                    guard let userId = event.stateKey,
                        let member = timeline.state.members.member(withUserId: userId) else {
                            return
                    }

                    self.handleMember(member: member)
                    self.resort()
                    self.delegate?.didChangeAllRows()

                case .roomPowerLevels:
                    if let delegate = self.delegate {
                        self.resort()
                        delegate.didChangeAllRows()
                    }

                case .roomThirdPartyInvite:
                    //TODO - How to handle these?
                    break

                default:
                    break
                }
            }
        }
        
        // Load room members, if needed.
        reloadData()
    }
    
    open func reloadData() {
        let callback = { [weak self] (members: MXRoomMembers?) in
            members?.members.forEach { self?.handleMember(member: $0) }

            self?.resort()

            self?.delegate?.didChangeAllRows()
        }

        room.members(
            { [weak self] members in
                self?.roomMembers.removeAll()

                callback(members)
            },
            lazyLoadedMembers: callback,
            failure: { error in
                print("[\(String(describing: type(of: self)))]#reloadData error=\(error?.localizedDescription ?? "")")
                // TODO
        })
    }
    
    /**
     Do the actual adding of a new member/removal of old member from the data set, based on the membership for the room member. First remove any old data (if present) then add the new.
     */
    open func handleMember(member: MXRoomMember) {
        let allowedMemberships: [MXMembership] = [.join, .invite]
        
        // Remove previous data for this user/member.
        if let previouslyAddedMember = roomMemberMapping[member.userId], let index = roomMembers.firstIndex(of: previouslyAddedMember) {
            roomMembers.remove(at: index)
            roomMemberMapping.removeValue(forKey: member.userId)
        }
        
        // Add the new data (based on the new membership value).
        guard allowedMemberships.contains(member.membership) else {
            return
        }

        roomMembers.append(member)
        roomMemberMapping[member.userId] = member
    }
    
    deinit {
        delegate = nil

        self.observers.forEach { NotificationCenter.default.removeObserver($0) }

        if let listener = roomListener {
            roomTimeline?.removeListener(listener)
            roomListener = nil
        }
    }
    
    /**
     Order first by admins,moderators,others, then by display name.
     */
    open func resort() {
        RoomMembersDataSource.sortMembers(members: &roomMembers, inRoom: room)
    }
    
    /**
     Order first by admins, moderators, others, then by friendly name.

     - parameter members: Array of room members to sort.
     - parameter room: The room, the members are in.
     */
    open class func sortMembers(members: inout [MXRoomMember], inRoom room: MXRoom) {
        members.sort { (m1, m2) -> Bool in
            switch (room.isAdmin(userId: m1.userId), room.isAdmin(userId: m2.userId)) {
            case (true, false):
                return true

            case (false, true):
                return false

            default:
                break
            }

            switch (room.isModerator(userId: m1.userId), room.isModerator(userId: m2.userId)) {
            case (true, false):
                return true

            case (false, true):
                return false

            default:
                break
            }

            // BUGFIX: Using `member2.displayname` directly, can lead to an
            // unexpected nil runtime exception. Additionally, we want to use the
            // friendly name (which can be user-defined), as much as possible
            // anyway.
            return FriendsManager.shared.getOrCreate(m1).name
                .compare(FriendsManager.shared.getOrCreate(m2).name) == .orderedAscending
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomMembers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = delegate?.createCell(at: indexPath, for: roomMembers[indexPath.row]) {
            return cell
        }

        return UITableViewCell() // Fallback to avoid crash
    }
    
    public func roomMember(at indexPath: IndexPath) -> MXRoomMember? {
        if indexPath.section == section, indexPath.row >= 0, indexPath.row < roomMembers.count {
            return roomMembers[indexPath.row]
        }

        return nil
    }
}


