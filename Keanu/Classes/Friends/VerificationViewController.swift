//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 01.07.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import KeanuCore
import MatrixSDK

open class VerificationViewController: UIViewController, QrScanViewControllerDelegate {

    public enum State {

        case incomingRequest(_ otherUserId: String, _ otherDeviceId: String)

        case waitingOnAccept

        case chooseMethod(canSas: Bool, canScan: Bool, _ qrCode: Data?)

        case waitingOnChallenge

        case showSas(_ otherUserId: String, _ otherDeviceId: String, _ emojis: String?)

        case qrScannedByOther

        case waitingOnConfirm

        case verified(_ otherUserId: String, _ otherDeviceId: String)

        case cancelled(_ reason: String?, byMe: Bool = false)

        case error(_ error: Error?)
    }

    private var transaction: MXKeyVerificationTransaction?
    private var keyVerificationRequest: MXKeyVerificationRequest?

    private var state: State?

    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!
    @IBOutlet weak var messageTv: UITextView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var actionBt1: UIButton!
    @IBOutlet weak var actionBt2: UIButton!

    private var token: NSKeyValueObservation?

    private var originalMessageTvTopInset: CGFloat?

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.title = "Verify Device".localize()

        // Finally render state which was given before the view was loaded.
        render()
    }

    open func render(_ transaction: MXKeyVerificationTransaction) {
        print("[\(String(describing: type(of: self)))] transaction=\(transaction)")

        self.transaction = transaction
        keyVerificationRequest = nil
        token = nil
        if let transaction = transaction as? MXSASTransaction {
            switch transaction.state {
            case MXSASTransactionStateIncomingShowAccept:
                state = .incomingRequest(transaction.otherUserId, transaction.otherDeviceId)

            case MXSASTransactionStateOutgoingWaitForPartnerToAccept:
                state = .waitingOnAccept

            case MXSASTransactionStateWaitForPartnerKey:
                state = .waitingOnChallenge

            case MXSASTransactionStateShowSAS:
                state = .showSas(
                    transaction.otherUserId, transaction.otherDeviceId,
                    transaction.sasEmoji?.map { e -> String in "\(e.emoji) \(e.name)" }.joined(separator: "\n\n"))

            case MXSASTransactionStateWaitForPartnerToConfirm:
                state = .waitingOnConfirm

            case MXSASTransactionStateVerified:
                state = .verified(transaction.otherUserId, transaction.otherDeviceId)

            case MXSASTransactionStateCancelled:
                state = .cancelled(transaction.reasonCancelCode?.humanReadable)

            case MXSASTransactionStateError:
                state = .error(transaction.error)

            default:
                break
            }
        }
        else if let transaction = transaction as? MXQRCodeTransaction {
            switch transaction.state {
            case .waitingOtherConfirm:
                state = .waitingOnConfirm

            case .verified:
                state = .verified(transaction.otherUserId, transaction.otherDeviceId)

            case .error:
                state = .error(transaction.error)

            case .cancelled, .cancelledByMe:
                state = .cancelled(transaction.reasonCancelCode?.humanReadable, byMe: transaction.state == .cancelledByMe)

            case .qrScannedByOther:
                state = .qrScannedByOther

            default:
                break
            }
        }

        render()
    }

    open func render(_ request: MXKeyVerificationRequest) {
        print("[\(String(describing: type(of: self)))] request=\(request), state=\(request.state)")

        transaction = nil
        keyVerificationRequest = request
        token = request.observe(\.state, changeHandler: requestStateChangeHandler)

        render(request.isFromMyDevice ? .waitingOnAccept : .incomingRequest(request.otherUser, request.fromDevice))
    }

    public func render(_ state: State? = nil) {
        if let state = state {
            self.state = state
        }

        print("[\(String(describing: type(of: self)))]#render state=\(String(describing: state))")

        DispatchQueue.main.async {
            guard self.isViewLoaded else {
                return
            }

            var title: String?
            var subtitle: String?
            var message: String?
            var image: UIImage?
            var action1: String?
            var action2: String?

            switch self.state {
            case .incomingRequest(let otherUserId, let otherDeviceId):
                title = "Incoming Verification Request".localize()
                message = "% wants to establish verification between this device and their device %.\n\nDo you want to start the verification process?"
                    .localize(values: otherUserId, otherDeviceId)
                action1 = "Accept".localize()

            case .waitingOnAccept:
                title = "Waiting".localize()
                subtitle = "Waiting on other party to accept…".localize()

            case .chooseMethod(let canSas, let canScan, let qrCode):
                title = "Choose Method".localize()

                switch (canSas, canScan, qrCode) {
                case (true, true, .some(qrCode)):
                    subtitle = "Show the other party this QR code, scan theirs or compare emoji instead".localize()

                case (true, true, .none):
                    subtitle = "Scan the other parties QR code or compare emoji instead".localize()

                case (false, true, .some(qrCode)):
                    subtitle = "Show the other party this QR code or scan theirs".localize()

                case (true, false, .some(qrCode)):
                    subtitle = "Show the other party this QR code or compare emoji instead".localize()

                default:
                    break // These should never happen.
                }

                if let qrCode = qrCode {
                    image = UIImage.qrCode(qrCode, self.image.bounds.size)
                }

                if canScan {
                    action1 = "Scan QR Code".localize()
                }

                if canSas {
                    action2 = "Compare Emoji".localize()
                }

            case .waitingOnChallenge:
                title = "Waiting".localize()
                subtitle = "Waiting on challenge negotiation…".localize()

            case .showSas(let otherUserId, let otherDeviceId, let emojis):
                title = "Verification Request".localize()
                subtitle = "Verify device % of user % by comparing these emoji:".localize(values:
                    otherDeviceId, otherUserId)
                message = emojis
                action1 = "Verify".localize()

            case .qrScannedByOther:
                title = "Scanned".localize()
                subtitle = "Is the other device showing the same shield?".localize()
                message = .verified_user // Material icon; see below.
                action1 = "No".localize()
                action2 = "Yes".localize()

            case .waitingOnConfirm:
                title = "Verified!".localize()
                subtitle = "Waiting on other party to confirm…".localize()
                message = .verified_user // Material icon; see below.

            case .verified(let otherUserId, let otherDeviceId):
                title = "Verified!".localize()
                subtitle = "You have verified device % of user % successfully!".localize(values:
                    otherDeviceId, otherUserId)
                message = "Secure messages with this user are end-to-end encrypted and cannot be read by third parties."
                    .localize()
                action1 = "OK".localize()

            case .cancelled(let reason, let byMe):
                title = "Cancelled".localize()
                subtitle = byMe ? "Request was cancelled".localize() : "The other party cancelled the request".localize()
                message = "Reason: %".localize(value: reason ?? "No reason available.".localize())
                action1 = "OK".localize()

            case .error(let error):
                title = "Error".localize()
                message = error?.localizedDescription ?? "No description available".localize()
                action1 = "OK".localize()

            default:
                break
            }

            self.titleLb.text = title
            self.titleLb.isHidden = title == nil

            self.subtitleLb.text = subtitle
            self.subtitleLb.isHidden = subtitle == nil

            self.messageTv.text = message
            self.messageTv.isHidden = message == nil

            // Special handling - shows Material Icon in this case.
            switch self.state {
            case .qrScannedByOther, .waitingOnConfirm:
                self.messageTv.font = .materialIcons(ofSize: 96)
                self.messageTv.textColor = .systemGreen
                self.messageTv.textAlignment = .center
                var inset = self.messageTv.textContainerInset
                if self.originalMessageTvTopInset == nil {
                    self.originalMessageTvTopInset = inset.top
                }
                inset.top = 100
                self.messageTv.textContainerInset = inset

            default:
                if !(message?.isEmpty ?? true) {
                    self.messageTv.font = .systemFont(ofSize: 14)
                    self.messageTv.textColor = .keanuLabel
                    self.messageTv.textAlignment = .natural
                    var inset = self.messageTv.textContainerInset
                    inset.top = self.originalMessageTvTopInset ?? 0
                    self.messageTv.textContainerInset = inset
                }
            }

            self.image.image = image
            self.image.isHidden = image == nil

            self.actionBt1.setTitle(action1)
            self.actionBt1.isHidden = action1 == nil

            self.actionBt2.setTitle(action2)
            self.actionBt2.toggle(action2 != nil)
        }
    }


    // MARK: Actions

    @objc func cancel() {
        keyVerificationRequest?.cancel(with: .user(), success: nil)
        transaction?.cancel(with: .user())

        done()
    }

    @IBAction func action1() {
        switch state {
        case .incomingRequest:
            if let keyVerificationRequest = keyVerificationRequest {
                render(.waitingOnAccept)

                self.keyVerificationRequest = nil // Avoid repetition on next tap.

                keyVerificationRequest.accept(
                    withMethods: [MXKeyVerificationMethodQRCodeScan, MXKeyVerificationMethodQRCodeShow, MXKeyVerificationMethodReciprocate, MXKeyVerificationMethodSAS],
                    success: { [weak self] in
                        print("[\(String(describing: type(of: self)))]#keyVerificationRequest.accept success")
                    },
                    failure: { [weak self] (error) in
                        print("[\(String(describing: type(of: self)))]#keyVerificationRequest.accept error=\(error)")

                        self?.render(.error(error))
                })
            }
            else {
                (transaction as? MXIncomingSASTransaction)?.accept()
            }

        case .chooseMethod(_, let canScan, _):
            if canScan {
                let vc = UIApplication.shared.theme.createQrScanViewController()
                vc.delegate = self

                navigationController?.pushViewController(vc, animated: true)
            }

        case .showSas:
            (transaction as? MXSASTransaction)?.confirmSASMatch()

        case .qrScannedByOther:
            (transaction as? MXQRCodeTransaction)?.otherUserScannedMyQrCode(false)

        case .verified:
            done()

        case .cancelled, .error:
            cancel()

        default:
            break
        }
    }

    @IBAction func action2() {
        switch state {
            case .chooseMethod(let canSas, _, _):
                if canSas,
                    let request = keyVerificationRequest ?? transaction?.manager?.pendingRequests.first
                {
                    startSas(request)
                }

        case .qrScannedByOther:
            (transaction as? MXQRCodeTransaction)?.otherUserScannedMyQrCode(true)

        default:
            break
        }
    }


    // MARK: QrScanViewControllerDelegate

    public func qrCodeFound(data: Data, controller: QrScanViewController) {
        if let transaction = self.transaction as? MXQRCodeTransaction,
            let otherQrCodeData = MXQRCodeDataCoder().decode(data)
        {
            transaction.userHasScannedOtherQrCodeData(otherQrCodeData)
            controller.navigationController?.popViewController(animated: true)
        }
        else {
            controller.showError("That QR code doesn't contain a valid verification code!".localize())
        }
    }


    // MARK: Private Methods

    private func requestStateChangeHandler(_ request: MXKeyVerificationRequest, _ change: NSKeyValueObservedChange<MXKeyVerificationRequestState>) {
        print("[\(String(describing: type(of: self)))] state changed to \(request.state), change=\(change)")

        switch request.state {
        case MXKeyVerificationRequestStateCancelled,
             MXKeyVerificationRequestStateExpired:
            render(.cancelled(request.reasonCancelCode?.humanReadable))

        case MXKeyVerificationRequestStateAccepted:
            render(.waitingOnChallenge)

        case MXKeyVerificationRequestStateReady:
            if let methods = request.acceptedMethods, !methods.isEmpty {
                if methods.count == 1 && methods.first == MXKeyVerificationMethodSAS {
                    render(.waitingOnChallenge)

                    if request.isFromMyDevice {
                        startSas(request)
                    }
                }
                else {
                    let t = request.manager?.qrCodeTransaction(withTransactionId: request.requestId)
                    transaction = t

                    var qrCode: Data? = nil

                    if let qrCodeData = t?.qrCodeData {
                        qrCode = MXQRCodeDataCoder().encode(qrCodeData)
                    }

                    render(.chooseMethod(
                        canSas: methods.contains(MXKeyVerificationMethodSAS),
                        // Logic seems reversed, but it seems the methods are the answers of the other device
                        // at this point.
                        canScan: (request.isFromMyDevice && methods.contains(MXKeyVerificationMethodQRCodeShow))
                            || (!request.isFromMyDevice && methods.contains(MXKeyVerificationMethodQRCodeScan)),
                        qrCode))
                }
            }
            else {
                let error = NSError(
                    domain: MXKeyVerificationErrorDomain,
                    code: Int(MXKeyVerificationUnsupportedMethodCode.rawValue),
                    userInfo: [NSLocalizedDescriptionKey: "No mutually supported verification methods.".localize()])

                render(.error(error))
            }

        case MXKeyVerificationRequestStateCancelledByMe:
            cancel()

        default:
            break
        }
    }

    private func done() {
        DispatchQueue.main.async {
            if let navC = self.navigationController {
                navC.dismiss(animated: true)
            }
            else {
                self.dismiss(animated: true)
            }
        }

        keyVerificationRequest = nil
        token = nil
        transaction = nil
    }

    private func startSas(_ request: MXKeyVerificationRequest) {
        DispatchQueue.global(qos: .userInitiated).async {
            request.manager.beginKeyVerification(
                from: request, method: MXKeyVerificationMethodSAS,
                success: { transaction in
                    // Nothing to do here. Will continue with the MXKeyVerificationTransaction flow.
                },
                failure: { [weak self] error in
                    self?.render(.error(error))
                })
        }
    }
}
