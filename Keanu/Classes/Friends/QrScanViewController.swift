//
//  QrScanViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 28.05.20.
//

import KeanuCore
import AVFoundation
import ZXingObjC

public protocol QrScanViewControllerDelegate {

    func qrCodeFound(data: Data, controller: QrScanViewController)
}

open class QrScanViewController: UIViewController, ZXCaptureDelegate {

    class var canScan: Bool {
        return AVCaptureDevice.authorizationStatus(for: .video) != .restricted
            && ZXCapture().hasBack()
    }

    open var delegate: QrScanViewControllerDelegate?

    open var message: String? {
        get {
            return messageLb.text
        }
        set {
            messageLb.text = newValue
        }
    }

    @IBOutlet weak var container: UIView!

    @IBOutlet weak var settingsBt: UIButton! {
        didSet {
            settingsBt.setTitle("Settings".localize())
            settingsBt.isHidden = true
            settingsBt.addTarget(self, action: #selector(goToSettings), for: .touchUpInside)
        }
    }

    @IBOutlet weak var messageLb: UILabel!

    private static let defaultText = "Place the QR code in the center.".localize()

    private lazy var capture: ZXCapture = {
        let capture = ZXCapture()
        capture.delegate = self
        capture.camera = capture.hasBack() ? capture.back() : capture.front()

        return capture
    }()

    private lazy var flashBt = UIBarButtonItem(materialIcon: .flash_on, target: self,
                                               action: #selector(toggleTorch))

    open override func viewDidLoad() {
        super.viewDidLoad()

        // Only add a "Cancel" button, if the navigation controller was just created
        // for us.
        // If we're used in a deeper navigation stack, there will be an automatic
        // back button which should be used.
        if navigationController?.viewControllers.first == self {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
            navigationItem.title = "Scan QR Code".localize()
        }
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // Trigger access request.
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied, .restricted:
            // In case of .restricted: Parental controls don't allow camera access.
            // In that case, this scene should have never been shown.
            // Therefore, we don't handle that situation specially.
            // Use QrScanViewController.canScan to check for this situation!
            showDenied()

        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] success in
                DispatchQueue.main.async {
                    if success {
                        self?.startScanning()
                    }
                    else {
                        self?.showDenied()
                    }
                }
            }

        default:
            startScanning()
        }
    }

    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        fixOrientation()
    }

    open override func viewWillTransition(to size: CGSize, with
        coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { _ in
            // Ignored.
        }) { [weak self] _ in
            self?.fixOrientation()
        }
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        capture.stop()
    }

    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        capture.layer.removeFromSuperlayer()
        capture.hard_stop()
    }

    open func showError(_ message: String) {
        self.message = message
        messageLb.textColor = .systemRed

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.message = QrScanViewController.defaultText
            self.messageLb.textColor = .keanuLabel
        }
    }


    // MARK: ZXCaptureDelegate

    public func captureResult(_ capture: ZXCapture?, result: ZXResult?) {
        if let bytes = result?.resultMetadata?[kResultMetadataTypeByteSegments.rawValue] as? NSArray,
            let byteArray = bytes.firstObject as? ZXByteArray
        {
            let data = Data(bytes: UnsafeRawPointer(byteArray.array), count: Int(byteArray.length))

            DispatchQueue.main.async {
                self.delegate?.qrCodeFound(data: data, controller: self)
            }
        }
    }


    // MARK: Private Methods

    @objc
    private func cancel() {
        dismiss(animated: true)
    }

    private func startScanning() {
        var items = [UIBarButtonItem]()

        if capture.hasBack() && capture.hasFront() {
            items.append(UIBarButtonItem(materialIcon: .switch_camera, target: self,
                                         action: #selector(switchCamera)))
        }

        if capture.hasTorch() && capture.hasBack() {
            items.append(flashBt)
        }

        navigationItem.rightBarButtonItems = items

        message = QrScanViewController.defaultText

        capture.layer.frame = container.bounds
        container.layer.addSublayer(capture.layer)

        capture.start()
    }

    private func showDenied() {
        message = "Please go to Settings and allow camera access.".localize()
        settingsBt.isHidden = false
    }

    @objc
    private func goToSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.openURL(url)
        }
    }

    @objc
    private func switchCamera() {
        capture.camera = capture.camera == capture.back() ? capture.front() : capture.back()

        if capture.camera == capture.back() {
            flashBt.isEnabled = true
        }
        else {
            flashBt.isEnabled = false
            capture.torch = false
        }
    }

    @objc
    private func toggleTorch() {
        capture.hasTorch()

        capture.torch = !capture.torch

        flashBt.materialIcon = capture.torch ? .flash_off : .flash_on
    }

    private func fixOrientation() {
        let rotation: CGFloat
        let transform: CGFloat

        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft:
            rotation = 180
            transform = 90

        case .landscapeRight:
            rotation = 0
            transform = 270

        case .portraitUpsideDown:
            rotation = 270
            transform = 180

        default:
            rotation = 90
            transform = 0
        }

        let radius: CGFloat = transform / 180 * CGFloat.pi

        capture.transform = CGAffineTransform(rotationAngle: radius)
        capture.rotation = rotation
        capture.layer.frame = container.bounds
    }
}
