//
//  KeyCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import MatrixSDK

open class DeviceCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    public static var minimalHeight: CGFloat = 140

    @IBOutlet weak var badgeLb: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var idLb: UILabel!
    @IBOutlet weak var verifiedLb: UILabel!
    @IBOutlet weak var fingerprintLb: UILabel!
    @IBOutlet weak var verifiedSw: UISwitch!


    weak var device: MXDeviceInfo?
    weak var session: MXSession?

    /**
     Applies a device info to this cell.

     - parameter device: The `MXDeviceInfo` for a device.
     - parameter delegate: The delegate to call back, when the user changed the `verifiedSw`.
     - parameter isThisDevice: true, if that is the device info of the device we're running on.
     - returns: self for convenience.
     */
    open func apply(device: MXDeviceInfo?, session: MXSession?, isThisDevice: Bool = false) -> DeviceCell {
        self.device = device
        self.session = session

        let verified = device?.trustLevel?.isVerified ?? false

        DevicesViewController.setBadge(badgeLb, verified)

        if isThisDevice {
            let text = NSMutableAttributedString()

            if let name = device?.displayName {
                text.append(NSAttributedString(string: "\(name) – "))
            }

            text.append(NSAttributedString(
                string: "This Device".localize().localizedUppercase,
                attributes: [.font: UIFont.boldSystemFont(ofSize: nameLb.font.pointSize)]))

            nameLb.attributedText = text
        }
        else {
            nameLb.text = device?.displayName
        }
        idLb.text = device?.deviceId

        if verified {
            verifiedLb.text = "verified".localize()
        }
        else {
            switch device?.trustLevel?.localVerificationStatus {
            case .unknown:
                verifiedLb.text = "new".localize()
            case .unverified:
                verifiedLb.text = "unverified".localize()
            default:
                verifiedLb.text = "blocked".localize()
            }
        }

        setFingerprint(fingerprintLb, text: device?.fingerprint,
                       alignment: .natural, bold: true)

        verifiedSw.isOn = verified
        verifiedSw.isEnabled = !isThisDevice && !(device?.trustLevel?.isCrossSigningVerified ?? false)

        return self
    }


    // MARK: Actions

    @IBAction func changeVerification() {
        guard let session = session, let device = device else {
            return
        }

        // Switch to blocked can be done right away.
        if !verifiedSw.isOn {
            VerificationManager.shared.verify(session, device, .blocked) { [weak self] _ in
                self?.resetSwitch()
            }

            return
        }

        guard let topVc = UIApplication.shared.keyWindow?.rootViewController?.top else {
            return
        }

        let id = "ID: %".localize(value: device.deviceId)
        let fingerprint = "Fingerprint:\n%".localize(value: device.fingerprint.split(by: 4).joined(separator: " "))
        let warning = "Is the other device near you and did you compare its key fingerprint?".localize()
        let advice = "If not or unsure, use interactive verification!".localize()

        AlertHelper.present(
            topVc,
            message: String(format: "%@\n\n%@\n\n%@\n\n%@", id, fingerprint, warning, advice),
            title: "Verify %".localize(value: device.displayName),
            actions: [
                AlertHelper.defaultAction("Use Interactive Verification".localize(), handler: { [weak self] _ in
                    // Need to reset prophylacticly, otherwise there will be situations where that
                    // switch is on, but the actual values is still off.
                    // Instead, if verification was successful, there's going to be a
                    // MXDeviceListDidUpdateUsersDevicesNotification or MXDeviceInfoTrustLevelDidChangeNotification
                    // which will trigger an update of the list and the switch will be correct.
                    self?.resetSwitch()

                    VerificationManager.shared.interactiveVerify(session, device) { error in
                        AlertHelper.present(topVc, message: error.localizedDescription)
                    }
                }),
                AlertHelper.destructiveAction("Fingerprints are the same!".localize(), handler: { [weak self] _ in
                    VerificationManager.shared.verify(session, device, .verified) { [weak self] _ in
                        self?.resetSwitch()
                    }
                }),
                AlertHelper.cancelAction(handler: { [weak self] _ in
                    self?.resetSwitch()
                }),
        ])
    }


    // MARK: Private Methods
    
    /**
     Set text content of label which should contain a fingerprint.

     Copious amounts of letter and line spacing are used.

     - parameter label: The `UILabel` to use.
     - parameter text: Falls back to an error message, if nil.
     - parameter alignment: Alignment used.
     - parameter bold: If bold system font should be used.
     */
    private func setFingerprint(_ label: UILabel, text: String?, alignment: NSTextAlignment, bold: Bool) {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        style.alignment = alignment

        var attributes: [NSAttributedString.Key : Any] = [.kern: 1, .paragraphStyle: style]

        if bold {
            attributes[.font] = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        }

        let string = text?.split(by: 4).joined(separator: " ")
            // Error message instead of key fingerprint which could not be read.
            ?? "CODE ERROR".localize()

        label.attributedText = NSAttributedString(string: string,
                                                  attributes: attributes)
    }

    private func resetSwitch() {
        verifiedSw.setOn(device?.trustLevel?.isVerified ?? false, animated: true)
    }
}
