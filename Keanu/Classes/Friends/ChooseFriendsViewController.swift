//
//  ChooseFriendViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 23.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

public protocol ChooseFriendsDelegate {
    
    /**
     Callback when the user successfully selected a list of friends and finishes
     her selection to go on with the process.
     
     This will only be called when the user selected *at least* one friend!
     
     - parameter friends: List of `MXUser` objects. Contains at least one.
    */
    func friendsChosen(_ friends: [Friend])
}

/**
 View controller to choose one or more friends of the list of known contacts.
 
 There's a UICollectionView at the top which is hidden, when empty, which shows
 all selected friends.
 
 The table is searchable and shows a sorted (by name) list of friends.
 
 The first section contains an "Add Friend" button.
 
 ATTENTION FOR iOS 11 and up:
 
 The search bar will be integrated into the navigation bar, as that's the
 latest hot shit there and looks really great. HOWEVER:
 The auto-reveal will only work as expected by the user, if the UITableView
 is in logical order BEFORE the UICollectionView (pillbox).
 So when changing the layout in the Interface Builder, ensure, that the logical
 order of elements stays intact.
 (You can mess with the *placement* as much as you like, though.)
 
*/
open class ChooseFriendsViewController: BaseViewController, UITableViewDelegate,
    UITableViewDataSource, UISearchResultsUpdating, UICollectionViewDelegate,
UICollectionViewDataSource {
    
    @IBOutlet weak var pillbox: UICollectionView!
    @IBOutlet weak var pillboxContainer: UIView!
    @IBOutlet weak var pillboxContainerBottom: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    /**
     Object to call back when user selected a list of friends successfully
     and wants to go on with the process.
    */
    public var delegate: ChooseFriendsDelegate?

    /**
     Configures type of display:

     - false (default): Selection of one friend will show their profile scene.
       `AddFriendViewController` will be available via a "+" button in the
       nav bar on the right.
     - true: Selection of a friend will add them to a pillbox at the bottom.
       `AddFriendViewcontroller` will be available via the first table row.
       Right nav bar button will be the "compose" icon which calls the delegate
       and dismisses itself.
       A "cancel" button will be in the nav bar on the left.
    */
    public var multiselect = false

    /**
     If multiselect is true, this setting controls if the "done" button should be enabled even if no friend is selected.
     Default is false.
     */
    public var allowEmpty = false
    /**
     Determines if newly added friends should be "selected" like anyone else, or if a chat should be started with them.
     Default value is false.
     */
    public var selectNewlyAddedFriends = false
    
    // MARK: Private properties
    
    private var filteredFriends = [Friend]()
    
    open var selectedFriends = [Friend]()
    
    private let searchController = UISearchController(searchResultsController: nil)

    /**
     Delete action for table list row. Deletes a friend.

     If the friend is in a 1:1 room, shows an alert and leaves that room first
     on confirmation.
    */
    private lazy var deleteAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .destructive,
            title: "Delete".localize())
        { (action, indexPath) in

            let friend = self.getFriend(indexPath)

            if let room = friend.privateRoom {
                let title = "Friend in Private Room".localize()
                let message =
                    "This friend is in a private room with you!\nAre you sure you want to do that?\nThe private room will be left, too!"
                    .localize()

                let actions = [
                    AlertHelper.cancelAction(),
                    AlertHelper.destructiveAction("Delete".localize()) { action in
                        room.leave() { response in
                            if response.isSuccess {
                                _ = FriendsManager.shared.remove(friend)
                            }
                            else {
                                let message = "Could not leave the private room with %!".localize(value: friend.name)
                                AlertHelper.present(self, message: message)
                            }
                        }
                    }
                ]
                
                AlertHelper.present(self, message: message, title: title, actions: actions)
            }
            else {
                _ = FriendsManager.shared.remove(friend)
            }

            self.tableView.setEditing(false, animated: true)
        }
        
        return action
    }()

    /**
     Edit action for table list row. User can edit the name of a friend.
    */
    private lazy var editAction: UITableViewRowAction = {
        let action = UITableViewRowAction(
            style: .normal,
            title: "Edit Name".localize())
        { (action, indexPath) in

            let friend = self.getFriend(indexPath)

            AlertHelper.presentEditNameAlert(self, friend.name) { newName in
                friend.name = newName
                FriendsManager.shared.sort()

                self.tableView.reloadData()
                self.pillbox.reloadData()
            }

            self.tableView.setEditing(false, animated: true)
        }

        return action
    }()

    
    // MARK: UIViewController

    override open func viewDidLoad() {
        super.viewDidLoad()

        // Register cell types.
        tableView.register(ActionButtonCell.nib, forCellReuseIdentifier: ActionButtonCell.defaultReuseId)
        tableView.register(AvatarAnd3LabelsCell.nib, forCellReuseIdentifier: AvatarAnd3LabelsCell.defaultReuseId)
        pillbox.register(PillboxFriendCell.nib, forCellWithReuseIdentifier: PillboxFriendCell.defaultReuseId)
        
        // Hide pillbox at the beginning.
        pillboxContainer.hide()

        // Set up search.
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        if #available(iOS 11.0, *) {
            // ATTENTION: Read class comments!
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }

        if multiselect {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .cancel, target: self, action: #selector(cancel))

            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .done, target: self, action: #selector(finished))
            navigationItem.rightBarButtonItem?.isEnabled = allowEmpty
        }
        else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .add, target: self, action: #selector(add))
        }
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Slightly different titles for different use.
        // Let it stay here to repair the damage done in `AddFriendViewController`!
        navigationItem.title = multiselect
            ? "Choose Friends".localize()
            : "Friends".localize()

        // Register for FriendsManager change event.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(friendsChanged(_:)),
                                               name: FriendsManager.changedNotification,
                                               object: nil)
        
        // Could have been changed, while we didn't listen to notifications.
        // (e.g. in AddFriendViewController)
        tableView.reloadData()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func friendsChanged(_ notification: Notification) {
        tableView.reloadData()

        let friends = FriendsManager.shared.friends
        let reduced = selectedFriends.filter() { friends.contains($0) }

        if reduced.count != selectedFriends.count {
            selectedFriends = reduced
            pillbox.reloadData()
            hidePillboxIfEmpty()
        }
    }
    
    // MARK: KeanuViewController
    
    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.pillboxContainerBottom.constant = kbSize.height * -1

            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        pillboxContainerBottom.constant = 0
        
        animateDuringKeyboardMovement(notification)
    }

    
    // MARK: - UITableViewDataSource

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1 + (isFiltering() ? 1 : FriendsManager.shared.sortedFriends.count)
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return !multiselect || isFiltering() ? 0 : 1
        }
        
        return isFiltering() ? filteredFriends.count
            : FriendsManager.shared.get(section - 1)?.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier:
                ActionButtonCell.defaultReuseId, for: indexPath) as? ActionButtonCell {
                
                return cell.addFriend()
            }
        }
        else {
            if let cell = tableView.dequeueReusableCell(withIdentifier:
                AvatarAnd3LabelsCell.defaultReuseId, for: indexPath) as? AvatarAnd3LabelsCell {
                
                return cell.apply(getFriend(indexPath))
            }
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : FriendsManager.shared.sortedIndex[section - 1]
    }

    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        // We don't show the section index on iOS < 11, because there we need
        // to put the search bar in the table header. The combination of having
        // a search bar in the table header *and* a section index on the right
        // looks pretty ugly: The gray search bar then has a white gap on the right.
        // The workaround for this is having a UITableView in a UIScrollView
        // and the search bar outside the table view. That seems pretty massive
        // and will complicate the code further, so I refrain from doing that
        // for now...
        guard #available(iOS 11.0, *) else {
            return nil
        }

        return isFiltering() ? nil : FriendsManager.shared.sortedIndex
    }

    public func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index + 1
    }

    
    // MARK: UITableViewDelegate

    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section > 0
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return [deleteAction, editAction]
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ActionButtonCell.height
    }
    
    /**
     Add the selected friend to the `selectedUsers` array, if not in it.
     Update `pillbox`.
     
     Or, jump to "new friend" controller.
     
    */
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            pushAddFriend()
        }
        else {
            selectFriend(friend: getFriend(indexPath))
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func selectFriend(friend: Friend) {
        if multiselect {
            if selectedFriends.isEmpty {
                pillboxContainer.show(animated: true)
                
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
            if !selectedFriends.contains(friend) {
                selectedFriends.append(friend)
                
                pillbox.reloadData()
                
                // Scroll pillbox to show latest additions.
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                    self.pillbox.scrollToItem(
                        at: IndexPath(row: self.selectedFriends.count - 1, section: 0),
                        at: .bottom, animated: true)
                }
            }
        }
        else {
            selectedFriends = [friend]
            
            pushProfile()
        }
    }
    
    // MARK: UISearchResultsUpdating

    /**
     `MXUser.displayname` and `MXUser.userId` are searched.
    */
    public func updateSearchResults(for searchController: UISearchController) {
        if let search = searchController.searchBar.text?.lowercased() {
            filteredFriends = FriendsManager.shared.friends.filter() {
                $0.matrixId.lowercased().contains(search)
                    || $0.name.lowercased().contains(search)
            }.sorted(by: <)
        }
        
        tableView.reloadData()
    }
    
    
    // MARK: UICollectionViewDataSource
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedFriends.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = pillbox.dequeueReusableCell(withReuseIdentifier:
            PillboxFriendCell.defaultReuseId, for: indexPath) as? PillboxFriendCell {
            
            return cell.apply(selectedFriends[indexPath.row])
        }
        
        assertionFailure("Unexpected condition in \(String(describing: type(of: self))).")
        
        return UICollectionViewCell()
    }

    
    // MARK: UICollectionViewDelegate
    
    /**
     User pressed a selected friend. Remove that from the selection.
     
     We don't use the remove button itself, because that's actually hard to
     tap on a phone.
     */
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedFriends.remove(at: indexPath.row)
        pillbox.reloadData()

        hidePillboxIfEmpty()
    }

    
    // MARK: Actions.
    
    /**
     Callback for "cancel" button in left navigation bar.
     
     Dismisses ourselves without further ado.
    */
    @IBAction func cancel() {
        dismiss(animated: true)
    }
        
    /**
     Callback for "compose" button in right navigation bar.
     
     Tells the delegate about the selected friends and dismisses ourselves.
    */
    @IBAction func finished() {
        dismiss(animated: true) {
            self.delegate?.friendsChosen(self.selectedFriends)
        }
    }

    @objc func add() {
        pushAddFriend()
    }

    /**
     Performs segue to the `AddFriendViewController`.
    */
    @discardableResult
    @objc open func pushAddFriend(animated: Bool = true) -> AddFriendViewController {
        let vc = UIApplication.shared.theme.createAddFriendViewController()

        if selectNewlyAddedFriends {
            vc.startChatWithAddedFriend = false
            vc.friendAddedCallback = { [weak self] matrixId in
                self?.tableView.reloadData()
                if let friend = FriendsManager.shared.get(matrixId) {
                    self?.selectFriend(friend: friend)
                }
            }
        }
        else {
            vc.startChatWithAddedFriend = true
            vc.friendAddedCallback = nil
        }

        navigationController?.pushViewController(vc, animated: animated)

        return vc
    }

    // MARK: Private Methods

    /**
     Performs segue to the `ProfileViewController`.
     */
    open func pushProfile() {
        let profileVc = UIApplication.shared.theme.createProfileViewController()
        profileVc.friend = selectedFriends.first

        navigationController?.pushViewController(profileVc, animated: true)
    }

    /**
     - returns: true, if a search filter is currently set by the user.
    */
    private func isFiltering() -> Bool {
        return searchController.isActive
            && !(searchController.searchBar.text?.isEmpty ?? true)
    }

    /**
     - parameter indexPath: The needed indexPath of the `UITableView`.
     - returns: the correct `MXUser` depending on if the user is currently searching or not.
    */
    private func getFriend(_ indexPath: IndexPath) -> Friend {
        return isFiltering() ? filteredFriends[indexPath.row]
            : FriendsManager.shared.get(indexPath.section - 1, indexPath.row)!
    }

    /**
     Hides the pillbox, if it's empty
    */
    private func hidePillboxIfEmpty() {
        if multiselect, selectedFriends.isEmpty {
            navigationItem.rightBarButtonItem?.isEnabled = allowEmpty

            pillboxContainer.hide(animated: true)
        }
    }
}
