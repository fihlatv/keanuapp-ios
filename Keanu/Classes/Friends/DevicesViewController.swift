//
//  DevicesViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 19.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

/**
 A scene showing all devices (resp. `MXDeviceInfo`s), their verification status,
 their master key fingerprint some overview and allows the user to verify or
 block certain devices.
 */
open class DevicesViewController: UITableViewController {
        
    open var friend: Friend?
    open var devices = [MXDeviceInfo]()

    override open func viewDidLoad() {
        super.viewDidLoad()

        // For an unkown reason, this needs to be set programatically. The setting
        // In the XIB doesn't do anything.
        tableView.allowsSelection = false

        if let friend = friend {
            devices = friend.devices.sorted(by: deviceSorter(_:_:))

            navigationItem.title = friend.name
        }

        // Register cell types.
        tableView.register(DeviceHeaderCell.nib, forCellReuseIdentifier: DeviceHeaderCell.defaultReuseId)
        tableView.register(DeviceCell.nib, forCellReuseIdentifier: DeviceCell.defaultReuseId)

        reloadDevices()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(
            self, selector: #selector(devicesUpdated(_:)),
            name: .MXDeviceListDidUpdateUsersDevices, object: nil)

        NotificationCenter.default.addObserver(
            self, selector: #selector(deviceUpdated),
            name: .MXDeviceInfoTrustLevelDidChange, object: nil)
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }

    @objc
    open func devicesUpdated(_ notification: Notification) {
        if let matrixId = friend?.matrixId,
            (notification.userInfo as? [String: [MXDeviceInfo]])?.keys.contains(matrixId) ?? false {

            reloadDevices()
        }
    }

    @objc
    open func deviceUpdated(_ notification: Notification) {
        if let matrixId = friend?.matrixId,
            let device = notification.object as? MXDeviceInfo,
            device.userId == matrixId {

            reloadDevices()
        }
    }


    // MARK: UITableViewDelegate

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? DeviceHeaderCell.height : UITableView.automaticDimension
    }

    open override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? DeviceHeaderCell.height : DeviceCell.minimalHeight
    }


    // MARK: UITableViewDataSource

    override open func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : devices.count
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let friend = friend,
                let cell = dequeueHeaderCell(indexPath) {

                return cell.apply(friend, countKeys())
            }
        }
        else {
            if let cell = dequeueDeviceCell(indexPath) {

                return cell.apply(device: devices[indexPath.row], session: friend?.session)
            }
        }

        return UITableViewCell()
    }


    // MARK: Helper Methods

    open func dequeueHeaderCell(_ indexPath: IndexPath) -> DeviceHeaderCell? {
        return tableView.dequeueReusableCell(withIdentifier: DeviceHeaderCell.defaultReuseId,
                                             for: indexPath) as? DeviceHeaderCell
    }

    open func dequeueDeviceCell(_ indexPath: IndexPath) -> DeviceCell? {
        return tableView.dequeueReusableCell(withIdentifier: DeviceCell.defaultReuseId,
                                             for: indexPath) as? DeviceCell
    }

    /**
     Sets the style of a badge to either be red and display a shield with an
     exclamation mark or be green and display a shield with a check mark.

     - parameter label: The badge label to change.
     - parameter ok: Set true if the device is verified.
     */
    open class func setBadge(_ label: UILabel, _ ok: Bool) {
        if ok {
            label.backgroundColor = .systemGreen
            label.text = "" // Shield with check mark
        }
        else {
            label.backgroundColor = .systemRed
            label.text = "" // Shield with exclamation mark
        }
    }

    open func reloadDevices() {
        friend?.loadDevices() { [weak self] devices in
            guard let self = self else {
                return
            }

            self.devices = devices.sorted(by: self.deviceSorter(_:_:))

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    /**
     Sorter for `MXDeviceInfo` classes.

     - Sort order:
        - "Unkown" (new) devices
        - Verified devices
        - Unverified devices
        - Blocked devices
     - Second sort order:
        - Ascending by device ID
     */
    open func deviceSorter(_ a: MXDeviceInfo, _ b: MXDeviceInfo) -> Bool {
        let aVerified = a.trustLevel?.isVerified ?? false
        let aLocalStatus = a.trustLevel?.localVerificationStatus
        let bVerified = b.trustLevel?.isVerified ?? false
        let bLocalStatus = b.trustLevel?.localVerificationStatus

        if aVerified == bVerified || aLocalStatus == bLocalStatus {
            return a.deviceId < b.deviceId
        }


        if !aVerified && aLocalStatus == .unknown {
            return true
        }

        if !bVerified && bLocalStatus == .unknown {
            return false
        }

        if aVerified {
            return true
        }

        if bVerified {
            return false
        }

        if aLocalStatus == .unverified {
            return true
        }

        // Verification states must here be:
        //   aLocalStatus == .blocked
        //   bLocalStatus == .unverified

        return false
    }

    /**
     Counts devices' verification status.

     - returns: A named tuple with number of `MXDeviceUnknown` and number of
        `MXDeviceVerified` keys.
     */
    open func countKeys() -> (unverifiedNew: Int, verified: Int) {
        var unverifiedNew = 0
        var verified = 0

        for device in devices {
            if device.trustLevel?.isVerified ?? false {
                verified += 1
            }
            else if device.trustLevel?.localVerificationStatus == .unknown {
                unverifiedNew += 1
            }
        }

        return (unverifiedNew, verified)
    }
}
