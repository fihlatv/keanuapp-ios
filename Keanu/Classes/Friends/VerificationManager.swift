//
//  VerificationManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 24.06.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import UIKit
import KeanuCore
import MatrixSDK

/**
 https://docs.google.com/document/d/1SXmyjyNqClJ5bTHtwvp8tT1Db4pjlGVxfPQNdlQILqU/edit#
 https://github.com/matrix-org/matrix-doc/pull/2072
 https://github.com/uhoreg/matrix-doc/blob/qr_key_verification/proposals/1543-qr_code_key_verification.md
 */
open class VerificationManager {

    public enum VmError: LocalizedError {

        public var errorDescription: String? {
            switch self {
            case .noBackupFound:
                return "No backup could be found to restore from!".localize()
            default:
                return "Internal Error".localize()
            }
        }

        case internalError
        case noBackupFound
    }
    
    /**
     Singleton instance.
     */
    public static let shared = VerificationManager()

    public lazy var navC: UINavigationController = {
        let navC = UINavigationController(rootViewController: verificationVc)
        navC.modalPresentationStyle = .formSheet

        return navC
    }()

    public lazy var verificationVc: VerificationViewController = {
        return UIApplication.shared.theme.createVerificationViewController()
    }()


    public init() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(verification),
            name: .MXKeyVerificationTransactionDidChange, object: nil)

        NotificationCenter.default.addObserver(
            self, selector: #selector(newRequest),
            name: .MXKeyVerificationManagerNewRequest, object: nil)
    }

    /**
     Set another device's verified state just like that.

     The user should have acknowledged that they compared fingerprints before calling this to verify a device!
     Better actually: Let the user very through interactive means! (See `#interactiveVerify`!)

     - parameter session: The account session.
     - parameter device: The `MXDeviceInfo` of the device to verify.
     - parameter state: The verification state to set.
     - parameter failure: Error callback, in case of an error. Optional.
     - parameter error: An error, if one happened.
     */
    open func verify(_ session: MXSession, _ device: MXDeviceInfo, _ state: MXDeviceVerification, _ failure: ((_ error: Error?) -> Void)? = nil) {
        session.crypto?.setDeviceVerification(
            state, forDevice: device.deviceId, ofUser: device.userId,
            success: {
                print("[\(String(describing: type(of: self)))] setDeviceVerification to \(state), success!")
            },
            failure: { [weak self] error in
                print("[\(String(describing: type(of: self)))] setDeviceVerification to \(state), error=\(String(describing: error))")

                failure?(error)
            })
    }

    /**
     Trigger interactive verification of another device.

     Keanu currently supports verification through Emoji compare (method "SAS"), through scanning a QR code,
     through displaying a QR code and through "reciprocate". (The network acknowledgement of the other device,
     that a QR code was scanned and it is legit.)

     - parameter session: The account session.
     - parameter device: The `MXDeviceInfo` of the device to verify.
     - parameter failure: Error callback, in case of an error. Optional.
     - parameter error: An error, if one happened.
     */
    open func interactiveVerify(_ session: MXSession, _ device: MXDeviceInfo, _ failure: ((_ error: Error) -> Void)? = nil) {
        let startVerification = {
            self.presentViewController().render(.waitingOnAccept)

            var methods = [MXKeyVerificationMethodQRCodeShow, MXKeyVerificationMethodReciprocate, MXKeyVerificationMethodSAS]

            if QrScanViewController.canScan {
                methods.append(MXKeyVerificationMethodQRCodeScan)
            }

            session.crypto?.keyVerificationManager?.requestVerificationByToDevice(
                withUserId: device.userId, deviceIds: [device.deviceId],
                methods: methods,
                success: { [weak self] request in
                    print("[\(String(describing: type(of: self)))] requestVerification success, request=\(request)")

                    DispatchQueue.main.async {
                        self?.presentViewController().render(request)
                    }
                },
                failure: { [weak self] error in
                    print("[\(String(describing: type(of: self)))] requestVerification, error=\(String(describing: error))")

                    self?.presentViewController().render(.error(error))

                    failure?(error)
                })
        }

        guard !(session.crypto?.crossSigning?.canCrossSign ?? true),
            let topVc = UIApplication.shared.keyWindow?.rootViewController?.top
        else {
            startVerification()
            return
        }

        topVc.present(PasswordViewController.instantiate(
            title: "Enable Cross Signing".localize(),
            message: "Cross signing on this device is not enabled, yet.\n\nIt's highly recomended to do that, in order to improve your and your peers security!\n\nPlease provide your account password!".localize()
        ) { [weak self] vc in
            guard let password = vc.password else {
                return
            }

            self?.enableCrossSigning(session, password) { error in
                DispatchQueue.main.async {
                    if let error = error {
                        failure?(error)
                    }
                    else {
                        startVerification()
                    }
                }
            }
        }, animated: true)
    }

    /**
     Enables cross-signing on this device, in case it isn't, already.

     - parameter session: The account session.
     - parameter password: The user's account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func enableCrossSigning(_ session: MXSession, _ password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        guard let csManager = session.crypto?.crossSigning,
            !csManager.canCrossSign else
        {
            completion?(nil)
            return
        }

        csManager.bootstrap(withPassword: password, success: {
            completion?(nil)
        }, failure: { [weak self] error in
            print("[\(String(describing: type(of: self)))]#enableCrossSigning error=\(String(describing: error))")

            completion?(error)
        })
    }

    /**
     Check, if a backup is already ongoing.
     */
    open func checkBackup(_ session: MXSession, completion: @escaping (_ isBackingUp: Bool, _ error: Error?) -> Void) {
        guard let backup = session.crypto?.backup else {
            completion(false, VmError.internalError)
            return
        }

        let check = {
            switch backup.state {
            case MXKeyBackupStateEnabling,
                 MXKeyBackupStateReadyToBackUp,
                 MXKeyBackupStateWillBackUp,
                 MXKeyBackupStateBackingUp:
                completion(true, nil)
            default:
                completion(false, nil)
            }
        }

        if let version = backup.keyBackupVersion
        {
            backup.trust(for: version) { trust in
                check()
            }
        }
        else {
            check()
        }
    }

    /**
     Adds a new room key backup to the server, if there's not already one enabled.

     Tries to store the recovery key to the user's keychain and displays an alert showing the recovery key to the user, if a `vc` is given..

     - parameter session: The account session.
     - parameter password: The user's account password for convenience or a different, special key backup password.
     - parameter vc: A `UIViewController` to display the alert on.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func addNewBackup(_ session: MXSession, _ password: String,  vc: UIViewController? = nil,
                           completion: ((_ error: Error?) -> Void)? = nil)
    {
        checkBackup(session) { [weak self] isBackingUp, error in
            if let error = error {
                completion?(error)
                return
            }

            // Already backing up, leave alone.
            if isBackingUp {
                completion?(nil)
                return
            }

            guard let backup = session.crypto?.backup else {
                completion?(VmError.internalError)
                return
            }

            backup.prepareKeyBackupVersion(withPassword: password, success: { info in
                backup.createKeyBackupVersion(info, success: { _ in

                    let message: String

                    if (try? Keychain.store(Keychain.Credentials(matrixId: session.myUserId, info.recoveryKey))) != nil
                    {
                        message = "This is your recovery key:\n\n%\n\nThe recovery key can be used if you forget your backup password. It was also stored to your iOS keychain."
                            .localize(value: info.recoveryKey)
                    }
                    else {
                        message = "Please note down your recovery key:\n\n%\n\nThe recovery key can be used if you forget your backup password and is revealed only once now."
                            .localize(value: info.recoveryKey)
                    }

                    if let vc = vc {
                        AlertHelper.present(vc, message: message, title: "Recovery Key".localize())
                    }

                    completion?(nil)
                }, failure: { error in
                    print("[\(String(describing: type(of: self)))]#addNewBackup error=\(String(describing: error))")

                    completion?(error)
                })
            }, failure: { error in
                print("[\(String(describing: type(of: self)))]#addNewBackup error=\(String(describing: error))")

                completion?(error)
            })
        }
    }

    /**
     Restore the latest room key backup from the server, if there's one available.

     If the provided password fails, this method will try to find a stored key backup recovery key in the keychain and try to use that.

     - parameter session: The account session.
     - parameter password: The key backup password or the recovery key. Might be the same as the account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     - parameter foundKeys: The number of found keys, if successful.
     - parameter importedKeys: The number of imported keys, if successful. (Should be the same or less than `foundKeys`.)
     */
    open func restoreBackup(_ session: MXSession, _ password: String, completion: ((_ error: Error?, _ foundKeys: UInt?, _ importedKeys: UInt?) -> Void)? = nil) {
        guard let backup = session.crypto?.backup else {
            completion?(VmError.internalError, nil, nil)
            return
        }

        backup.version(nil, success: { version in
            guard let version = version else {
                print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: VmError.noBackupFound))")

                completion?(VmError.noBackupFound, nil, nil)
                return
            }


            let callback = { [weak self] (foundKeys: UInt, importedKeys: UInt) in
                print("[\(String(describing: type(of: self)))]#restore foundKeys=\(foundKeys), importedKeys=\(importedKeys)")

                completion?(nil, foundKeys, importedKeys)
            }

            backup.restore(version, withPassword: password, room: nil, session: nil,
                               success: callback)
            { [weak self] error in
                backup.restore(
                    version, withRecoveryKey: password, room: nil, session: nil,
                    success: callback) { error in

                        if let recoveryKey = (try? Keychain.read(Keychain.Credentials(matrixId: session.myUserId)))?.password
                        {
                            print("[\(String(describing: type(of: self)))]#restoreBackup with recovery key from keychain.")

                            backup.restore(
                                version, withRecoveryKey: recoveryKey, room: nil, session: nil, success: callback) { error in
                                print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

                                completion?(error, nil, nil)

                            }
                        }
                        else {
                            print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

                            completion?(error, nil, nil)
                        }
                    }
            }

        }) { [weak self] error in
            print("[\(String(describing: type(of: self)))]#restoreBackup error=\(String(describing: error))")

            completion?(error, nil, nil)
        }
    }

    /**
     Restores the latest room key backup from the server, if there's one available and the password/recovery key is correct,
     or adds a new backup, if no backup found.

     - parameter session: The account session.
     - parameter password: The key backup password or the recovery key. Might be the same as the account password.
     - parameter completion: Callback when everything finished. In case of an error happens, contains that. Optional.
     - parameter error: An error, if one happened.
     */
    open func restoreOrAddNewBackup(_ session: MXSession, _ password: String, completion: ((_ error: Error?) -> Void)? = nil) {
        restoreBackup(session, password) { [weak self] error, foundKeys, importedKeys in
            if let error = error as? VmError, error == .noBackupFound {
                self?.addNewBackup(session, password) { error in
                    completion?(error)
                }
            }
            else {
                completion?(error)
            }
        }
    }

    /**
     Callback for `MXDeviceVerificationTransactionDidChange` notification.

     Handles all SAS and QR code interactive verification states.

     - parameter notification: The notification object.
     */
    @objc open func verification(_ notification: Notification) {
        print("[MXKeyVerification][\(String(describing: type(of: self)))]#verification notification=\(notification)")

        guard let transaction = notification.object as? MXKeyVerificationTransaction else {
            return
        }

        presentViewController().render(transaction)
    }

    /**
     Callback for `MXKeyVerificationManagerNewRequest` notification.

     Handles all verification requests from other devices of same user.

     - parameter notification: The notification object.
     */
    @objc
    open func newRequest(_ notification: Notification) {
        print("[MXKeyVerification][\(String(describing: type(of: self)))]#newRequest notification=\(notification)")

        guard let request = notification.userInfo?[MXKeyVerificationManagerNotificationRequestKey] as? MXKeyVerificationRequest,
            request.state == MXKeyVerificationRequestStatePending else {
                return
        }

        if !request.methods.contains(MXKeyVerificationMethodQRCodeShow)
            && !request.methods.contains(MXKeyVerificationMethodQRCodeShow)
            && !request.methods.contains(MXKeyVerificationMethodSAS)
        {
            request.cancel(with: .unknownMethod(), success: nil)

            return
        }

        presentViewController().render(request)
    }


    // MARK: Private Methods

    /**
     Presents the `VerificationViewController` modally, if not already presenting
     */
    private func presentViewController() -> VerificationViewController {
        if navC.presentingViewController == nil,
            let rootVc = UIApplication.shared.delegate?.window??.rootViewController {

            rootVc.present(navC, animated: true)
        }

        return verificationVc
    }
}
