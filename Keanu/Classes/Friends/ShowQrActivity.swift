//
//  ShowQrActivity.swift
//  Keanu
//
//  Created by Benjamin Erhart on 06.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

/**
 Activity for `UIActivityViewController` to display a string as QR code.
 */
class ShowQrActivity: UIActivity {
    
    private var qrCode: String?
    
    override var activityTitle: String? {
        get {
            return "QR Code".localize()
        }
    }
    
    override var activityType: UIActivity.ActivityType? {
        get {
            return ActivityType("KeanuActivityTypeQrCode")
        }
    }

    override var activityImage: UIImage? {
        get {
            return UIImage(named: "ic_qr_code", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
    }
    
    override var activityViewController: UIViewController? {
        get {
            let vc = UIApplication.shared.theme.createShowQrViewController()
            vc.qrCode = qrCode ?? ""

            let navC = UINavigationController(rootViewController: vc)
            navC.modalPresentationStyle = .formSheet

            return navC
        }
    }

    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        if activityItems.count == 1,
            let item = activityItems.first {
            
            return item is String || item is URL
        }
        
        return false
    }
    
    override func prepare(withActivityItems activityItems: [Any]) {
        if let item = activityItems.first {
            if let item = item as? String {
                qrCode = item
            }
            else if let item = item as? URL {
                qrCode = item.absoluteString
            }
        }
    }
}
