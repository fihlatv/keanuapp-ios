//
//  UrlHandler.swift
//  Keanu
//
//  Created by Benjamin Erhart on 14.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.

import KeanuCore

/**
 Class to encapsulate all handling of URLs which this app is asked to open.
 */
open class UrlHandler {

    private static var handlers: [String: ((_ url: URL) -> Bool)] = [
        
        /**
         Handles a universal link. Will try to add the provided Matrix ID as
         a friend.
         */
        Config.universalLinkHost : { url in
            
            let urlString = url.absoluteString
            // Note - Simplified parsing. Assume %@ (i.e. the user id) is always at the end of the invite url.
            if let index = Config.inviteLinkFormat.range(of: "%@") {
                let prefix = Config.inviteLinkFormat[..<index.lowerBound]
                if urlString.hasPrefix(prefix) {
                    let id = String(urlString[index.lowerBound...])

                    if MXTools.isMatrixUserIdentifier(id) {
                        return addFriend(id)
                    }
                    else if MXTools.isMatrixRoomIdentifier(id) || MXTools.isMatrixRoomAlias(id) {
                        return joinRoom(id)
                    }
                }
            }
            
            return false
        },
        
        /**
         Handles an "invite" link. Will try to add the provided Matrix ID as
         a friend.
         */
        "invite":  { url in
            if let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
                let matrixId = components.queryItems?.first(where: { $0.name == "id" })?.value {
                return addFriend(matrixId)
            }

            return false
        },

        /**
         Handles a "join" link. Will try to join the provided room ID.
        */
        "join": { url in
            if let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
                let roomId = components.queryItems?.first(where: { $0.name == "id" })?.value {
                return joinRoom(roomId)
            }

            return false
        }
    ]

    /**
     Singleton instance.
     */
    public static let shared = UrlHandler()

    /**
     Register a handler for a URL path.

     - parameter path: The URL path to handle.
     - parameter handler: The handler which can handle that path.
    */
    open func register(path: String, handler: @escaping ((_ url: URL) -> Bool)) {
        UrlHandler.handlers[path] = handler
    }

    /**
     Tries to handle a URL with which the app was opened.

     - parameter url: The URL resource to open.
     - returns: true, if a handler successfully handled the request, false if no
        suitable handler could be found or the handler encountered an error.
    */
    open func handle(_ url: URL) -> Bool {
        if let host = url.host,
            let handler = UrlHandler.handlers[host] {
            return handler(url)
        }

        return false
    }
    
    /**
     Add a user with the given matrix ID. It is done by opening the
     `AddFriendsViewController` and setting the user text.

     - parameter matrixId: The friend's Matrix ID.
     - returns: Always true.
    */
    private static func addFriend(_ matrixId: String) -> Bool {
        UIApplication.shared.openAddFriendsViewController { addFriendVc in
            addFriendVc?.searchTf.text = matrixId
            // Don't add automaticlly, have the user confirm via button press.
            //addFriendVc?.addFriend()
        }

        return true
    }

    /**
     Join a room with the given room ID or alias.
     Opens the room in the UI, if successful.

     - parameter roomId: The room ID or room alias to join.
     - returns: true on success, false on failure.
     */
    private static func joinRoom(_ roomId: String) -> Bool {
        UIApplication.shared.joinRoom(roomId, uninvited: true) { success, room in
            if success,
                let chatListViewController = UIApplication.shared.popToChatListViewController() {
                chatListViewController.openRoom(room)
            }
        }

        return true
    }
}
