//
//  AirShareManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 28.04.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import KeanuCore
import AirShare

class AirShareMessage {

    let message: BLEDataMessage
    let timeout: TimeInterval
    var receivers = [BLERemotePeer]()

    convenience init?(_ content: String) {
        guard let data = content.data(using: .utf8) else {
            return nil
        }

        self.init(data)
    }

    init(_ content: Data) {
        message = BLEDataMessage(data: content, extraHeaders: [:])
        timeout = Date().timeIntervalSince1970 + 60
    }
}

open class AirShareManager: NSObject, BLESessionManagerDelegate {

    private var sessionManager: BLESessionManager!

    private var messages = [AirShareMessage]()

    private var working = false

    /**
     Singleton instance.
     */
    private static var _shared: AirShareManager?
    public static var shared: AirShareManager? {
        if !Config.useExperimentalAirShare {
            return nil
        }

        if _shared == nil {
            _shared = AirShareManager()
        }

        return _shared
    }

    override init() {
        super.init()

        let localPeerData = UserDefaults.standard.object(forKey: "airShareLocalPeerData") as? Data

        let localPeer: BLELocalPeer

        if let localPeerData = localPeerData,
            let lp = NSKeyedUnarchiver.unarchiveObject(with: localPeerData) as? BLELocalPeer {
            localPeer = lp
        }
        else {
            let keyPair = BLEKeyPair(type: .ed25519)
            localPeer = BLELocalPeer(publicKey: keyPair?.publicKey, privateKey: keyPair?.privateKey)
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: localPeer),
                                      forKey: "airShareLocalPeerData")
        }

        localPeer.alias = MXKAccountManager.shared()?.activeAccounts?.first?.friendlyName

        sessionManager = BLESessionManager(localPeer: localPeer, delegate: self,
                                           serviceName: Bundle.main.displayName,
                                           supportsBackground: false)
    }

    open func startListening() {
        print("[\(String(describing: type(of: self)))]#startListening")

        sessionManager.advertiseLocalPeer()
    }

    open func stop() {
        print("[\(String(describing: type(of: self)))]#stop")

        sessionManager.stop()
    }

    open func send(_ message: String) {
        guard let message = AirShareMessage(message) else {
            return
        }

        messages.append(message)

        sessionManager.scanForPeers()

        workQueue()
    }

    open func sendInvite(_ roomAlias: String) {
        send(Payload(invite: Invite(roomAlias: roomAlias)))
    }

    open func send<EncodableType: Encodable>(_ payload: EncodableType) {
        guard let encoded = try? JSONEncoder().encode(payload) else {
            return
        }

        messages.append(AirShareMessage(encoded))

        sessionManager.scanForPeers()

        workQueue()
    }


    // MARK: BLESessionManagerDelegate

    public func sessionManager(_ sessionManager: BLESessionManager?, peer: BLERemotePeer?, statusUpdated status: BLEConnectionStatus, peerIsHost: Bool) {
        print("[\(String(describing: type(of: self)))] peer=\(peer?.alias ?? "no peer/no alias"), status=\(describe(status)), peerIsHost=\(peerIsHost)")
    }

    public func sessionManager(_ sessionManager: BLESessionManager?, receivedMessage
        message: BLESessionMessage?, from peer: BLERemotePeer?) {

        print("[\(String(describing: type(of: self)))] sessionManager=\(String(describing: sessionManager)), message=\(String(describing: message)), peer=\(String(describing: peer))")

        guard let data = (message as? BLEDataMessage)?.data,
            let message = String(data: data, encoding: .utf8) else {
                return
        }

        print("[\(String(describing: type(of: self)))] message=\(message)")

        if let payload = try? JSONDecoder().decode(Payload.self, from: data),
            let roomAlias = payload.invite?.roomAlias {

            print("[\(String(describing: type(of: self)))] roomAlias=\(roomAlias)")
        }

        DispatchQueue.main.async {
            if let topVc = UIApplication.shared.keyWindow?.rootViewController?.top {
                AlertHelper.present(topVc, message: message,
                                    title: "Message from %".localize(value: peer?.alias ?? "unknown".localize()))
            }
        }
    }


    // MARK: Private Methods

    @objc private func workQueue() {
        print("[\(String(describing: type(of: self)))]#workQueue scheduled in 5 seconds.")

        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 5) {
            print("[\(String(describing: type(of: self)))]#workQueue")

            if self.working {
                return
            }

            self.working = true

            let now = Date().timeIntervalSince1970

            // First, remove timed-out messages.
            self.messages.removeAll { $0.timeout < now }

            if self.messages.isEmpty {
                print("[\(String(describing: type(of: self)))]#workQueue stopped. No more messages.")

                self.stop()
                self.startListening()

                self.working = false

                return
            }

            for peer in self.sessionManager.discoveredPeers() {
                guard let peer = peer as? BLERemotePeer else {
                    continue
                }

                for message in self.messages {
                    if !message.receivers.contains(peer) {
                        print("[\(String(describing: type(of: self)))] sending message=\(String(data: message.message.data, encoding: .utf8) ?? "nil") to peer=\(peer.alias ?? "no alias")")

                        self.sessionManager.send(message.message, to: peer)

                        message.receivers.append(peer)
                    }
                }
            }

            DispatchQueue.main.async {
                self.workQueue()
            }

            self.working = false
        }
    }

    private func describe(_ status: BLEConnectionStatus) -> String {
        switch status {
        case .disconnected:
            return "disconnected"

        case .connecting:
            return "connecting"

        case .connected:
            return "connected"

        @unknown default:
            return "unknown"
        }
    }
}
