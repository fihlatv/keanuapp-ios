//
//  StoryGalleryViewController
//  Keanu
//
//  Created by N-Pex on 04.06.19.
//

import KeanuCore
import AVKit
import Photos
import MobileCoreServices

public protocol StoryGalleryViewControllerDelegate: class {
    func didPick(image:UIImage)
}

open class StoryGalleryViewController: UIViewController, UITabBarDelegate,
UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    public weak var delegate: StoryGalleryViewControllerDelegate?
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    @IBOutlet weak var tabBar: UITabBar! {
        didSet {
            let font = UIFont.materialIcons()

            for item in tabBar?.items ?? [] {
                item.setTitleTextAttributes([.font: font], for: .normal)
            }

            tabBar.selectedItem = tabBar.items?.first

            tabBar.delegate = self
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!

    private var assets = [PHAsset]()

    private lazy var imageManager = PHCachingImageManager()
    
    private var tabSelectedIndex: Int {
        if let selectedItem = tabBar.selectedItem {
            return tabBar.items?.firstIndex(of: selectedItem) ?? 0
        }

        return 0
    }

    private var useAudioLayout: Bool {
        return tabSelectedIndex == 2
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Gallery".localize()

        collectionView.register(StoryAudioAssetCell.nib, forCellWithReuseIdentifier: StoryAudioAssetCell.defaultReuseId)
        collectionView.register(StoryImageAssetCell.nib, forCellWithReuseIdentifier: StoryImageAssetCell.defaultReuseId)

        updateDataSource()
    }
    
    func updateDataSource() {
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { status in
                DispatchQueue.main.async {
                    if status != .authorized {
                        self.onNoPermission()
                    }
                    else {
                        self.onPermission()
                    }
                }
            }
        }
        else {
            self.onPermission()
        }
    }
    
    func onNoPermission() {
        //TODO
    }
    
    func onPermission() {
        var allowedTypes = [PHAssetMediaType]()

        switch tabSelectedIndex {
        case 1:
            allowedTypes.append(.video)

        case 2:
            allowedTypes.append(.audio)

        default:
            allowedTypes.append(.image)
        }

        let options = PHFetchOptions()
        options.includeAssetSourceTypes = [.typeUserLibrary, .typeCloudShared, .typeiTunesSynced]

        let assets = PHAsset.fetchAssets(with: options)

        self.assets.removeAll(keepingCapacity: true)

        if assets.count > 0 {
            assets.enumerateObjects { asset, index, stop in
                if allowedTypes.contains(asset.mediaType) {
                    self.assets.append(asset)
                }
            }
        }

        collectionView.reloadData()
    }
    

    // MARK: UITabBarDelegate
    
    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        updateDataSource()
    }


    // MARK: UICollectionViewDataSource

    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }

    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if useAudioLayout {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryAudioAssetCell.defaultReuseId, for: indexPath)

            if let cell = cell as? StoryAudioAssetCell {
                let asset = assets[indexPath.item]

                cell.titleLabel.text = asset.burstIdentifier
                cell.detailLabel.text = asset.creationDate != nil ? Formatters.format(date: asset.creationDate!) : nil
            }

            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryImageAssetCell.defaultReuseId, for: indexPath)

            if let cell = cell as? StoryImageAssetCell {
                cell.imageView.image = nil

                imageManager.requestImage(for: assets[indexPath.item],
                                          targetSize: cell.imageView.frame.size,
                                          contentMode: .aspectFit, options: nil)
                { image, options in
                    cell.imageView.image = image
                }
            }

            return cell
        }
    }


    // MARK: UICollectionViewDelegate

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset = self.assets[indexPath.item]

        if asset.mediaType == .image {
            // Show that we are working here...
            workingOverlay.isHidden = false

            imageManager.requestImageData(for: asset, options: nil) { data, string, orientation, options in
                if let image = data.flatMap({ UIImage(data: $0) }) {
                    self.delegate?.didPick(image: image)
                }
                self.workingOverlay.isHidden = true
                self.navigationController?.popViewController(animated: true)
            }
        }
        else if asset.mediaType == .audio {
            //TODO
        }
    }


    // MARK: UICollectionViewDelegateFlowLayout

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if useAudioLayout {
            return CGSize(width: collectionView.frame.width, height: 100)
        }
        else {
            return CGSize(width: 72, height: 72)
        }
    }
    
    func pickDocument() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [
            UtiHelper.typePdf,
            UtiHelper.typeMovie,
            UtiHelper.typeImage,
            UtiHelper.typeAudio
        ], in: .import)

//        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .fullScreen

        present(documentPicker, animated: true)
    }
}

extension StoryGalleryViewController: UIDocumentPickerDelegate {
    private func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
        if controller.documentPickerMode == .import {
            print("Import URL: \(url)")
        }
    }
}
