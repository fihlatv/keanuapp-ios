//
//  RoomBubbleData.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

public enum RoomBubbleDataType {
    case unknown
    case message
    case messageEncrypted
    case roomNameChange
    case roomTopicChange
    case newMember
    case paging
    case devicesAdded
    case requestRoomKeys
    
    /**
     Support for custom cell rendering by users of the Keanu lib.
     The first String is an identifier used for dequeing cells from the table view. The other value in the tuple is an optional rendering callback used when rendering the cell. If no callback is provided (i.e. it is set to nil) then it is assumed that the dequed cell implements the RoomBubbleDataRenderer protocol.
     */
    case custom(String, ((UITableViewCell,RoomBubbleData,RoomBubbleDataRendererDelegate?) -> ())?)
}

extension RoomBubbleDataType: Equatable {
    public static func == (lhs: RoomBubbleDataType, rhs: RoomBubbleDataType) -> Bool {
        switch (lhs, rhs) {
        case (.unknown, .unknown),
             (.message, .message),
             (.messageEncrypted, .messageEncrypted),
             (.roomNameChange, .roomNameChange),
             (.roomTopicChange, .roomTopicChange),
             (.newMember, .newMember),
             (.paging, .paging),
             (.devicesAdded, .devicesAdded),
             (.requestRoomKeys, .requestRoomKeys):
            return true
        case let (.custom(identifier, _), .custom(identifier_, _)):
            return identifier == identifier_
        default:
            return false
        }
    }
}

open class RoomBubbleData: NSObject {

    /**
     The data source used to generate this bubble data.
    */
    open var dataSource: RoomDataSource
    open var state: MXRoomState
    open var bubbleType: RoomBubbleDataType = RoomBubbleDataType.unknown
    
    /**
     If this/these events are considered "received". For now, this criteria is that ONE OR MORE joined members have sent us a read receipt.
     */
    open var isReceived: Bool = false
    
    /**
     The events that are grouped together in this bubble
     */
    open var events: [MXEvent] = []

    /**
     Can be used to store arbitrary information related to the bubble
     */
    open var userData: [String:Any]?
    
    /**
     An array of bubbles that are linked to this one, such that when this bubble is deleted the linked bubbles are deleted as well. It may also be that the linked bubbles are not related to any real events, or even that they are initialized from the same events as this bubble.
     */
    open var linkedBubbles: [RoomBubbleData]?
    
    /**
     Create a bubble containing the given events.
     - parameter dataSource: The RoomDataSource that owns this bubble.
     - parameter state: The room state in the timeline, just **before** the events in *events* were received.
     - parameter events: The events that this bubble handles.
     - parameter type: If given, use this type for the bubble. Else derive the type form the events.
     */
    public init?(_ dataSource: RoomDataSource, state: MXRoomState, events: [MXEvent], type: RoomBubbleDataType?) {
        self.dataSource = dataSource
        self.state = state
        self.events = events
        super.init()
        
        if let type = type {
            bubbleType = type
        }
        else if let event = events.first {
            //TODO - for now just look at first event to determine type, maybe need to look at more?
            switch event.eventType {
            case .roomMessage:
                bubbleType = .message

            case .roomEncrypted:
                bubbleType = .messageEncrypted

            case .sticker:
                bubbleType = event.wireType == kMXEventTypeStringRoomEncrypted
                    ? .messageEncrypted : .message

            case .roomName:
                bubbleType = .roomNameChange

            case .roomTopic:
                bubbleType = .roomTopicChange

            case .roomMember:
                bubbleType = .newMember

            default:
                print("Ignoring event type \(String(describing: event.type))")
                return nil
            }
        }
    }
    
    public var isIncoming: Bool {
        get {
            if let me = dataSource.room.mxSession?.myUser?.userId, let event = events.first {
                return event.sender != me
            }
            return true
        }
    }
    
    public func isIncoming(event: MXEvent) -> Bool {
        if let me = dataSource.room.mxSession?.myUser?.userId {
            return event.sender != me
        }
        return true
    }


    /**
     Helper method for displaying the best name of a user in a room.

     - parameter matrixId: The Matrix ID identifying the user.
     - returns: Either the name of a friend assigned by the user, the friend's
     self-assigned name, the local part of the Matrix ID or the complete
     Matrix ID. (The last should only happen when the RegEx fails.)
     */
    func getName(_ matrixId: String) -> String {
        if let member = state.members?.member(withUserId: matrixId) {
            return FriendsManager.shared.getOrCreate(member).name
        }

        return Friend.getFriendlyName(nil, matrixId)
    }
}
