//
//  StoryImageCell.swift
//  Keanu
//
//  Created by N-Pex 28.05.19.
//

import UIKit
import MatrixSDK

open class StoryImageCell: StoryBaseCell {
    @IBOutlet var imageView: MessageImageView!
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        // Install tap recognizer on the image view
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapMediaView(recognizer:)))
        tapRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapRecognizer)
    }
    
    public override func render(roomBubbleData: RoomBubbleData, delegate: RoomBubbleDataRendererDelegate?) {
        super.render(roomBubbleData: roomBubbleData, delegate: delegate)
        guard let attachment = getAttachment() else {return}
        imageView.setNeedsLayout()
        imageView.layoutIfNeeded()
        imageView.imageView.contentMode = .scaleAspectFill
        imageView.setAttachmentThumb(attachment)
    }
}
