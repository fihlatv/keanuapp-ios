//
//  IncomingMessageCell.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

open class IncomingMessageCell: MessageCell {
        
    public override func didTapAvatarImage(_ sender: UITapGestureRecognizer) {
        super.didTapAvatarImage(sender)
        if let delegate = self.delegate, let roomBubbleData = self.roomBubbleData {
            delegate.didTapIncomingAvatar(roomBubbleData: roomBubbleData, view: self.avatarImageView)
        }
    }
}
