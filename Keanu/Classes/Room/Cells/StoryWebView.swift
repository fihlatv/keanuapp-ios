//
//  StoryWebView.swift
//  Keanu
//
//  Created by N-Pex on 04.21.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import KeanuCore
import AVKit

class StoryWebView: WKWebView {
    private static var formatter: MXKEventFormatter = {
        let fm = MXKEventFormatter()
        fm.initDateTimeFormatters()
        return fm
    }()
    
    public var item:AttachmentItem?
    
    /**
     Name of the stylesheet file to use (without file extension). Defaults to "StoryDefaultStyles".
     */
    public var styleSheetName:String = "StoryDefaultStyles"
    
    private var webViewContent: URL?
    private var webViewCacheDir: URL?
    private var pendingDownloads = [MXMediaLoader]()
    
    public func setStory(_ event: MXEvent, mediaManager: MXMediaManager) {
        if let attachment = MXKAttachment(event: event, andMediaManager: mediaManager) {
            self.item = AttachmentItem(attachment, event: event, completed: { [weak self] (item) in
                guard let self = self else {return}
                self.item = item
                self.show()
            })
        }
    }
    
    public func setStory(_ item: AttachmentItem) {
        // TODO - Stupid copy here, just to avoid file deletion
        self.item = AttachmentItem(item.attachment, event: item.event, completed: { [weak self] (item) in
            guard let self = self else {return}
            self.item = item
            self.show()
        })
    }
    
    // WKWebView steals focus, causing "back" button to not work... See https://stackoverflow.com/questions/56332558/uibutton-selector-not-working-after-button-tapped-within-wkwebview
    override func becomeFirstResponder() -> Bool {
        return false
    }
    
    public func done() {
        // Abort pending downloads!
        for loader in pendingDownloads {
            loader.cancel()
        }
        pendingDownloads.removeAll()
    }
    
    // MARK: Private Methods
    
    private func configureWebView() {
        // Create a configuration for the preferences
        configuration.preferences.javaScriptEnabled = true
        configuration.allowsAirPlayForMediaPlayback = true
        configuration.allowsInlineMediaPlayback = true
        if #available(iOS 10.0, *) {
            configuration.mediaTypesRequiringUserActionForPlayback = []
        }
        configuration.userContentController.removeAllUserScripts()
    }
    
    private func addMetadata(title: String?, date: String?) {
        let source = """
        var d = document.createElement('div');
        d.setAttribute('style','position:fixed;visibility:invisible');
        d.setAttribute('data-story-title', '\(title ?? "")');
        document.body.appendChild(d);
        d = document.createElement('div');
        d.setAttribute('style','position:fixed;visibility:invisible');
        d.setAttribute('data-story-date', '\(date ?? "")');
        document.body.appendChild(d);
        """
        configuration.userContentController.addUserScript(WKUserScript(source: source,
                                      injectionTime: .atDocumentEnd,
                                      forMainFrameOnly: true))
    }
    
    private func addStyleSheet(styleSheetName: String) {
        // Apply style sheet.
        // We do this by adding a user script to the page.
        if let path = Bundle.main.path(forResource: styleSheetName, ofType: "css") ??
            Bundle(for: AttachmentViewController.self).path(forResource: styleSheetName, ofType: "css") {
            do {
                let cssString = try String(contentsOfFile: path).components(separatedBy: .newlines).joined()
                let source = """
                var meta = document.createElement('meta');
                meta.setAttribute('name','viewport');
                meta.setAttribute('content','width=device-width, initial-scale=1');
                document.head.appendChild(meta);
                var style = document.createElement('style');
                style.innerHTML = '\(cssString)';
                document.head.appendChild(style);                
                for (var imageIndex = 0; imageIndex < document.images.length; imageIndex++) {
                    var image = document.images[imageIndex];
                    image.classList.add('image_index_' + (imageIndex + 1));
                }
                """
                
                let userScript = WKUserScript(source: source,
                                              injectionTime: .atDocumentEnd,
                                              forMainFrameOnly: true)
                configuration.userContentController.addUserScript(userScript)
            } catch {}
        }
    }
    
    private func show() {
        guard
            let item = self.item,
            let url = item.previewItemURL,
            #available(iOS 11.0, *),
            item.attachment.isHtml
            else { return }
        
        configureWebView()
        addStyleSheet(styleSheetName: self.styleSheetName)
        addMetadata(
            title: item.attachment.fileDisplayName,
            date: StoryWebView.formatter.dateString(from: item.event, withTime: true))
        
        let blockRules = """
[{
                    "trigger": {
                        "url-filter": ".*"
                    },
                    "action": {
                        "type": "block"
                    }
},
{
                    "trigger": {
                        "url-filter": ".*",
                        "resource-type": ["document","image","media"]
                    },
                    "action": {
                        "type": "ignore-previous-rules"
                    }
}]
"""
        
        let ruleListName = "KeanuStory-BlockExternalStoryResources"
        WKContentRuleListStore.default()?.removeContentRuleList(forIdentifier: ruleListName)
        { error in
            WKContentRuleListStore.default()?.compileContentRuleList(
                forIdentifier: ruleListName, encodedContentRuleList: blockRules)
            { [weak self] ruleList, error in
                guard let self = self, error == nil, let ruleList = ruleList else {
                    return
                }
                self.configuration.userContentController.add(ruleList)
                DispatchQueue.global().async {
                    self.loadContent(url: url)
                }
            }
        }
    }
    
    private func loadContent(url: URL) {
        do {
            var content = try String(contentsOf: url)
            if
                let item = self.item,
                let storyCachePath = url.absoluteString.cachePathForStory(eventId: item.attachment.eventId) {
                // Process the data, replacing media with cached versions (or adding to
                // download queue as necessary).
                var urlsNeedingDownload:[String] = []
                if let homeserver = item.attachment.mediaManager.homeserverURL,
                    homeserver.starts(with: "https://"),
                    let urlReplacer = CashedMediaSourceReplacer(homeserver: homeserver) {
                    content = urlReplacer.stringByReplacingMatches(in: content, options: [], range: NSMakeRange(0, content.count), withTemplate: "")
                    urlsNeedingDownload = urlReplacer.urlsNeedingDownload
                }
                
                // Save to cache path
                let storyUrl = URL(fileURLWithPath: storyCachePath)
                try content.write(to: storyUrl, atomically: true, encoding: .utf8)
                
                DispatchQueue.main.async {
                    self.loadContent(storyUrl: storyUrl, urlsNeedingDownload: urlsNeedingDownload)
                }
            }
        } catch {
            // TODO  - Handle error
        }
    }
    
    private func loadContent(storyUrl:URL, urlsNeedingDownload:[String]) {
        webViewContent = storyUrl
        webViewCacheDir = storyUrl.deletingLastPathComponent()
        
        if urlsNeedingDownload.count > 0 {
            for url in urlsNeedingDownload {
                guard let cachePath = url.cachePathForStoryMedia() else { continue }
                let loader = MXMediaLoader()
                pendingDownloads.append(loader)
                loader.downloadMedia(fromURL: url, withIdentifier: url, andSaveAtFilePath: cachePath, success: { (path) in
                    self.onDownloadedOrFailed(loader: loader)
                }) { (error) in
                    self.onDownloadedOrFailed(loader: loader)
                }
            }
        } else {
            doLoadContent()
        }
    }
    
    
    /**
     When a download is done, check if ALL pending downloads are done. In that case load the story web page!
     */
    private func onDownloadedOrFailed(loader: MXMediaLoader?) {
        if let loader = loader {
            self.pendingDownloads.removeAll(where: { $0 == loader })
        }
        if pendingDownloads.count == 0 {
            doLoadContent()
        }
            
        // Hide the progress overlay
        //workingOverlay.isHidden = true
    }
    
    /**
     When needed media is downloaded, load the actual content (data blob) in the web view!
     */
    private func doLoadContent() {
        if
            let webViewContent = webViewContent,
            let webViewCacheDir = webViewCacheDir {
            DispatchQueue.main.async {
                self.loadFileURL(webViewContent, allowingReadAccessTo: webViewCacheDir)
            }
        }
    }
        
    class CashedMediaSourceReplacer: NSRegularExpression {
        
        public var urlsNeedingDownload:[String] = []
        
        init?(homeserver:String) {
            do {
                try super.init(pattern: "src=\"(\(homeserver)([^\"]*))\"", options: [.caseInsensitive])
            } catch {
                return nil
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // Override just to reset the urlsNeedingDownload array...
        override func stringByReplacingMatches(in string: String, options: NSRegularExpression.MatchingOptions = [], range: NSRange, withTemplate templ: String) -> String {
            urlsNeedingDownload.removeAll()
            return super.stringByReplacingMatches(in: string, options: options, range: range, withTemplate: templ)
        }
        
        override func replacementString(for result: NSTextCheckingResult, in string: String, offset: Int, template templ: String) -> String {
            
            // Make sure to offset the range
            let hasSrcContentRange = result.numberOfRanges == 3
            var range = result.range(at: hasSrcContentRange ? 1 : 0)
            range.location += offset
            let s = (string as NSString).substring(with: range)
            
            // Already cached?
            if hasSrcContentRange,
                let storyMediaId = s.storyMediaId(),
                let cachePath = s.cachePathForStoryMedia() {
                if !FileManager.default.fileExists(atPath: cachePath) {
                    urlsNeedingDownload.append(s)
                }
                if s.hasSuffix("?jpg") {
                    return "src=\"./media\(storyMediaId).jpg\""
                }
                return "src=\"./media\(storyMediaId).m4a\""
            }
            return s
        }
    }
}

fileprivate extension String {
    func storyMediaId() -> String? {
        if let url = URL(string: self), url.deletingPathExtension().lastPathComponent.count == 24 {
            return url.deletingPathExtension().lastPathComponent
        }
        return nil
    }
    
    func cachePathForStory(eventId: String?) -> String? {
        do {
            if let cachePath = MXMediaManager.getCachePath()
            {
                var cacheUrl = URL(fileURLWithPath: cachePath, isDirectory: true)
                cacheUrl.appendPathComponent("story", isDirectory: true)
                
                let fm = FileManager.default
                
                if !fm.fileExists(atPath: cacheUrl.path) {
                    try fm.createDirectory(at: cacheUrl, withIntermediateDirectories: true, attributes: nil)
                }
                return cacheUrl.appendingPathComponent("story\(eventId?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "").html", isDirectory: false).path
            }
        } catch {
            print("[\(String(describing: type(of: self)))] Error: \(error)")
        }
        
        return nil
    }
    
    func cachePathForStoryMedia() -> String? {
        do {
            if let cachePath = MXMediaManager.getCachePath(),
                let storyMediaId = self.storyMediaId()?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            {
                var cacheUrl = URL(fileURLWithPath: cachePath, isDirectory: true)
                cacheUrl.appendPathComponent("story", isDirectory: true)
                
                let fm = FileManager.default
                
                if !fm.fileExists(atPath: cacheUrl.path) {
                    try fm.createDirectory(at: cacheUrl, withIntermediateDirectories: true, attributes: nil)
                }
                
                if self.hasSuffix("?jpg") {
                    return cacheUrl.appendingPathComponent("media\(storyMediaId).jpg", isDirectory: false).path
                }
                return cacheUrl.appendingPathComponent("media\(storyMediaId).m4a", isDirectory: false).path
            }
        } catch {
            print("[\(String(describing: type(of: self)))] Error: \(error)")
        }
        
        return nil
    }
}
