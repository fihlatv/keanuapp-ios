//
//  AudioRecorder.swift
//  Keanu
//
//  Created by N-Pex on 11.12.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

open class AudioRecorder: UIView, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    @IBOutlet weak var view:UIView!
    
    // Recording views
    @IBOutlet weak var buttonView:UIView!
    @IBOutlet weak var recordView:UIView!
    @IBOutlet weak var recordBackgroundView:UIButton!
    @IBOutlet weak var microphoneView:UIView!
    @IBOutlet weak var trashcanView:UIView!
    @IBOutlet weak var recordDecibelView:UIView!
    @IBOutlet weak var recordDecibelViewMargin:NSLayoutConstraint!
    @IBOutlet weak var recordButtonLabel:UIButton!
    @IBOutlet weak var labelCurrentRecord:UILabel!
    
    // Error views
    @IBOutlet weak var errorButton:UIButton!
    @IBOutlet weak var errorTextButton:UIButton!
    
    private var defaultRecordButtonMargin:CGFloat = 20
    private var audioRecorder:AVAudioRecorder?
    private var recordDecibelTimer:Timer?
    private var currentRecordTimer:Timer?
    private var successCallback:((URL) -> Void)?
    
    static func showIn(view: UIView, success:@escaping ((URL) -> Void)) {
        let audioRecorder = AudioRecorder.nib.instantiate(withOwner: nil, options: nil)[0] as? AudioRecorder
        if let audioRecorder = audioRecorder {
            audioRecorder.successCallback = success
            audioRecorder.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(audioRecorder)
            audioRecorder.autoPinEdgesToSuperviewEdges()
        }
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        defaultRecordButtonMargin = recordDecibelViewMargin.constant
        hideRecordingUI()
        hideErrorUI()
        
        let recordingSession = AVAudioSession.sharedInstance()
        
        do {
            if #available(iOS 10.0, *) {
                try recordingSession.setCategory(
                    .playAndRecord, mode: .default,
                    options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                              .defaultToSpeaker, .duckOthers])
            }
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [weak self] allowed in
                guard let strongSelf = self else { return }
                DispatchQueue.main.async {
                    if !allowed {
                        // failed to record!
                        strongSelf.showErrorUI()
                    } else {
                        strongSelf.showRecordingUI()
                    }
                }
            }
        } catch {
            // failed to record!
            showErrorUI()
        }
    }
    
    private func hideErrorUI() {
        errorButton.isHidden = true
        errorTextButton.isHidden = true
    }
    
    private func showErrorUI() {
        errorButton.isHidden = false
        errorTextButton.isHidden = false
    }
    
    
    private func hideRecordingUI() {
        recordView.isHidden = true
        recordButtonLabel.isHidden = true
        labelCurrentRecord.text = " "
        //labelCurrentRecord.isHidden = true
    }
    
    private func showRecordingUI() {
        recordButtonLabel.setTitle("Hold to record".localize(), for: .normal)
        recordButtonLabel.setTitle("Hold to record".localize(), for: .highlighted)
        labelCurrentRecord.text = " "
        recordView.isHidden = true
        recordButtonLabel.isHidden = false
        setAudioLevel(0)
        setDragDistance(0)
    }
    
    public func reset() {
        if let audioRecorder = audioRecorder {
            audioRecorder.stop()
            self.audioRecorder = nil
        }
    }
    
    func getFileURL() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("recording.m4a")
    }
    
    private func distanceBetween(point1:CGPoint, point2:CGPoint) -> CGFloat {
        return sqrt(pow(point2.x-point1.x,2)+pow(point2.y-point1.y,2));
    }
    
    @IBAction func touchDownRecord(_ sender: Any) {
        startRecording()
    }


    @IBAction func touchDragInsideRecord(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDrag(touch)
        }
    }
    
    @IBAction func touchDragOutsideRecord(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            didDrag(touch)
        }
    }
    
    private func didDrag(_ touch: UITouch) {
        let pointInView = touch.location(in: self.view)
        
        let transButtonCenter = trashcanView.convert(trashcanView.center, to: self.view)
        let holdToTalkCenter = recordButtonLabel.convert(recordButtonLabel.center, to: self.view)
        let normalDistance = distanceBetween(point1: transButtonCenter, point2: holdToTalkCenter)
        let distance = distanceBetween(point1: pointInView, point2: transButtonCenter)
        
        let fracDistance = (normalDistance - distance)/normalDistance
        
        let testPoint = self.view.convert(pointInView, to: trashcanView)
        if trashcanView.point(inside: testPoint, with: nil) {
            setDragDistance(1)
            setLabelReleaseToCancel()
        } else {
            setDragDistance(Float(fracDistance))
            setLabelReleaseToStop()
        }
    }
    
    @IBAction func touchUpRecord(_ sender: Any, forEvent event: UIEvent) {
        if let touch = event.allTouches?.first {
            let testPoint = touch.location(in: trashcanView)
            if !trashcanView.point(inside: testPoint, with: nil) {
                finishRecording(success: true)
                return
            }
        }
        finishRecording(success: false)
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
        if let tapRecognizer = sender as? UITapGestureRecognizer {
            // Ignore touches in the recorder view, just react if tap is on the background.
            var point = tapRecognizer.location(in: buttonView)
            if buttonView.point(inside: point, with: nil) {
                return
            }
            point = tapRecognizer.location(in: recordView)
            if !recordView.isHidden, recordView.point(inside: point, with: nil) {
                return
            }
        }
        reset()
        removeFromSuperview()
    }
    
    @IBAction func didTapSend(_ sender: Any) {
        reset()
        removeFromSuperview()
        if let successCallback = successCallback {
            successCallback(getFileURL())
        }
    }
    
    @IBAction func didTapEnableMic(_ sender: Any) {
        if let settings = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.openURL(settings)
        }
    }
    
    private func setLabelReleaseToStop() {
        recordButtonLabel.setTitle("Release to send".localize(), for: .normal)
        recordButtonLabel.setTitle("Release to send".localize(), for: .highlighted)
    }
    
    private func setLabelReleaseToCancel() {
        recordButtonLabel.setTitle("Release to cancel recording".localize(), for: .normal)
        recordButtonLabel.setTitle("Release to cancel recording".localize(), for: .highlighted)
    }
    
    func startRecording() {
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 16000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: getFileURL(), settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.isMeteringEnabled = true
            recordDecibelTimer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(decibelTimerUpdate(_:)), userInfo: nil, repeats: true)
            currentRecordTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(currentRecordTimerUpdate(_:)), userInfo: nil, repeats: true)
            audioRecorder?.record()
            setLabelReleaseToStop()
            recordView.isHidden = false
        } catch {
            finishRecording(success: false)
        }
    }
    
    func finishRecording(success: Bool) {
        recordDecibelTimer?.invalidate()
        recordDecibelTimer = nil
        currentRecordTimer?.invalidate()
        currentRecordTimer = nil
        let recordingTime = audioRecorder?.currentTime ?? TimeInterval(0)
        audioRecorder?.stop()
        audioRecorder = nil
        if success, recordingTime > 0.5 {
            // TODO - remove this hack. Why is delay needed?!?
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.didTapSend(self)
            }
        } else {
            showRecordingUI() // Reset the UI for a new recording
        }
    }
    
    public func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    @objc func decibelTimerUpdate(_ sender: Any) {
        if let audioRecorder = audioRecorder {
            audioRecorder.updateMeters()
            let decibel = audioRecorder.averagePower(forChannel: 0)
            
            var scale:Float = 0
            
            //Values for human speech range quiet to loud
            let mindB:Float = -80
            let maxdB:Float = -10
            if decibel >= maxdB {
                //too loud
                scale = 1
            } else if decibel >= mindB,decibel <= maxdB {
                //normal voice
                let powerFactor:Float = 20
                let mindBScale:Float = pow(10, mindB / powerFactor);
                let maxdBScale:Float = pow(10, maxdB / powerFactor);
                let linearScale:Float = pow (10, decibel / powerFactor);
                let scaleMin:Float = 0;
                let scaleMax:Float = 1;
                //Get a value between 0 and 1 for mindB & maxdB values
                scale = ( ((scaleMax - scaleMin) * (linearScale - mindBScale)) / (maxdBScale - mindBScale)) + scaleMin;
            }
            setAudioLevel(scale)
            //let color = UIColor(red: 1, green: CGFloat(1-scale), blue: CGFloat(1-scale), alpha: 1)
            //view.backgroundColor = color
        }
    }

    private func setAudioLevel(_ scale:Float) {
        recordDecibelViewMargin.constant = defaultRecordButtonMargin * CGFloat(1 - scale)
        recordDecibelView.layoutIfNeeded()
        recordDecibelView.layer.cornerRadius = recordDecibelView.frame.width / 2
        recordDecibelView.alpha = 0.5 + CGFloat(0.5 * scale)
    }
    
    private func setDragDistance(_ dist:Float) {
        microphoneView.alpha = CGFloat(1.0 - dist)
        trashcanView.alpha = CGFloat(dist)
        if dist == 1.0 {
            recordBackgroundView.backgroundColor = UIColor(hexString: "#ffff9999")
        } else {
            recordBackgroundView.backgroundColor = UIColor(hexString: "#fff0f0f0")
        }
    }
    
    @objc func currentRecordTimerUpdate(_ sender: Any) {
        guard let audioRecorder = audioRecorder else {return}
        labelCurrentRecord.text = Formatters.format(duration: audioRecorder.currentTime)
    }
}
