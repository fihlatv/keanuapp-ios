//
//  AudioPlayer.swift
//  Keanu
//
//  Created by N-Pex on 06.12.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore


public protocol AudioPlayerDelegate: class {
    func onPrevious()
    func onNext()
    func onPlayerFinished()
}

/**
 A view coupled with an audio player, for presenting playback options for a media attachment.
 */
@IBDesignable
public class AudioPlayer: UIView, AVAudioPlayerDelegate {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    weak var delegate: AudioPlayerDelegate?

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var previousButton: UIButton?
    @IBOutlet weak var nextButton: UIButton?
    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet weak var labelCurrent: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var downloadButton: UIButton?
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    
    private var audioPlayer: AVAudioPlayer?
    private var attachment: MXKAttachment?
    private var loadCalled: Bool = false
    private var mediaLoader: MXMediaLoader?
    
    @IBOutlet weak var view: UIView?

    public var onShareCallback: (() -> ())? {
        didSet {
            shareButton.isHidden = onShareCallback == nil
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override open func prepareForInterfaceBuilder() {
        commonInit()
        view?.prepareForInterfaceBuilder()
    }
    
    private func commonInit() {
        self.view = AudioPlayer.nib.instantiate(withOwner: self, options: nil).first as? UIView
        guard let view = view else {return}
        addSubview(view)
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = false
        view.autoPinEdgesToSuperviewEdges()
        resetViews()
    }
    
    private func resetViews() {
        shareButton.isHidden = true
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        sliderView.isHidden = true
        sliderView.value = 0
        labelCurrent.text = ""
        labelTotal.text = ""
        playButton.isHidden = true
        pauseButton.isHidden = true
        errorButton.isHidden = true
        //NOTE: Visibility of these are controlled by the view owner.
        //previousButton?.isHidden = true
        //nextButton?.isHidden = true
        downloadButton?.isHidden = false
        activityIndicatorLabel.isHidden = true
    }
    
    /**
     Set bubble data to load. It is assumed that the first event of this bubble contains a media attachment event.
     */
    public func setRoomBubbleData(roomBubbleData: RoomBubbleData) {
        guard let event = roomBubbleData.events.first,
            let attachment = MXKAttachment(event: event, andMediaManager: roomBubbleData.dataSource.room.mxSession.mediaManager) else {
                return}
        setAttachment(attachment: attachment)
    }
    
    public func setAttachment(attachment: MXKAttachment) {
        self.attachment = attachment
        
        // If it's cached, load immediately.
        if let cacheFilePath = attachment.cacheFilePath,
            FileManager.default.fileExists(atPath: cacheFilePath) {
            
            loadAttachment()
        }
    }

    public func loadAttachment() {
        guard let attachment = attachment, !loadCalled else { return }
        loadCalled = true
        
        downloadButton?.isHidden = true
        activityIndicator.startAnimating()
        activityIndicatorLabel.text = ""
        activityIndicatorLabel.isHidden = false
        
        let _ = attachment.downloadData(success: { (data) in
            self.activityIndicator.stopAnimating()
            self.activityIndicatorLabel.isHidden = true
            self.setData(data, url: nil)
        }, failure: { (error) in
            self.activityIndicator.stopAnimating()
            self.activityIndicatorLabel.isHidden = true
            self.showLoadingError()
        }) { (percentage) in
            self.activityIndicatorLabel.text = "Downloading: :percent%".localize(dictionary: ["percent":"\(percentage)"])
        }
    }
    
    private func showLoadingError() {
        errorButton.isHidden = false
    }
    
    public func loadAudioFile(url: URL) {
        reset()
        downloadButton?.isHidden = true
        setData(nil, url: url)
    }
    
    private func setData(_ data:Data?, url: URL?) {
        do {
            if let data = data {
                audioPlayer = try AVAudioPlayer(data: data)
            } else if let url = url {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
            } else {
                showLoadingError()
            }
            if let audioPlayer = audioPlayer {
                do {
                    if #available(iOS 10.0, *) {
                        try AVAudioSession.sharedInstance().setCategory(
                            .playAndRecord, mode: .default,
                            options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                                      .defaultToSpeaker, .duckOthers])
                    }
                } catch {}
                audioPlayer.delegate = self
                labelTotal.text = Formatters.format(duration: audioPlayer.duration)
                sliderView.minimumValue = 0
                sliderView.maximumValue = Float(audioPlayer.duration)
                sliderView.value = 0
                sliderView.isHidden = false
                playButton.isHidden = false
                pauseButton.isHidden = true
            }
        }
        catch {
            showLoadingError()
        }
    }
    
    public func reset() {
        if let audioPlayer = audioPlayer {
            audioPlayer.stop()
            self.audioPlayer = nil
        }
        loadCalled = false
        mediaLoader = nil
        resetViews()
    }
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playButton.isHidden = false
        pauseButton.isHidden = true
        sliderView.setValue(0, animated: false)
        labelCurrent.text = ""
    }
    
    @IBAction func didPressDownload(sender:Any) {
        loadAttachment()
    }
    
    @IBAction func didPressPlay(sender:Any) {
        guard let audioPlayer = audioPlayer else {return}
        if !audioPlayer.isPlaying {
            audioPlayer.play()
            updateUI()
        }
    }
    
    @IBAction func didPressPause(sender:Any) {
        guard let audioPlayer = audioPlayer else {return}
        if audioPlayer.isPlaying {
            audioPlayer.pause()
        }
    }
    
    private func updateUI() {
        guard let audioPlayer = audioPlayer else {return}
        if audioPlayer.isPlaying {
            pauseButton.isHidden = false
            playButton.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.updateUI()
            }
        } else {
            pauseButton.isHidden = true
            playButton.isHidden = false
        }
        labelCurrent.text = Formatters.format(duration: audioPlayer.currentTime)
        sliderView.value = Float(audioPlayer.currentTime)
    }


    // MARK: Actions
    
    @IBAction func didChangeSlider(_ sender: Any) {
        guard let audioPlayer = audioPlayer else {return}
        audioPlayer.currentTime = TimeInterval(sliderView.value)
    }
    
    @IBAction func didPressPrevious(sender: Any) {
        delegate?.onPrevious()
    }
    
    @IBAction func didPressNext(sender: Any) {
        delegate?.onNext()
    }

    @IBAction func didPressShare() {
        onShareCallback?()
    }
}
