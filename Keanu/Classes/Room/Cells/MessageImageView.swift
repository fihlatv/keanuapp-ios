//
//  AnimatedGIFImageView.swift
//  Keanu
//
//  Created by N-Pex on 17.01.19.
//  Copyright © 2019 GuardianProject. All rights reserved.
//

import KeanuCore
import FLAnimatedImage

/**
 An override of MXKImageView that can handle animated GIFs. This is done by reacting to when the (still) image is set. If the mime type of the attachment is then "image/gif" try to load it as an animated GIF.
 */
class MessageImageView: MXKImageView {
    
    var attachment: MXKAttachment?
    
    override func setAttachment(_ attachment: MXKAttachment!) {
        self.attachment = attachment
        super.setAttachment(attachment)
    }
    
    override func setAttachmentThumb(_ attachment: MXKAttachment!) {
        self.attachment = attachment
        super.setAttachmentThumb(attachment)
    }
    
    override var image: UIImage! {
        get {
            return super.image
        }
        set {
            super.image = newValue
            checkIfAnimatedGIF()
            checkIfVideo()
        }
    }

    private func checkIfVideo() {
        // If this is a video, add a "play button" icon on top of the image, to indicate it's playable.
        guard let attachment = attachment, attachment.type == MXKAttachmentTypeVideo else { return }
        let playButton = UILabel(forAutoLayout: ())
        playButton.translatesAutoresizingMaskIntoConstraints = false
        playButton.font = UIFont(name: "Material Icons", size: 80)
        playButton.minimumScaleFactor = 0.2
        playButton.text = ""
        playButton.textAlignment = .center
        playButton.numberOfLines = 1
        playButton.adjustsFontSizeToFitWidth = true
        playButton.textColor = .white
        playButton.shadowOffset = CGSize(width: 1, height: 1)
        playButton.shadowColor = .black
        self.addSubview(playButton)
        playButton.autoCenterInSuperview()
        playButton.autoMatch(.height, to: .height, of: self, withMultiplier: 0.3)
        playButton.autoMatch(.width, to: .width, of: self, withMultiplier: 0.3)
    }
    
    private func checkIfAnimatedGIF() {
        guard let attachment = attachment,
            let mimeType = attachment.contentInfo?["mimetype"] as? String,
            mimeType == UtiHelper.mimeGif else { return }
        
        attachment.getData({ (data) in
            if let animatedImage = FLAnimatedImage(gifData: data) {
                let animatedImageView = FLAnimatedImageView(image: super.image)
                animatedImageView.animatedImage = animatedImage
                self.addSubview(animatedImageView)
                animatedImageView.autoPinEdgesToSuperviewEdges()
            }
        }) { (error) in
        }
    }
}
