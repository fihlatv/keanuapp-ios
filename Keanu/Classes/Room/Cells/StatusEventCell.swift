//
//  StatusEventCell.swift
//  Keanu
//
//  Created by N-Pex on 26.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

/**
 A cell used to show status events, like room name and topic updates.
 */
open class StatusEventCell: UITableViewCell, RoomBubbleDataRenderer {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @objc @IBOutlet open weak var titleLabel:UILabel!
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let label = view as? UILabel {
                if label.numberOfLines == 0 {
                    label.preferredMaxLayoutWidth = label.bounds.width
                }
            }
        }
    }
    
    public func render(roomBubbleData: RoomBubbleData, delegate:RoomBubbleDataRendererDelegate?) {
        switch roomBubbleData.bubbleType {
        case .roomNameChange:
            guard let event = roomBubbleData.events.first(where: { $0.eventType == .roomName }) else {
                return
            }

            let name = event.content["name"] as? String
                ?? roomBubbleData.state.name
                ?? ""

            titleLabel.text = "% changed the room name to \"%\"".localize(values:
                roomBubbleData.getName(event.sender), name)
            break

        case .roomTopicChange:
            guard let event = roomBubbleData.events.first(where: { $0.eventType == .roomTopic }) else {
                return
            }

            let topic = event.content["topic"] as? String
                ?? roomBubbleData.state.topic
                ?? ""

            titleLabel.text = "% changed the room topic to \"%\"".localize(values:
                roomBubbleData.getName(event.sender), topic)
            break

        default:
            break
        }
    }
}


