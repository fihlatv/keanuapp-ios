//
//  StoryWebViewCell.swift
//  Keanu
//
//  Created by N-Pex on 4.29.20.
//

import UIKit
import MatrixSDK

open class StoryWebViewCell: UITableViewCell {
    private lazy var storyWebView: StoryWebView = {
        let storyWebView = StoryWebView(forAutoLayout: ())
        storyWebView.isUserInteractionEnabled = false // Taps should fire "rowSelected" not do stuff on content.
        
        // Use list style sheet
        storyWebView.styleSheetName = "StoryListItemStyles"
        
        contentView.addSubview(storyWebView)
        let constraint = storyWebView.autoMatch(.height, to: .width, of: storyWebView, withMultiplier: 1)
        constraint.autoRemove()
        constraint.priority = .defaultHigh
        constraint.autoInstall()
        storyWebView.autoPinEdgesToSuperviewEdges()
        return storyWebView
    }()
    
    public func setStory(_ event: MXEvent, mediaManager: MXMediaManager) {
        storyWebView.setStory(event, mediaManager: mediaManager)
    }
}
