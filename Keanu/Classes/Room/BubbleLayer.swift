//
//  BubbleLayer.swift
//  Keanu
//
//  Created by N-Pex on 04.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import CoreFoundation

public enum BubbleViewType {
    case none
    case incoming
    case outgoing
    case incomingMask
    case outgoingMask
}

extension UIView {
    
    public var bubbleLayer: BubbleLayer? {
        get {
            if let sublayers = layer.sublayers {
                for layer in sublayers {
                    if let layer = layer as? BubbleLayer {
                        return layer
                    }
                }
            }

            return nil
        }
    }
    
    /**
     Set a chat bubble layer as background.

     - parameter type: The bubble type to use.
     */
    public func setBubbleView(_ type: BubbleViewType) {
        // Remove old bubble layer, if any.
        if let oldBubble = bubbleLayer {
            oldBubble.removeFromSuperlayer()
        }

        if type != .none {
            let bubbleLayer = BubbleLayer(view: self, type: type)
            layer.insertSublayer(bubbleLayer, at: 0)
        }
    }
    
    func setBubbleMask(_ type: BubbleViewType) {
        if type == .none {
            layer.mask = nil

            return
        }

        layer.mask = BubbleLayer(view: self, type: type)
    }
}

/**
 Implements a bubble view as a sublayer of a given target view.
 
 Layer path from here:
 https://medium.com/@dima_nikolaev/creating-a-chat-bubble-which-looks-like-a-chat-bubble-in-imessage-the-advanced-way-2d7497d600ba
 */
public class BubbleLayer: CAShapeLayer {

    weak var view: UIView?

    public var type: BubbleViewType
    
    public override init(layer: Any) {
        if let layer = layer as? BubbleLayer {
            view = layer.view
            type = layer.type
        }
        else {
            type = .none
        }

        super.init(layer: layer)

        typeUpdated()

        needsDisplayOnBoundsChange = true

        if let view = view {
            frame = view.frame
        }
    }
    
    init(view: UIView, type: BubbleViewType) {
        self.view = view
        self.type = type

        super.init()

        typeUpdated()

        needsDisplayOnBoundsChange = true
        frame = view.frame
    }

    private func typeUpdated() {
        // When in light mode, the background of the RoomViewController is a
        // very light gray, so a white bubble can be recognized.
        //
        // When in dark mode, the background is pitch black, so we need to
        // use a very dark gray bubble, to be recognizable as such.

        let bgColor: CGColor

        if #available(iOS 13.0, *) {
            if view?.traitCollection.userInterfaceStyle == .dark {
                bgColor = UIColor.secondarySystemBackground.cgColor
            }
            else {
                bgColor = UIColor.white.cgColor
            }
        }
        else {
            bgColor = UIColor.white.cgColor
        }

        switch type {
        case .incoming, .outgoing:
            fillColor = bgColor
            strokeColor = bgColor
            lineWidth = 0

        case .incomingMask, .outgoingMask:
            fillColor = bgColor
            strokeColor = UIColor.black.cgColor
            lineWidth = 2

        default:
            break
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSublayers() {
        super.layoutSublayers()

        if let superlayer = superlayer {
            frame = superlayer.bounds
        }
        path = UIApplication.shared.theme.createChatBubble(type: type, rect: frame)
    }
    
    public class func createPath(type: BubbleViewType, frame: CGRect, lineWidth: CGFloat) -> CGPath? {
        // Need to make sure the whole path is inside frame. Since lineWidth/2
        // will stick out to the left, right, top and bottom, we just subtract
        // lineWidth from both width and height.
        let width = frame.size.width - lineWidth
        let height = frame.size.height - lineWidth
        
        switch type {
        case .incoming, .incomingMask:
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 22, y: height))
            bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: width, y: 17))
            bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
            bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
            bezierPath.close()
            // Offset by half line width, so everything fits
            let transform = CGAffineTransform(translationX: lineWidth / 2, y: lineWidth / 2)
            bezierPath.apply(transform)
            return bezierPath.cgPath

        case .outgoing, .outgoingMask:
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: width - 22, y: height))
            bezierPath.addLine(to: CGPoint(x: 17, y: height))
            bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
            bezierPath.addLine(to: CGPoint(x: 0, y: 17))
            bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
            bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
            bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
            bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
            bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
            bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
            bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
            bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
            bezierPath.close()
            // Offset by half line width, so everything fits
            let transform = CGAffineTransform(translationX: lineWidth / 2, y: lineWidth / 2)
            bezierPath.apply(transform)
            return bezierPath.cgPath

        default:
            return nil
        }
    }
}
