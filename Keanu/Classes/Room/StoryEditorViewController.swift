//
//  StoryEditorViewController.swift
//  Keanu
//
//  Created by N-Pex on 06-09-19.
//

import KeanuCore
import MatrixSDK
import RichEditorView

public protocol StoryEditorViewControllerDelegate: class {
    func onSaveStory(fileName:String, content:String)
}

open class StoryEditorViewController: BaseViewController, StoryAddMediaViewControllerDelegate, RichEditorToolbarDelegate {

    public weak var delegate: StoryEditorViewControllerDelegate?
    public weak var session: MXSession?

    @IBOutlet weak var titleView: UITextField!
    @IBOutlet weak var editor: RichEditorView!
    @IBOutlet weak var toolbarView: RichEditorToolbar!

    /**
     Safari can't play audio and video directly from uploaded url on the matrix server. We want the preview
     to work in the editor, so in the editor we use local cache paths. When saving the story HTML, however,
     we want to use the uploaded https url:s, so we apply a regex using the mapping below!
     */
    private var urlMap:[String:String] = [:]
    
    private var uploadOperation: MXMediaLoader?
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.title = "Your Story".localize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .save, target: self, action: #selector(save))

        let image = UIImage(named: "ic_add", in: Bundle.mxk_bundle(for: StoryEditorViewController.self) , compatibleWith: .none)

        let addImageOption = RichEditorOptionItem(image: image, title: "+") { [weak self] toolbar in
            let vc = UIApplication.shared.theme.createStoryAddMediaViewController()
            vc.delegate = self

            self?.navigationController?.pushViewController(vc, animated: true)
        }

        var options:[RichEditorOption] = [
            RichEditorOptions.link,
            RichEditorOptions.bold,
            RichEditorOptions.italic,
            //RichEditorOptions.clear,
            RichEditorOptions.undo,
            //RichEditorOptions.redo,
            //RichEditorOptions.subscript,
            //RichEditorOptions.superscript,
            //RichEditorOptions.strike,
            //RichEditorOptions.underline,
            //RichEditorOptions.textColor,
            //RichEditorOptions.textBackgroundColor,
            RichEditorOptions.header(1),
            RichEditorOptions.header(2),
            //RichEditorOptions.header(3),
            //RichEditorOptions.header(4),
            //RichEditorOptions.header(5),
            //RichEditorOptions.header(6),
            //RichEditorOptions.indent,
            //RichEditorOptions.outdent,
            RichEditorOptions.orderedList,
            RichEditorOptions.unorderedList,
            RichEditorOptions.alignLeft,
            RichEditorOptions.alignCenter,
            RichEditorOptions.alignRight,
            //RichEditorOptions.Image,
        ]
        options.insert(addImageOption, at: 0)
        toolbarView.options = options
        toolbarView.editor = editor
        toolbarView.delegate = self

        // Issue #216 - Can't play video or audio when added. We need to give file
        // access to the cache path when loading the editor html.
        // We can either hack RichEditorView or just reload the html here,
        // with correct permission!
        if let url = Bundle(for: RichEditorView.self).url(forResource: "rich_editor", withExtension: "html")
        {
            let cacheUrl = URL(fileURLWithPath: MXMediaManager.getCachePath(), isDirectory: true)
            editor?.webView.loadFileURL(url, allowingReadAccessTo: cacheUrl)
        }
    }

    public func didSelectMedia(_ media: [MediaInfo]) -> Bool {
        self.editor?.focus()
        for m in media {
            addMedia(media: m)
        }
        return true
    }

    private func addMedia(media: MediaInfo) {
        guard let session = session,
            let uploader = MXMediaManager.prepareUploader(
                withMatrixSession: session, initialRange: 0, andRange: 1) else { return }
        
        var data:Data? = nil
        if media.isImage {
            if let image = MXKTools.forceImageOrientationUp(media.image)?.reduce(to: CGSize(width: 480, height: 480)) {
                data = image.pngData()
            }
        } else if media.isAudio, let url = media.url {
            do {
                data = try Data(contentsOf: url)
            } catch {}
        } else if media.isVideo, let url = media.url {
            do {
                data = try Data(contentsOf: url)
            } catch {}
        }
        
        guard data != nil else { return }
       
        workingOverlay.message = "Uploading... tap to cancel".localize()
        workingOverlay.tapHandler = { [weak self] in
            self?.uploadOperation?.cancel()
            self?.uploadOperation = nil
        }
        workingOverlay.isHidden = false
        uploadOperation = uploader
        
        uploader.uploadData(
            data,
            filename: nil, mimeType: media.mime,
            success: { [weak self] matrixUrl in
                self?.workingOverlay.isHidden = true

                guard let matrixUrl = matrixUrl,
                    let url = session.mediaManager.url(ofContent: matrixUrl) else {
                        return
                }

                let writeToCache = { (serverUrl: String, type: String) -> String in
                    if let cachePath = MXMediaManager.cachePath(forMatrixContentURI: matrixUrl, andType: type, inFolder: nil) {
                        let cacheUrl = URL(fileURLWithPath: cachePath)

                        try? data?.write(to: cacheUrl)

                        if FileManager.default.isReadableFile(atPath: cacheUrl.path) {
                            self?.urlMap[cacheUrl.absoluteString] = serverUrl
                            return cacheUrl.absoluteString
                        }
                    }

                    return serverUrl
                }

                var html: String?

                if media.isImage {
                    html = "<img src=\"\(url)?jpg\" alt=\"\" style=\"max-width: 100%; width: auto; height:auto\"/>"
                }
                else if media.isAudio {
                    let src = writeToCache(url, media.mime)

                    html = "<audio controls=true src=\"\(src)\"></audio>"
                }
                else if media.isVideo {
                    let src = writeToCache(url, media.mime)

                    html = "<video width=\"320\" height=\"240\" controls=true src=\"\(src)\" /></video>"
                }

                if let html = html {
                    self?.editor?.runJS("RE.prepareInsert();")
                    // Fixed issue #215: If the only thing that's contained inside a
                    // contenteditable=true element is some multimedia element,
                    // then text selection/cursor positioning doesn't work anymore
                    // and nothing more can't be inserted.
                    // Solution: Add a BR at the end, similar to what RichEditorView's JS does.
                    self?.editor?.runJS("RE.insertHTML('\(html)<div><br /></div>');")
                }
        }, failure: { [weak self] error in
            self?.workingOverlay.isHidden = true
            print("Error " + (error?.localizedDescription ?? "unknown"))
        })
    }

    /**
     Open a dialog to insert a link.
     */
    public func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        let alert = AlertHelper.build(title: "https://...".localize(), actions: [AlertHelper.cancelAction()])

        AlertHelper.addTextField(alert)

        alert.addAction(AlertHelper.defaultAction(handler: { [weak self] _ in
            if let link = alert.textFields?.first?.text {
                self?.editor.insertLink(link, title: link)
            }
        }))

        present(alert, animated: true)
    }

    @objc public func cancel() {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    @objc public func save() {
        guard editor.contentHTML.count > 0 else { return }
        
        var title:String? = nil
        if let editText = titleView.text, editText.count > 0 {
            title = editText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        }
        let titleString = title ?? "story\(Date().timeIntervalSince1970)"
        
        var content = editor.contentHTML
        
        // Make sure to use real url:s on server
        if let replacer = MappedSourceReplacer(urlMap: urlMap) {
            content = replacer.stringByReplacingMatches(in: content, options: [], range: NSMakeRange(0, content.count), withTemplate: "")
        }
        delegate?.onSaveStory(fileName: titleString.appending(".html"), content: content)

        navigationController?.dismiss(animated: true)
    }

    class MappedSourceReplacer: NSRegularExpression {
        private let urlMap:[String:String]
        
        init?(urlMap:[String:String]) {
            self.urlMap = urlMap
            do {
                try super.init(pattern: "src=\"([^\"]*)\"", options: [.caseInsensitive])
            } catch {
                return nil
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func replacementString(for result: NSTextCheckingResult, in string: String, offset: Int, template templ: String) -> String {
            
            if result.numberOfRanges == 2 {
                var range = result.range(at: 1)
                range.location += offset
                let s = (string as NSString).substring(with: range)
                if let url = urlMap[s] {
                    return "src=\"\(url)\""
                }
            }
            var range = result.range(at: 0)
            range.location += offset
            return (string as NSString).substring(with: range)
        }
    }
}

