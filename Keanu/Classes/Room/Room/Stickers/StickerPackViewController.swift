//
//  StickerPackViewController
//  Keanu
//
//  Created by N-Pex on 13.02.20.
//
//

import UIKit
import Photos

open class StickerPackViewController: UIPageViewController,
UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    private var stickerPacks = [String]()
    private(set) lazy var orderedViewControllers = [UIViewController]()

    open weak var pickDelegate: PickStickerViewControllerDelegate?

    public convenience init() {
        self.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override init(transitionStyle style: UIPageViewController.TransitionStyle,
                         navigationOrientation: UIPageViewController.NavigationOrientation,
                         options: [UIPageViewController.OptionsKey : Any]? = nil) {

        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))

        view.backgroundColor = .keanuBackground

        // Sticker packs are sorted by a three character prefix of the file folders, like 00 general
        if let docsPath = StickerManager.stickersFolderPath {
            do {
                stickerPacks = (try? FileManager.default.contentsOfDirectory(atPath: docsPath))?
                    .sorted(by: { $0.compare($1) != .orderedDescending }) ?? []
            }
        }

        // Create view controllers
        for stickerPack in stickerPacks {
            let vc = UIApplication.shared.theme.createPickStickerViewController()
            vc.delegate = pickDelegate
            vc.stickerPackFileName = stickerPack
            // Remove prefix
            vc.stickerPack = String(stickerPack[stickerPack.index(stickerPack.startIndex, offsetBy: 3)...])
            orderedViewControllers.append(vc)
        }

        dataSource = self
        delegate = self

        if let firstViewController = orderedViewControllers.first as? PickStickerViewController {
            navigationItem.title = firstViewController.stickerPack
            setViewControllers([firstViewController], direction: .forward, animated: true)
        }
    }

    open func pageViewController(_ pageViewController: UIPageViewController,
                                 viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {
            return nil
        }

        guard orderedViewControllers.count > previousIndex else {
            return nil
        }

        return orderedViewControllers[previousIndex]
    }

    open func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }

        guard orderedViewControllersCount > nextIndex else {
            return nil
        }

        return orderedViewControllers[nextIndex]
    }

    open func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }

    open func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = orderedViewControllers.firstIndex(of: firstViewController) else {
                return 0
        }

        return firstViewControllerIndex
    }

    open func pageViewController(_ pageViewController: UIPageViewController,
                                 didFinishAnimating finished: Bool,
                                 previousViewControllers: [UIViewController],
                                 transitionCompleted completed: Bool) {

        if let vc = pageViewController.viewControllers?.first as? PickStickerViewController {
            navigationItem.title = vc.stickerPack
        }
    }

    @objc func cancel() {
        navigationController?.dismiss(animated: true)
    }
}
