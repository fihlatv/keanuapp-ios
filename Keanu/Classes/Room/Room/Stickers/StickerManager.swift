//
//  StickerManager.swift
//  Keanu
//
//  Created by N-Pex on 13.02.20.
//

import MatrixSDK

fileprivate extension String {
    var sanitizedFileName: String {
        return components(separatedBy: .init(charactersIn: "/\\:?%*|\"<>")).joined()
    }
}

public class StickerManager: NSObject {
    
    /**
     The path to a folder containing sticker pack folders. Set this to a valid path at startup in order
     to support keanu-style stickers in your app.
     */
    public static var stickersFolderPath: String? {
        didSet(value) {
            if hasStickers {
                // If we have stickers, install a formatter for "last message" so we can remove sticker short codes.
                MXRoomSummary.lastMessageFormatter = { summary in
                    if let lastEvent = summary.lastMessageEvent, StickerManager.isStickerEvent(event: lastEvent) {
                            return "Sticker".localize()
                        }
                    return nil
                }
            }
        }
    }

    public static var hasStickers: Bool {
        guard let path = StickerManager.stickersFolderPath else { return false }
        return FileManager.default.fileExists(atPath: path)
    }
    
    public static func isStickerEvent(event: MXEvent) -> Bool {
        if event.eventType == .roomMessage,
            let msgType = event.content["msgtype"] as? String,
            msgType == kMXMessageTypeText,
            let body = event.content["body"] as? String,
            isValidStickerShortCode(body) {
            return true
        }
        return false
    }
    
    public static func isValidStickerShortCode(_ message:String) -> Bool {
        if message.hasPrefix(":"), message.hasSuffix(":") {
            if let fileName = stickerFilename(fromMessage: message) {
                if (FileManager.default.fileExists(atPath: fileName)) {
                    return true
                }
            }
        }
        return false
    }
    
    public static func stickerFilename(fromMessage message:String) -> String? {
        let stickerDescription = message.trimmingCharacters(in: CharacterSet(charactersIn: ":"))
        if let firstDash = stickerDescription.firstIndex(of: "-") {
            let packPart = String(stickerDescription[..<firstDash])
            let stickerPart = String(stickerDescription[stickerDescription.index(firstDash, offsetBy: 1)...])
            let regex = try! NSRegularExpression(pattern: "[^a-zA-Z0-9_& -]+", options: [])
            let messagePack = regex.stringByReplacingMatches(in: packPart, options: [], range: NSMakeRange(0, packPart.count), withTemplate: "")
            let messageSticker = stickerPart.sanitizedFileName
            return filenameFor(sticker: messageSticker, inPack: messagePack)
        }
        return nil
    }
    
    public static func filenameFor(sticker:String, inPack pack:String) -> String? {
        guard let stickersFolderPath = stickersFolderPath else { return nil }
        
        var foundPack:String?
        var foundSticker:String?
        
        // iOS is case sensitive, so need to match file case
        //
        let fileManager = FileManager.default
        do {
            let stickerPacks = try fileManager.contentsOfDirectory(atPath: stickersFolderPath)
            for stickerPack in stickerPacks {
                let stickerPackName = String(stickerPack[stickerPack.index(stickerPack.startIndex, offsetBy: 3)...])
                if (stickerPackName.caseInsensitiveCompare(pack) == ComparisonResult.orderedSame) {
                    foundPack = stickerPack
                    
                    let stickers = try fileManager.contentsOfDirectory(atPath: stickersFolderPath + "/" + foundPack!)
                    for s in stickers {
                        var sName = s
                        if s.hasPrefix("_") {
                            sName = String(s[s.index(s.startIndex, offsetBy: 1)...])
                        }
                        if (sName.caseInsensitiveCompare(sticker + ".png") == ComparisonResult.orderedSame) {
                            foundSticker = s
                            break
                        }
                    }
                    break
                }
            }
        } catch {
            print(error)
        }
        
        if let foundPack = foundPack, let foundSticker = foundSticker {
            return stickersFolderPath + "/" + foundPack + "/" + foundSticker
        }
        return nil
    }
}
