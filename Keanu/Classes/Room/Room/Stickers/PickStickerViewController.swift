//
//  PickStickerViewController.swift
//  Keanu
//
//  Created by N-Pex on 13.02.20.
//
//

import UIKit

public protocol PickStickerViewControllerDelegate: class {
    func didPickSticker(_ sticker:String, inPack: String)
}

open class PickStickerViewController: UICollectionViewController {

    open weak var delegate: PickStickerViewControllerDelegate?
    
    open var stickerPackFileName = ""
    open var stickerPack = ""

    private var stickers = [String]()
    private var stickerPaths = [String]()

    fileprivate var assetGridThumbnailSize = CGSize()
    
    open override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(PickStickerCell.nib, forCellWithReuseIdentifier: PickStickerCell.defaultReuseId)
        
        if let stickersPath = StickerManager.stickersFolderPath {
            let docsPath = stickersPath + "/" + stickerPackFileName

            do {
                let stickerFiles = try? FileManager.default.contentsOfDirectory(atPath: docsPath)

                for item in stickerFiles ?? [] {
                    // Hide "old" stickers with an starting "_"
                    if !item.hasPrefix("_") {
                        stickers.append((item as NSString).deletingPathExtension)
                        stickerPaths.append(docsPath + "/" + item)
                    }
                }
            }
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        let cellSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        assetGridThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
    }
    
    // Mark - UICollectionViewDataSource
    
    open override func collectionView(_ collectionView: UICollectionView,
                                      numberOfItemsInSection section: Int) -> Int {
        return stickerPaths.count
    }
    
    open override func collectionView(_ collectionView: UICollectionView,
                                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PickStickerCell.defaultReuseId, for: indexPath)

        if let cell = cell as? PickStickerCell {
            cell.imageView.image = UIImage(contentsOfFile: stickerPaths[indexPath.item])
        }

        return cell
    }

    open override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.dismiss(animated: true)

        delegate?.didPickSticker(stickers[indexPath.item], inPack: stickerPack)
    }
}
