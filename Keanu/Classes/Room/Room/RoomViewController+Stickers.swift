//
//  RoomViewController+Stickers.swift
//  Keanu
//
//  Created by N-Pex on 13.02.20.
//

import MatrixSDK

extension RoomViewController: PickStickerViewControllerDelegate {

    public func didPickSticker(_ sticker: String, inPack pack: String) {
        closeAttachmentPicker()
        
        if let room = room {
            sendText(room: room, text: ":" + pack + "-" + sticker + ":")
        }
    }
}
