//
//  RoomDataSourceDelegate.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK

public protocol RoomDataSourceDelegate: class {
    func didChange(deleted:[IndexPath]?, added:[IndexPath]?, changed:[IndexPath]?, live:Bool, disableScroll:Bool)
    func didScroll(offset:CGFloat)
    func onTypingUsersChange(_ room:MXRoom)
    
    func pagingStarted(direction:MXTimelineDirection)
    func pagingDone(direction:MXTimelineDirection, newIndexPaths:[IndexPath], changedIndexPaths:[IndexPath], success: Bool, scrollToIndexPath: IndexPath?)
    
    /**
     Create a table view cell for this event.
     
     - Parameter at: The IndexPath of the object in the table view.
     - Parameter for: The RoomBubbleData object to display.
     */
    func createCell(at indexPath:IndexPath, for roomBubbleData:RoomBubbleData) -> UITableViewCell?
    
    /**
     Create a table view cell of the given type. Used e.g. to create the "Loading more..." paging cell.
     
     - Parameter type: The type of cell to create.
     - Parameter at: The IndexPath of the object in the table view.
     */
    func createCell(type:RoomBubbleDataType, at indexPath:IndexPath) -> UITableViewCell?

    /**
     Create a collection view cell for this event.
     
     - Parameter at: The IndexPath of the object in the collection view.
     - Parameter for: The RoomBubbleData object to display.
     */
    func createCollectionViewCell(at indexPath:IndexPath, for roomBubbleData:RoomBubbleData) -> UICollectionViewCell?

    func unknownDevicesUpdate(unknownDevices: Int, unknownDeviceUsers: Int)
}
