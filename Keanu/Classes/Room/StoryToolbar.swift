//
//  StoryToolbar.swift
//  Keanu
//
//  Created by N-Pex on 31.05.19.
//

import KeanuCore
import AVKit

public protocol StoryToolbarDelegate: class {
    func didPressAudioButton()
}

open class StoryToolbar: RoomToolbar {
    public weak var storyDelegate: StoryToolbarDelegate?
    
    @IBAction func showAudioPlayerButtonPressed(_ sender: Any) {
        storyDelegate?.didPressAudioButton()
    }
}
