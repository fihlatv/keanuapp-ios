//
//  RoomViewController.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import MaterialComponents.MaterialSnackbar
import QuickLook
import ISEmojiView

open class RoomViewController: TableViewControllerWithToolbar, UINavigationControllerDelegate,
RoomToolbarDelegate, RoomDataSourceDelegate {

    static let kMessageSentDateShow: UInt = 5 * 60 * 1000 // Milliseconds
    
    /**
    How much data that is paged into the tableView. It is given as a fraction of the tableView height, so a value of 0.3 means have 30% more data scrollable.
    */
    static let kOffscreenBuffer: CGFloat = 0.5
    
    /**
     Change these NIBs to use different toolbars!
     */
    public static var toolbarNibNormalMode = RoomToolbar.nib
    public static var toolbarNibStoryMode = StoryToolbar.nib
    
    open var room: MXRoom? {
        didSet {
            if isViewLoaded {
                didChangeRoom(from: oldValue, to: room)
            }
        }
    }
    
    open var messageTableView: MessageTableView? {
        get {
            return self.tableView as? MessageTableView
        }
    }
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    open var roomListener: Any?
    open var observers: [NSObjectProtocol] = []
    open var dataSource: RoomDataSource?
    
    open var typingIndicator: TypingIndicator?
    open var attachmentPicker: AttachmentPicker?
    open var attachmentPickerCloseTapRecognizer: UITapGestureRecognizer?
    open var attachmentPickerHeight: NSLayoutConstraint?

    /**
     If we have not joined the room we are viewing, we show a "join room" view.
     */
    open var joinRoomView: JoinRoomView?

    /**
     Optional delegate for populating the attachment picker options
     */
    open weak var attachmentPickerDelegate: RoomViewControllerAttachmentPickerDelegate?
    
    open var isUpdating: Bool = false
    open var viewingLastMessage: Bool = true

    open var storyViewController: StoryViewController? = nil
    open var isStoryMode = false
    open var storyModeButton: UIBarButtonItem?
    
    /**
     A bar button item that is shown as a warning when there are unknown devices present in an encrypted room.
     */
    open var unknownDevicesBarButtonItem: UIBarButtonItem?
    
    /**
     The current string to show for unknown devices in the room, containing things like number of unknown devices and the number of users that have unknown devices. This string is shown in an alert when the user taps the warning button in the nav bar.
     */
    open var unknownDevicesWarningString: String?

    /**
     The attachment item used for QuickLook.
    */
    private var attachmentItem: AttachmentItem?

    open override var tableView: UITableView! {
        get {
            if !(super.tableView is MessageTableView) {
                // Replace the original table view with a MessageTableView!
                super.tableView = MessageTableView(frame: super.tableView.frame)
                super.tableView.backgroundColor = .keanuBackground
            }
            return super.tableView
        }
        set(value) {
            super.tableView = value
        }
    }
    
    
    /**
     If we have an outstanding network operation that can be canceled, this property will hold on to that.
     */
    private var currentOp: MXHTTPOperation?
    
    deinit {
        dataSource?.invalidate()
        dataSource = nil

        if let roomListener = roomListener {
            room?.removeListener(roomListener)
        }
        room = nil

        RoomManager.shared.currentlyViewedRoomId = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .keanuBackground

        navigationController?.delegate = self
        
        // System info button to show RoomSettingsViewController.
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            buttonType: .infoLight, target: self, action: #selector(didPressInfoButton))

        // Create the story mode "play" button.
        storyModeButton = UIBarButtonItem(materialIcon: .play_arrow, target: self,
                                          action: #selector(didPressStoryModeButton))
        navigationItem.rightBarButtonItems?.append(storyModeButton!)

        // Create the "unknown devices" warning button item.
        unknownDevicesBarButtonItem = UIBarButtonItem(materialIcon: .warning, target: self,
                                                      action: #selector(didPressUnknownDevicesButton))

        tableView.keyboardDismissMode = .onDrag
        tableView.scrollsToTop = false
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 2 // Arbitrary small value, needs to be smaller than the actual height of rows for correct layout when paging backwards.
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        updateToolbar()
        
        // Load typing indicator.
        typingIndicator = TypingIndicator.nib.instantiate(withOwner: nil, options: nil)[0] as? TypingIndicator

        // Load attachment picker.
        attachmentPicker = AttachmentPicker.nib.instantiate(withOwner: nil, options: nil)[0] as? AttachmentPicker
        attachmentPicker?.translatesAutoresizingMaskIntoConstraints = false
        attachmentPickerHeight = attachmentPicker?.autoSetDimension(.height, toSize: 0)

        attachmentPickerCloseTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeAttachmentPicker))
        if let recognizer = attachmentPickerCloseTapRecognizer {
            recognizer.isEnabled = false
            self.view.addGestureRecognizer(recognizer)
        }
        
        // Register cell types
        tableView.register(IncomingMessageCell.nib, forCellReuseIdentifier: IncomingMessageCell.defaultReuseId)
        tableView.register(OutgoingMessageCell.nib, forCellReuseIdentifier: OutgoingMessageCell.defaultReuseId)
        tableView.register(PagingCell.nib, forCellReuseIdentifier: PagingCell.defaultReuseId)
        tableView.register(NewMembersCell.nib, forCellReuseIdentifier: NewMembersCell.defaultReuseId)
        tableView.register(StatusEventCell.nib, forCellReuseIdentifier: StatusEventCell.defaultReuseId)
        tableView.register(UnknownDeviceCell.nib, forCellReuseIdentifier: UnknownDeviceCell.defaultReuseId)

        didChangeRoom(from: nil, to: room)
    }
    
    open func updateToolbar() {
        DispatchQueue.main.async {
            let nib = self.isStoryMode ? RoomViewController.toolbarNibStoryMode : RoomViewController.toolbarNibNormalMode
            guard let toolbar = nib.instantiate(withOwner: nil, options: nil)[0] as? RoomToolbar else {
                return
            }
            self.toolbarView = toolbar
            toolbar.delegate = self
            
            // If story mode toolbar, set delegate
            (toolbar as? StoryToolbar)?.storyDelegate = self.storyViewController
            
            // Call "didUpdateText" to initialize the controls to their proper values.
            self.didUpdateText(textView: toolbar.textInputView)
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        addObservers()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //tryToMarkAllMessagesAsRead()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        saveCurrentMessageText()
        removeObservers()
        stopAudio()
    }


    // MARK: UINavigationControllerDelegate

    public func navigationController(_ navigationController: UINavigationController,
                                     willShow viewController: UIViewController, animated: Bool) {
        if viewController == self {
            // Trigger rerendering of room name after return from RoomSettingsViewController.
            title = room?.summary?.displayname
        }
    }
    
//    open override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.transform = CGAffineTransform(translationX: 130, y: 0)
//    }
    
//    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return self.tableView.frame.height
//    }


    // MARK: Methods
    
    func createEmptyRoom(session: MXSession, roomCallback:((MXRoom?)->Void)? = nil) {
        // Just create a room with no current friends in it
        createRoom(withFriends: [], session: session, roomCallback: roomCallback)
    }
    
    func createRoom(withFriends friends: [Friend], session: MXSession, isPublic: Bool = false, encrypted: Bool = true, roomCallback:((MXRoom?)->Void)? = nil) {
        
        // Since cancelling room creation can be tricky, wait 5 seconds before
        // showing the "cancel" button. This will allow the operation to complete
        // under "normal" conditions.
        let workItem = DispatchWorkItem { [weak self] in
            if let self = self, !self.workingOverlay.isHidden {
                self.workingOverlay.message = "Creating room... tap to cancel".localize()
                self.workingOverlay.tapHandler = {
                    if let currentOp = self.currentOp, !currentOp.isCancelled {
                        currentOp.cancel()
                        self.dismiss()
                    }
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: workItem)

        workingOverlay.message = nil
        workingOverlay.tapHandler = nil
        workingOverlay.isHidden = false
        
        var params:[String:Any] = [:]
        if (isPublic) {
            params["visibility"] = "public"
            params["invite"] = friends.map { $0.matrixId }
            //params["is_direct"] = NSNumber(value: (friends.count == 1))
            var initialState:[[String:Any]] = [
                ["type": kMXEventTypeStringRoomHistoryVisibility,
                 "content": ["history_visibility": kMXRoomHistoryVisibilityShared]
                ],
                ["type": kMXEventTypeStringRoomGuestAccess,
                 "content": ["guest_access": kMXRoomGuestAccessCanJoin]
                ],
                ["type": kMXEventTypeStringRoomJoinRules,
                 "content": ["join_rule": kMXRoomJoinRulePublic]
                ]
            ]
            if encrypted {
                initialState.append(["type": kMXEventTypeStringRoomEncryption,
                                     "content": ["algorithm": kMXCryptoMegolmAlgorithm]])
            }
            params["initial_state"] = initialState
            params["preset"] = "public_chat"
        } else {
            params["visibility"] = "private"
            params["invite"] = friends.map { $0.matrixId }
            params["is_direct"] = NSNumber(value: (friends.count == 1))
            var initialState:[[String:Any]] = [
                ["type": kMXEventTypeStringRoomHistoryVisibility,
                 "content": ["history_visibility": kMXRoomHistoryVisibilityInvited]
                ],
                ["type": kMXEventTypeStringRoomGuestAccess,
                 "content": ["guest_access": kMXRoomGuestAccessForbidden]
                ],
                ["type": kMXEventTypeStringRoomJoinRules,
                 "content": ["join_rule": kMXRoomJoinRuleInvite]
                ]
            ]
            if encrypted {
                initialState.append(["type": kMXEventTypeStringRoomEncryption,
                                     "content": ["algorithm": kMXCryptoMegolmAlgorithm]])
            }
            params["initial_state"] = initialState
            params["preset"] = "private_chat"
        }

        currentOp = session.createRoom(parameters: params) { (createRoomResponse)  in
            self.currentOp = nil
            workItem.cancel()
            self.workingOverlay.isHidden = true
            if
                createRoomResponse.isSuccess,
                let response = createRoomResponse.value,
                let room = session.room(withRoomId: response.roomId) {
                self.room = room

                // If we created a new 1:1 room with a user, it's now our friend
                // and we need to keep the reference to the room, since that is
                // expensive to evaluate and therefore won't be done automatically.
                if friends.count == 1,
                    let friend = friends.first,
                    friend.privateRoom == nil {

                    friend.privateRoom = room
                    FriendsManager.shared.add(friend)
                }

                if let roomCallback = roomCallback {
                    roomCallback(self.room)
                } else if friends.count == 0 {
                    // If we created an empty room, open settings
                    self.showSettings()
                }
            } else {
                // Failed
                let message = MDCSnackbarMessage()
                message.duration = 3
                message.text = "Failed to create room".localize()
                message.completionHandler = { userInitiated in
                    self.navigationController?.popViewController(animated: true)
                }
                MDCSnackbarManager.show(message)
                roomCallback?(nil)
            }
        }
    }
    
    open func updateDataSource() {
        dataSource?.invalidate()
        if let room = room {
            if isStoryMode
            {
                dataSource = StoryDataSource(room: room)
                dataSource?.delegate = self

                // Story mode
                storyViewController = UIApplication.shared.theme.createStoryViewController()

                if let storyViewController = storyViewController {
                    storyViewController.dataSource = dataSource
                    (dataSource as? StoryDataSource)?.storyDelegate = storyViewController
                    storyViewController.renderDelegate = self

                    addChild(storyViewController)

                    view.addSubview(storyViewController.view)
                    storyViewController.didMove(toParent: self)
                    storyViewController.view.autoPinEdge(.top, to: .top, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.bottom, to: .bottom, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.leading, to: .leading, of: tableView, withOffset: 0)
                    storyViewController.view.autoPinEdge(.trailing, to: .trailing, of: tableView, withOffset: 0)

                    updateToolbar()
                }
            }
            else {
                dataSource = RoomDataSource(room: room)
                dataSource?.delegate = self
                tableView.dataSource = dataSource
                tableView.delegate = dataSource
                updateToolbar()
            }
        }
        else {
            tableView.dataSource = nil
            tableView.delegate = nil
            updateToolbar()
        }
    }
    
    open func reloadData() {
        if let storyViewController = storyViewController {
            storyViewController.reloadData()
        } else {
            self.tableView.reloadData()
        }
    }
    
    open func didChangeRoom(from: MXRoom?, to: MXRoom?, useStoryMode: Bool = false) {
        self.dataSource?.invalidate()
        self.dataSource = nil
        if let oldRoom = from, let roomListener = self.roomListener {
            oldRoom.removeListener(roomListener)
        }
        roomListener = nil
        self.title = nil
        unknownDevicesUpdate(unknownDevices: 0, unknownDeviceUsers: 0) // Reset

        // Remove old story mode view, if any
        if let storyViewController = self.storyViewController {
            storyViewController.view.removeFromSuperview()
            storyViewController.removeFromParent()
            storyViewController.didMove(toParent: nil)
            storyViewController.dataSource = nil
            self.storyViewController = nil
        }
        
        joinRoomView?.removeFromSuperview()
        navigationItem.rightBarButtonItem?.isEnabled = true
        isStoryMode = useStoryMode
        
        if let room = to {
            self.title = room.summary?.displayname
            updateDataSource()

            // Add listener
            roomListener = room.listen(toEventsOfTypes: [kMXEventTypeStringRoomEncryption]) { [weak self] (event, direction, state) in
                guard let self = self else {return}
                if let event = event, event.isState() {
                    self.didUpdateState()
                }
            }
            
            if room.summary.membership == .invite {
                navigationItem.rightBarButtonItem?.isEnabled = false
                showJoinRoomView()
            }
        } else {
            updateDataSource()
        }
        reloadData()
        didUpdateState()
        updateStoryModeButtonTitle()
        
        // Tell the app delegate which room is currently open. Need this to decide when to show in app notifications.
        RoomManager.shared.currentlyViewedRoomId = to?.roomId
    }

    open func addObservers() {
//        self.observers.append(
//            observe(\.tableView.bounds, options: [.new]) { (tableView, change) in
//                if let storyViewController = self.storyViewController {
//                    storyViewController.view.frame = self.tableView.frame
//                }
//        })
    }
    
    open func removeObservers() {
        for observer in self.observers {
            NotificationCenter.default.removeObserver(observer)
        }
        observers.removeAll()
    }
    
    var toolbar: RoomToolbar? {
        return self.toolbarView as? RoomToolbar
    }
    
    open func didScroll(offset: CGFloat) {
        if !isStoryMode,
            let messageTableView = messageTableView,
            !messageTableView.isUpdating {
            messageTableView.didScroll()
            DispatchQueue.main.async {
                self.checkIfPagingNeeded()
            }
        }
    }
    
    open func checkIfPagingNeeded() {
        // Need to page in more data?
        if let dataSource = dataSource, !dataSource.isPaging {
            if isStoryMode {
                if let storyViewController = self.storyViewController, storyViewController.needsForwardsPaging(),
                    dataSource.canPageForwards {
                    dataSource.pageForwards()
                }
                
                // Otherwise, for story mode, we only do that when you scroll
                return
            }
            let offsetY = self.tableView.contentOffset.y
            let contentHeight = self.tableView.contentSize.height
            if dataSource.canPageBackwards, (!tableView.viewportIsFilled || offsetY < (self.tableView.frame.height * RoomViewController.kOffscreenBuffer)) {
                dataSource.pageBackwards()
            } else if offsetY > (contentHeight - self.tableView.frame.height * (1 + RoomViewController.kOffscreenBuffer)), dataSource.canPageForwards {
                dataSource.pageForwards()
            }
        }
    }
    
    public func didUpdateText(textView: UITextView) {
        //Check if the text state changes from having some text to some or vice versa
        let hasText = textView.text.count > 0
        //if hasText != self.state.hasText  {
        //    self.state.hasText = hasText
        //    self.didUpdateState()
        //}
        
        //Everytime the textview has text and a notification comes through we are 'typing' otherwise     we are done typing
        if (hasText) {
            self.isTyping()
        } else {
            self.didFinishTyping()
        }
        if isStoryMode {
            if let room = room, let myUser = room.myUser, room.isAdmin(userId: myUser.userId) {
                self.toolbar?.sendButton.isHidden = !hasText
                self.toolbar?.microphoneButton.isHidden = hasText
            } else {
                // Not an admin, so we don't show the "add media" button. Instead, always show the send button, but only enable it if we have text.
                // Update - for now, disable sending if you are not admin!
                self.toolbar?.sendButton.isEnabled = false //hasText
                self.toolbar?.sendButton.isHidden = false
                self.toolbar?.microphoneButton.isHidden = true
            }
        } else {
            self.toolbar?.sendButton.isEnabled = hasText
            self.toolbar?.sendButton.isHidden = !hasText
            self.toolbar?.microphoneButton.isHidden = hasText
        }
        
        // Scroll to bottom when typing
        if hasText, !isStoryMode, let last = tableView.lastIndexPath {
            tableView.scrollToRow(at: last, at: .top, animated: true)
        }
    }

    @objc open func didPressInfoButton() {
        showSettings()
    }

    @objc open func didPressStoryModeButton() {
        didChangeRoom(from: room, to: room, useStoryMode: !isStoryMode)
    }
    
    open func showSettings() {
        let vc = UIApplication.shared.theme.createRoomSettingsViewController()
        vc.room = room
        vc.delegate = self

        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc open func didPressUnknownDevicesButton() {
        guard let alertMessage = unknownDevicesWarningString else { return }
        let alert = AlertHelper.build(message: alertMessage, title: "Unknown devices".localize(), style: .alert, actions: [AlertHelper.defaultAction()])
        present(alert, animated: true)
    }
    
    open func didUpdateState() {
        updateInputPlaceholder()
        //toolbar?.sendButton.isEnabled = self.state.hasText
    }
    
    open func updateStoryModeButtonTitle() {
        storyModeButton?.materialIcon = isStoryMode ? .storage : .play_arrow
    }
    
    open func updateInputPlaceholder() {
        room?.state({ (state:MXRoomState?) in
            DispatchQueue.main.async {
                self.toolbar?.inputPlaceholder = "Send a message"
            }
        })
    }
    
    func isTyping() {
        room?.sendTypingNotification(typing: true, timeout: 10, completion: { (response) in
        })
    }
    
    func didFinishTyping() {
        room?.sendTypingNotification(typing: false, timeout: 0, completion: { (response) in
        })
    }
    
    public var showTypingIndicator: Bool = false {
        didSet {
            guard !isStoryMode else {return}
            displayTypingIndicator(showTypingIndicator)
        }
    }
    
    open func displayTypingIndicator(_ showTypingIndicator: Bool) {
        let scrollToLast = viewingLastMessage
        if showTypingIndicator, let typingIndicator = self.typingIndicator {
            tableView.tableFooterView = typingIndicator
        } else {
            UIView.beginAnimations(nil, context: nil)
            tableView.tableFooterView = nil
            UIView.commitAnimations()
        }
        if scrollToLast, let last = tableView.lastIndexPath {
            tableView.scrollToRow(at: last, at: .top, animated: true)
        }
    }
    
    func saveCurrentMessageText() {
    }
    
    func restoreCurrentMessageText() {
        
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        guard let text = toolbar?.textInputView.text else { return }
        toolbar?.clearInput()
        if let room = room {
            sendText(room: room, text: text)
        }
    }

    /**
     In a story mode, the "add media" button has been pressed. Open up the StoryAddMediaViewController.
     */
    @IBAction func addStoryMediaButtonPressed(_ sender: Any) {
        if isStoryMode {
            let vc = UIApplication.shared.theme.createStoryAddMediaViewController()
            vc.delegate = self

            navigationController?.pushViewController(vc, animated: true)
        }
    }
        
    @IBAction open func microphoneButtonPressed(_ sender: Any) {
        // Close keyboard
        self.view.endEditing(true)
        
        AudioRecorder.showIn(view: self.view, success: { (url:URL) in
            self.sendAudio(audioUrl: url)
        })
    }
    
    open func showJoinRoomView() {
        if joinRoomView == nil {
            joinRoomView = JoinRoomView.nib.instantiate(withOwner: nil, options: nil)[0] as? JoinRoomView
        }
        if let joinRoomView = joinRoomView {
            var roomName = ""
            if let room = self.room {
                roomName = room.summary?.displayname ?? ""
                joinRoomView.room = room
            }
            joinRoomView.titleLabel.text = String(format: NSLocalizedString("You have been invited to the room ´%@´.", comment: "Title for join room view shown in room view controller"), roomName)
            
            joinRoomView.acceptButtonCallback = {() -> Void in
                guard let room = self.room else { return }
                DispatchQueue.main.async {
                UIApplication.shared.joinRoom(room.roomId) { success, room in
                        if success {
                            UIView.animate(withDuration: 0.5, animations: {
                                joinRoomView.alpha = 0.0
                            }, completion: { (success) in
                                // Re-initialize the room
                                self.didChangeRoom(from: self.room, to: room)
                            })
                        }
                    }
                }
            }

            joinRoomView.declineButtonCallback = { [weak self] in
                if let room = self?.room {
                    UIApplication.shared.declineRoomInvite(roomId: room.roomId) { _ in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
            self.view.addSubview(joinRoomView)
            joinRoomView.autoPinEdgesToSuperviewEdges()
            DispatchQueue.main.async {
                // Async calls in UpdateToolbar may result in that the toolbar
                // is shown on top of the join room view. Therefore, do a similar
                // async call here, to ensure we are topmost!
                joinRoomView.becomeFirstResponder()
                self.view.bringSubviewToFront(joinRoomView)
            }
        }
    }

    /**
     Walks the UIView tree of all `MessageCell`s to find `AudioPlayer`s and stops them.
     */
    private func stopAudio() {
        for s in 0 ..< tableView.numberOfSections {
            for r in 0 ..< tableView.numberOfRows(inSection: s) {
                if let cell = tableView.cellForRow(at: IndexPath(row: r, section: s)) as? MessageCell {
                    let players = searchUiView(ofType: AudioPlayer.self, in: cell.contentView)

                    for player in players {
                        player.didPressPause(sender: player)
                    }
                }
            }
        }
    }

    /**
     Recursively searches a `UIView` hirarchy by `UIView`s of the given type.

     - parameter type: The type of `UIView`s to search.
     - parameter root: The root view to start the search from.
     - returns: A list of all `UIView`s with the given type in the given view hirarchy.
     */
    private func searchUiView<T: UIView>(ofType type: T.Type, in root: UIView) -> [T] {
        var result = [T]()

        for view in root.subviews {
            if let tView = view as? T {
                result.append(tView)
            }
            else {
                result.append(contentsOf: searchUiView(ofType: type, in: view))
            }
        }

        return result
    }


    //MARK: RoomDataSourceDelegate
    open func didChange(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool) {
        if let storyViewController = storyViewController {
            //TODO
            storyViewController.didChange(deleted: deleted, added: added, changed: changed, live: live, disableScroll: disableScroll)
            return
        }
        messageTableView?.didChange(deleted: deleted, added: added, changed: changed, live: live, disableScroll: disableScroll)
    }

    open func pagingStarted(direction: MXTimelineDirection) {
        let indexPath = IndexPath(row: 0, section: dataSource?.sectionHeader ?? 0)
        didChange(deleted: nil, added: [indexPath], changed: nil, live: false, disableScroll: false)
    }
    
    open func pagingDone(direction: MXTimelineDirection, newIndexPaths:[IndexPath], changedIndexPaths:[IndexPath], success: Bool, scrollToIndexPath: IndexPath? = nil) {
        let indexPath = IndexPath(row: 0, section: dataSource?.sectionHeader ?? 0)
        didChange(deleted: [indexPath], added: newIndexPaths, changed: changedIndexPaths, live: direction == .forwards, disableScroll: scrollToIndexPath != nil)
        
        // Scroll to a certain message (e.g. the last read one)?
        if let scrollToIndexPath = scrollToIndexPath, self.tableView.isValidIndexPath(indexPath: scrollToIndexPath) {
            self.tableView.scrollToRow(at: scrollToIndexPath, at: .top, animated: false)
        }
        if success {
            DispatchQueue.main.async {
                // Check if we need more data to fill the screen/buffer
                self.checkIfPagingNeeded()
            }
        }
    }
    
    open func onTypingUsersChange(_ room: MXRoom) {
        DispatchQueue.main.async {
            if let typingIndicator = self.typingIndicator {
                typingIndicator.populate(room)
                self.showTypingIndicator = !typingIndicator.isHidden
            }
        }
    }
    
    open func createCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UITableViewCell? {
        switch roomBubbleData.bubbleType {
        case .message, .messageEncrypted:
            let cell = tableView.dequeueReusableCell(withIdentifier: roomBubbleData.isIncoming ? IncomingMessageCell.defaultReuseId : OutgoingMessageCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .roomNameChange, .roomTopicChange:
            let cell = tableView.dequeueReusableCell(withIdentifier: StatusEventCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .newMember:
            let cell = tableView.dequeueReusableCell(withIdentifier: NewMembersCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case .devicesAdded:
            let cell = tableView.dequeueReusableCell(withIdentifier: UnknownDeviceCell.defaultReuseId, for: indexPath)
            if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        case let .custom(ident, renderer):
            let cell = tableView.dequeueReusableCell(withIdentifier: ident, for: indexPath)
            if let renderer = renderer {
                // An explicit renderer was given when the bubble data was created. Use that!
                renderer(cell, roomBubbleData, self)
            } else if let renderCell = cell as? RoomBubbleDataRenderer {
                renderCell.render(roomBubbleData: roomBubbleData, delegate:self)
            }
            return cell
        default:
            return nil
        }
    }

    public func createCollectionViewCell(at indexPath: IndexPath, for roomBubbleData: RoomBubbleData) -> UICollectionViewCell? {
        if let storyViewController = self.storyViewController {
            return storyViewController.createCollectionViewCell(at: indexPath, for: roomBubbleData)
        }
        return nil
    }
    
    open func createCell(type: RoomBubbleDataType, at indexPath: IndexPath) -> UITableViewCell? {
        switch type {
        case .paging:
            let cell = tableView.dequeueReusableCell(withIdentifier: PagingCell.defaultReuseId, for: indexPath)
            return cell
        default:
            return nil
        }
    }
    
    public func unknownDevicesUpdate(unknownDevices: Int, unknownDeviceUsers: Int) {
        guard let unknownDevicesBarButtonItem = unknownDevicesBarButtonItem else { return }
        if unknownDevices == 0 {
            // Reset warning string and remove bar button item, if present
            unknownDevicesWarningString = nil
            self.navigationItem.rightBarButtonItems = self.navigationItem.rightBarButtonItems?.filter({ (item) -> Bool in
                return item != unknownDevicesBarButtonItem
            })
        } else {
            // Store the warning string, formatted
            if unknownDeviceUsers == 1 {
                if unknownDevices == 1 {
                    unknownDevicesWarningString = "1 user has an unknown device".localize()
                } else {
                    unknownDevicesWarningString = "1 user has % unknown devices".localize(value: "\(unknownDevices)")
                }
            } else {
                unknownDevicesWarningString = "% users have % unknown devices".localize(values: "\(unknownDeviceUsers)", "\(unknownDevices)")
            }
            
            // Make sure bar button item is added, if not already
            if !(self.navigationItem.rightBarButtonItems ?? []).contains(unknownDevicesBarButtonItem) {
                var newItems = (self.navigationItem.rightBarButtonItems ?? [])
                newItems.append(unknownDevicesBarButtonItem)
                self.navigationItem.rightBarButtonItems = newItems
            }
        }
    }
    
    open func sendText(room: MXRoom, text: String) {
        var localEcho:MXEvent?
        room.sendTextMessage(text, localEcho: &localEcho, completion: { (response) in
            //TODO - handler error
        })
        insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
    }
    
    open func sendImage(image:UIImage?) {
        // Reduce size to 1080 x 1080, if bigger.
        if let room = room,
            let image = image?.reduce(),
            let data = image.jpegData() {
            
            var localEcho: MXEvent?
            
            // Send the image. TODO - add an interim step, where you can cancel sending.
            room.sendImage(data: data, size: image.size,
                           mimeType: UtiHelper.mimeJpeg, thumbnail: nil,
                           localEcho: &localEcho)
            { response in
                //TODO - handle response
            }
            
            insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        }
    }
    
    open func sendAudio(audioUrl: URL?) {
        if let room = self.room,
            let audioUrl = audioUrl {
            var localEcho:MXEvent?
            room.sendAudioFile(localURL: audioUrl, mimeType: "audio/x-m4a", localEcho: &localEcho) { (response) in
                do {
                    // TODO - Only remove on success?
                    try FileManager.default.removeItem(at: audioUrl)
                } catch {
                    print(error)
                }
            }
            insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        }
    }
    
    open func sendVideo(videoUrl: URL?) {
        if let room = room,
            let videoUrl = videoUrl {
            var localEcho: MXEvent?
            room.sendVideo(localURL: videoUrl,
                           thumbnail: UIImage.thumbnailForVideo(at: videoUrl),
                           localEcho: &localEcho)
            { response in
                //TODO - handle response
            }
            
            insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        }
    }
    
    open func sendFile(url: URL?, mimeType:String) {
        guard let url = url else { return }
        let deleteTempFile = {
            do {
                try FileManager.default.removeItem(at: url)
            } catch {}
        }
        
        if let room = room {
            var localEcho: MXEvent?
            room.sendFile(localURL: url, mimeType: mimeType, localEcho: &localEcho) { response in
                // TODO - Only remove on success?
                deleteTempFile()
            }
            insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
        } else {
            deleteTempFile()
        }
    }
    
    open func insertLocalEcho(event:MXEvent?, roomState:MXRoomState) {
        dataSource?.insertLocalEcho(event: event, roomState: roomState)
    }

    /**
     Close attachment picker, when user tries to enter text.
     */
    override func keyboardWillShow(_ notification: NSNotification) {
        closeAttachmentPicker()

        super.keyboardWillShow(notification)
    }
}

extension RoomViewController : UIPopoverPresentationControllerDelegate {
    public func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        popoverPresentationController.sourceView = toolbar?.attachmentButton
    }
}

// MARK: Attachment picking
extension RoomViewController: UIImagePickerControllerDelegate {

    @objc public func closeAttachmentPicker() {
        attachmentPickerHeight?.constant = 0

        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }) { [weak self] _ in
            self?.attachmentPickerCloseTapRecognizer?.isEnabled = false
            self?.attachmentPicker?.removeFromSuperview()
        }
    }
    
    @IBAction open func attachButtonPressed(_ sender: Any) {
        guard let attachmentPicker = attachmentPicker, let toolbar = toolbar else {
            return
        }

        // Close keyboard.
        toolbar.textInputView.resignFirstResponder()
            
        attachmentPicker.clearActions()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            attachmentPicker.addAction("", id: "camera") { [weak self] in
                self?.closeAttachmentPicker()

                // Show camera
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = ["public.image", "public.movie"]
                imagePicker.delegate = self
                self?.present(imagePicker, animated: true)
            }
        }

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            attachmentPicker.addAction("", id: "photoLibrary") { [weak self] in
                self?.closeAttachmentPicker()

                // Show library
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = ["public.image", "public.movie"]
                imagePicker.delegate = self
                self?.present(imagePicker, animated: true)
            }
        }

        // Stickers
        if StickerManager.hasStickers {
            attachmentPicker.addAction("", id: "stickers") { [weak self] in
                self?.closeAttachmentPicker()

                // Pick sticker!
                let vc = UIApplication.shared.theme.createStickerPackViewController()
                vc.pickDelegate = self

                self?.present(UINavigationController(rootViewController: vc), animated: true)
            }
        }

        // Add story editor
        attachmentPicker.addAction("", id: "story") { [weak self] in
            self?.closeAttachmentPicker()

            let vc = UIApplication.shared.theme.createStoryEditorViewController()
            vc.session = self?.room?.mxSession
            vc.delegate = self

            self?.present(UINavigationController(rootViewController: vc), animated: true)
        }

        // Add stuff from delegate
        attachmentPickerDelegate?.addActions(attachmentPicker: attachmentPicker, room: room)

        // Bail out if nothing to show.
        guard attachmentPicker.actionCount() != 0 else { return }

        if attachmentPicker.superview == nil {
            view.addSubview(attachmentPicker)
        }

        attachmentPicker.autoPinEdge(.leading, to: .leading, of: toolbar)
        attachmentPicker.autoPinEdge(.trailing, to: .trailing, of: toolbar)
        attachmentPicker.autoPinEdge(.bottom, to: .top, of: toolbar)
        view.layoutIfNeeded()

        self.attachmentPickerHeight?.constant = attachmentPicker.defaultHeight

        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        })

        attachmentPickerCloseTapRecognizer?.isEnabled = true
    }

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
        info: [UIImagePickerController.InfoKey : Any]) {

        picker.dismiss(animated: true)

        guard let _ = room,
            let mediaType = info[.mediaType] as? String
        else {
            return
        }
        
        if mediaType == UtiHelper.typeImage {
            sendImage(image: (info[.editedImage] as? UIImage ?? info[.originalImage] as? UIImage))
        } else if mediaType == UtiHelper.typeMovie {
            sendVideo(videoUrl: info[.mediaURL] as? URL)
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

extension RoomViewController: RoomBubbleDataRendererDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate {

    func viewFriendProfile(friend: Friend) {
        if let room = room,
            let myUser = room.myUser,
            friend.matrixId == myUser.userId,
            let account = MXKAccountManager.shared()?.account(forUserId: myUser.userId)
            {
            // Special case, tapping ourselves
            let profileVc = MyProfileViewController()
            profileVc.account = account
            self.navigationController?.pushViewController(profileVc, animated: true)
            return
        }
        let profileVc = UIApplication.shared.theme.createProfileViewController()
        profileVc.friend = friend
        self.navigationController?.pushViewController(profileVc, animated: true)
    }
    
    public func didTapIncomingAvatar(roomBubbleData: RoomBubbleData, view: UIView) {
        guard let firstEvent = roomBubbleData.events.first else {return}

        var actions:[UIAlertAction] = []
        
        let viewProfileAction:((UIAlertAction?) -> Void) = { [weak self] _ in
            if let self = self, let senderId = firstEvent.sender, let friend = FriendsManager.shared.get(senderId) {
                self.viewFriendProfile(friend: friend)
            }
        }
        
        // Action to view profile.
        actions.append(AlertHelper.defaultAction("View profile".localize(), handler: viewProfileAction))
        
        // Action to send quick reaction
        // TODO - Check that the event is of a type we can react to!
        actions.append(AlertHelper.defaultAction("Quick Reaction".localize())
        { [weak self] action in
            guard let self = self else { return }
            self.didTapAddQuickReaction(roomBubbleData: roomBubbleData)
        })
        
        // Action to forward a message.
        actions.append(AlertHelper.defaultAction("Forward".localize())
        { [weak self] action in
            guard let self = self else { return }
            UIApplication.shared.forwardEvent(presentingViewController: self, event: firstEvent)
        })
        actions.append(AlertHelper.cancelAction())
        let alert = AlertHelper.build(message: nil, title: nil, style: .actionSheet, actions: actions)
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = view.bounds
            popoverController.permittedArrowDirections = []
        }
        present(alert, animated: true)
    }
    
    public func didTapOutgoingAvatar(roomBubbleData: RoomBubbleData, view: UIView) {
        guard let firstEvent = roomBubbleData.events.first else {return}
        
        var actions:[UIAlertAction] = []
        
        let viewProfileAction:((UIAlertAction?) -> Void) = { [weak self] _ in
            if let self = self, let myUser = self.room?.myUser {
                self.viewFriendProfile(friend: FriendsManager.shared.getOrCreate(myUser))
            }
        }
        
        // Action to view profile.
        actions.append(AlertHelper.defaultAction("View profile".localize(), handler: viewProfileAction))
        
        // Action to send quick reaction
        // TODO - Check that the event is of a type we can react to!
        actions.append(AlertHelper.defaultAction("Quick Reaction".localize())
        { [weak self] action in
            guard let self = self else { return }
            self.didTapAddQuickReaction(roomBubbleData: roomBubbleData)
        })

        if firstEvent.sentState == MXEventSentStateFailed {
            // Action to delete a failed message.
            actions.append(AlertHelper.defaultAction("Delete message".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.deleteOutgoingEvent(event: firstEvent)
            })
            // Action to retry sending a failed message.
            actions.append(AlertHelper.defaultAction("Retry".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.resendMessage(event: firstEvent)
            })
        } else if firstEvent.sentState == MXEventSentStateSent {
            // Action to resend a message.
            actions.append(AlertHelper.defaultAction("Resend".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.resendMessage(event: firstEvent)
            })
            
            // Action to forward a message.
            actions.append(AlertHelper.defaultAction("Forward".localize())
            { [weak self] action in
                guard let self = self else { return }
                UIApplication.shared.forwardEvent(presentingViewController: self, event: firstEvent)
            })
            
            // Action to redact a message.
            actions.append(AlertHelper.defaultAction("Redact".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.redactMessage(event: firstEvent)
            })

        } else {
            // Action to cancel a message.
            actions.append(AlertHelper.defaultAction("Cancel".localize())
            { [weak self] action in
                guard let self = self else { return }
                self.dataSource?.cancelMessage(event: firstEvent)
            })
        }
        
        if actions.count == 1 {
            viewProfileAction(nil)
        } else {
            actions.append(AlertHelper.cancelAction())
            let alert = AlertHelper.build(message: nil, title: nil, style: .actionSheet, actions: actions)
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = view
                popoverController.sourceRect = view.bounds
                popoverController.permittedArrowDirections = []
            }
            present(alert, animated: true)
        }
    }
    
    public func didTapMediaAttachmentItem(roomBubbleData: RoomBubbleData, view: UIView?, attachment: MXKAttachment?) {
        guard let attachment = attachment, let event = roomBubbleData.events.first else {
            return
        }

        var ignore = false

        workingOverlay.message = "Loading attachment... tap to cancel".localize()
        workingOverlay.tapHandler = {
            self.workingOverlay.tapHandler = nil
            self.workingOverlay.isHidden = true
            ignore = true
        }

        workingOverlay.isHidden = false

        attachmentItem = AttachmentItem(attachment, event: event) { item in
            if ignore {
                return
            }

            self.workingOverlay.tapHandler = nil

            if let error = item.error {
                return AlertHelper.present(self, message: error.localizedDescription)
            }

            let showShare = {
                let vc = UIActivityViewController(activityItems: [item], applicationActivities: nil)

                if let view = view {
                    vc.popoverPresentationController?.sourceView = view
                    vc.popoverPresentationController?.sourceRect = view.bounds
                }

                self.present(vc, animated: true)
            }


            if attachment.type == MXKAttachmentTypeVideo {
                self.present(AttachmentViewController.instantiate(item), animated: true)
            }
            else if attachment.type == MXKAttachmentTypeFile {
                if #available(iOS 11.0, *), attachment.isHtml {
                    self.present(AttachmentViewController.instantiate(item), animated: true)
                }
                else if QLPreviewController.canPreview(item) {
                    let vc = QLPreviewController()
                    vc.dataSource = self
                    vc.delegate = self

                    self.present(vc, animated: true)
                }
                else {
                    showShare()
                }
            }
            else if (attachment.type == MXKAttachmentTypeAudio) {
                showShare()
            }
            else {
                // Show photo viewer
                guard let event = roomBubbleData.events.first, let room = self.room else {return}

                let photoStreamHandler = PhotoStreamHandler()
                photoStreamHandler.fetchImagesAsync(room: room, initialEvent: event, onStart: nil) { images, initialImage in
                    let browser = PhotosViewController(photoStreamHandler: photoStreamHandler, referenceView: view)
                    //browser.delegate = self
                    self.present(browser, animated: true)
                }
            }

            // Delay until after next scene is shown, to reduce flicker.
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: {
                self.workingOverlay.isHidden = true
            })
        }
    }
    
    public func didTapAddQuickReaction(roomBubbleData: RoomBubbleData) {
        // Close keyboard
        self.view.endEditing(true)
        
        // Show emoji picker
        QuickReactionPicker.showIn(view: self.view, didPickEmoji: { [weak self] (emoji:String) in
            guard let self = self else {return}
            self.didTapQuickReaction(roomBubbleData: roomBubbleData, reaction: emoji)
        })
    }
    
    public func didTapQuickReaction(roomBubbleData: RoomBubbleData, reaction: String) {
        if let room = self.room, let event = roomBubbleData.events.first {
            if let aggregatedReactions = room.summary.mxSession.aggregations {
                let userHasReacted = aggregatedReactions.aggregatedReactions(onEvent: event.eventId, inRoom: room.roomId)?.reactions.contains(where: { (reactionCount) -> Bool in
                    return reactionCount.reaction == reaction && reactionCount.myUserHasReacted
                }) ?? false
                
                if userHasReacted {
                    aggregatedReactions.removeReaction(reaction, forEvent: event.eventId, inRoom: room.roomId, success: {
                    }) { (error) in
                        print(error)
                    }
                } else {
                    aggregatedReactions.addReaction(reaction, forEvent: event.eventId, inRoom: room.roomId, success: {
                    }) { (error) in
                        print(error)
                    }
                }
            }
        }
    }
    
    public func unknownDevicesVerify(roomBubbleData: RoomBubbleData, devices: [String:[String]]?, view: UIView?) {
        guard let devices = devices, devices.count > 0 else {return}
        if devices.count == 1 {
            guard let firstUserId = devices.keys.first, let friend = FriendsManager.shared.get(firstUserId) else {return}
            let devicesVC = DevicesViewController()
            devicesVC.friend = friend
            self.navigationController?.pushViewController(devicesVC, animated: true)
        } else {
            // More than 1 - Open room info in "show unknown devices" mode
            showSettings()
        }
    }
    
    public func requestRoomKeys(roomBubbleData: RoomBubbleData) {
        if let session = room?.mxSession,
            let event = roomBubbleData.events.first {
            session.crypto.reRequestRoomKey(for: event)
        }
    }


    // MARK: QLPreviewControllerDataSource

    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return attachmentItem == nil ? 0 : 1
    }

    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return attachmentItem!
    }


    // MARK: QLPreviewControllerDelegate

    public func previewControllerDidDismiss(_ controller: QLPreviewController) {
        // Trigger temp file destruction.
        // If user opens the same attachment again, there's going to be a race
        // condition otherwise and the QLPreviewController will show an empty file.
        attachmentItem = nil
    }
}

extension RoomViewController: RoomSettingsViewControllerDelegate {
    public func didArchiveRoom(_ roomSettingsViewController: RoomSettingsViewController) {
        if let room = roomSettingsViewController.room {
            room.summary.isArchived = true
        }
        let _ = UIApplication.shared.popToChatListViewController()
        dismiss(animated: true)
    }
    
    public func didLeaveRoom(_ roomSettingsViewController: RoomSettingsViewController) {
        self.room = nil
        let _ = UIApplication.shared.popToChatListViewController()
        dismiss(animated: true)
    }
}

extension RoomViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}

extension RoomViewController: StoryAddMediaViewControllerDelegate {
    public func didSelectMedia(_ media: [MediaInfo]) -> Bool {
        for m in media {
            if m.isImage {
                sendImage(image: m.image)
            } else if m.isAudio {
                sendAudio(audioUrl: m.url)
            } else if m.isVideo {
                sendVideo(videoUrl: m.url)
            }
        }
        return true
    }
}

extension RoomViewController: StoryEditorViewControllerDelegate {
    public func onSaveStory(fileName: String, content: String) {
        guard let tempUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(fileName) else { return }
        do {
            try content.write(to: tempUrl, atomically: false, encoding: .utf8)
            sendFile(url: tempUrl, mimeType: "text/html")
        }
        catch {}
    }
}
