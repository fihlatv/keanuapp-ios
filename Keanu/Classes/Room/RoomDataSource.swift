//
//  RoomDataSource.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore

/**
 Data source for a chat.
 */
open class RoomDataSource: NSObject, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource {
    
    /**
     Time number of seconds after which we cancel a pending outgoing message and retry.
     */
    public static let kSendTimeout:TimeInterval = 3600
    
    /**
     Static factory callback for creating bubbles that are not handled by the Keanu lib, or to modify ones created by Keanu. Will be called after the base lib has tried to create a bubble.
     
     The callback will have the following parameters:
     - roomBubbleData: The bubble created by the lib, or nil if none.
     - event: The event to create a bubble for.
     - roomState: Room state at the time just before the event.
     */
    public static var bubbleProcessor:((_ roomDataSource:RoomDataSource, _ event:MXEvent, _ roomBubbleData:RoomBubbleData?, _ roomState:MXRoomState) -> RoomBubbleData?)?
    
    open weak var delegate:RoomDataSourceDelegate?
    public var pagingDirection:MXTimelineDirection? = nil
    public var pagingSize:UInt = 10
    
    public var sectionHeader = 0
    public var sectionMessages = 1

    /**
     True if the last item in the "roomBubbleData" array should be displayed at top of table (Index path 0,sectionMessages)
     */
    public var reverseOrder:Bool = false
    
    open var room:MXRoom
    open var timeline:MXEventTimeline?
    open var timelineListener:Any?
    open var backPagingDisabled:Bool = false
    
    /**
     The bubble data that is handled by the data source.
     */
    open var roomBubbleData:[RoomBubbleData] = []
    
    /**
     Map between events and bubble data.
     */
    open var eventRoomBubbleDataMap:[MXEvent:RoomBubbleData] = [:]
    
    /**
     While paging, stores the incoming bubbles(events)
     */
    open var roomBubbleDataPaging:[RoomBubbleData] = []
    open var incomingEventFormatter:MXKEventFormatter?
    open var outgoingEventFormatter:MXKEventFormatter?
     
    /**
            Timestamp of last message we consider "read" (or 0 if unknown at the moment)
     */
    open var tsLastRead: UInt64 = 0
    
    // Notification center listener objects
    open var observers:[NSObjectProtocol] = [] //TODO - listen to more events?
    private var reactionListener: Any?
    
    public init(room:MXRoom) {
        self.room = room
        super.init()
        
        observers.append(NotificationCenter.default.addObserver(forName: Notification.Name.mxEventDidChangeSentState, object: nil, queue: OperationQueue.main) { [weak self] (notification) in
            if let self = self, let event = notification.object as? MXEvent,
                event.roomId == room.roomId {
                DispatchQueue.main.async {
                    if let bubbleData = self.eventRoomBubbleDataMap[event],
                        let indexPath = self.indexPath(for: bubbleData),
                        let delegate = self.delegate {
                        
                        // Just failed to decrypt?!?
                        if event.isEncrypted, event.decryptionError != nil {
                            // Remove the bubble, we don't display these.
                            let _ = self.deleteBubble(bubble: bubbleData)
                            return
                        }
                        
                        delegate.didChange(deleted: nil, added: nil, changed: [indexPath], live: false, disableScroll: false)
                    }
                    
                    // Did the event just fail to send?
                    if event.isFailedWithUnknownDevices() {
                        self.didFailWithUnknownDevices(event)
                    }
                }
            }
        })
        
        observers.append(NotificationCenter.default.addObserver(forName: Notification.Name.mxEventDidDecrypt, object: nil, queue: OperationQueue.main) { (notification) in
            if let event = notification.object as? MXEvent,
                event.roomId == room.roomId {
                DispatchQueue.main.async {
                    if let bubbleData = self.eventRoomBubbleDataMap[event],
                        let indexPath = self.indexPath(for: bubbleData),
                        let delegate = self.delegate {
                        delegate.didChange(deleted: nil, added: nil, changed: [indexPath], live: false, disableScroll: false)
                        if let linkedBubbles = bubbleData.linkedBubbles {
                            bubbleData.linkedBubbles = nil
                            linkedBubbles.forEach { _ = self.deleteBubble(bubble: $0) }
                        }
                    }
                }
            }
        })
        
        // Listen to "sessionDidSync" events. We use this to check for unknown devices present in the room, if it is encypted.
        observers.append(NotificationCenter.default.addObserver(forName: Notification.Name.mxSessionDidSync, object: nil, queue: OperationQueue.main, using: { (notification) in
            if let session = room.mxSession,
                let notificationSession = notification.object as? MXSession,
                session == notificationSession {
                // Any unknown devices?
                self.checkForUnknownDevices()
            }
        }))
        
        if room.summary.membership == .join {
            self.room.liveTimeline({ (timeline) in
                self.timeline = timeline
                self.timelineListener = self.timeline?.listenToEvents(nil, { [weak self] (_ event: MXEvent, _ direction: MXTimelineDirection, _ roomState: MXRoomState) in
                    guard let self = self else { return }
                    self.onEvent(event, direction, roomState)
                })
                
                // Add unsent messages
                for event in room.outgoingMessages() {
                    if event.sentState == MXEventSentStateSent {
                        room.removeOutgoingMessage(event.eventId)
                    } else if event.sentState == MXEventSentStateFailed {
                        // Insert last, we'll do a resend below
                        self.onEvent(event, .forwards, room.dangerousSyncState)
                    } else if let ts = event.userData?["echoAddedTs"] as? Int64 {
                        self.onEvent(event, .forwards, room.dangerousSyncState)

                        let then = Date(timeIntervalSince1970: TimeInterval(integerLiteral: ts))
                        let diff = abs(then.timeIntervalSinceNow)

                        // If older than one hour, cancel and retry
                        if diff > RoomDataSource.kSendTimeout {
                            DispatchQueue.main.async {
                                self.cancelMessage(event: event)
                                self.resendMessage(event: event)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.resendAllFailedMessages()
                }
                
                // Do initial backwards pagination
                if let timeline = self.timeline {
                    timeline.resetPagination()
                    self.doInitialPagination()
                }
                
                self.checkForUnknownDevices()
            })
        }
        self.createMessageFormatters()
        
        // Listen to replies/reactions!
        reactionListener =  room.mxSession.aggregations.listenToReactionCountUpdate(inRoom: room.roomId) { [weak self] (changes:[String : MXReactionCountChange]) in
            guard let self = self else {return}
            for change in changes {
                DispatchQueue.main.async { [weak self] in
                    if let self = self,
                        let originalEntry = self.eventRoomBubbleDataMap.first(where: { (event, bubble) -> Bool in
                            event.eventId == change.key
                        }),
                        let indexPath = self.indexPath(for: originalEntry.value) {
                        self.delegate?.didChange(deleted: nil, added: nil, changed: [indexPath], live: false, disableScroll: false)
                    }
                }
            }
        }
    }
    deinit {
        invalidate()
    }

    open func createMessageFormatters() {
        self.incomingEventFormatter = MXKEventFormatter(matrixSession: room.mxSession)
        self.outgoingEventFormatter = MXKEventFormatter(matrixSession: room.mxSession)
    }

    /**
     Read in the last N events
     */
    open func doInitialPagination() {
        if let lastRead = room.accountData.readMarkerEventId {
            delegate?.pagingStarted(direction: .backwards)
            self.pageBackwardsToLastRead(lastReadEventId: lastRead)
        } else {
            self.page(direction: .backwards, onlyFromStore: true)
        }
    }
    
    /**
        Page backwards until we find the event we have last read.
     */
    func pageBackwardsToLastRead(lastReadEventId: String) {
        if let timeline = self.timeline, timeline.canPaginate(.backwards) {
            pagingDirection = .backwards
            let completion = { (response:MXResponse<Void>) in
                DispatchQueue.main.async {
                    let hasFoundLastRead = self.roomBubbleDataPaging.contains { (bubble) -> Bool in
                        return bubble.events.contains { (event) -> Bool in
                            if event.eventId == lastReadEventId {
                                print("Scroll: found last event")
                                print(event)
                            }
                            return event.eventId == lastReadEventId
                        }
                    }
                    if response.isSuccess,
                        timeline.canPaginate(.backwards),
                        !hasFoundLastRead,
                        !self.backPagingDisabled
                    {
                        DispatchQueue.global().async {
                            self.pageBackwardsToLastRead(lastReadEventId: lastReadEventId)
                        }
                    } else {
                        // Done
                        self.pagingDone(.backwards, success: true, scrollToEventId: lastReadEventId)
                        self.updateReadReceipts(eventIds: nil)
                    }
                }
            }
            DispatchQueue.global().async {
                timeline.paginate(self.pagingSize, direction: .backwards, onlyFromStore: false, completion: completion)
            }
        }
    }
    
    /**
     Invalidate the data source. This will stop listeners and observers.
     */
    open func invalidate() {
        delegate = nil
        if let timeline = self.timeline, let timelineListener = self.timelineListener {
            timeline.removeListener(timelineListener)
        }
        self.timelineListener = nil
        self.timeline = nil
        if let reactionListener = reactionListener {
            room.mxSession.aggregations.removeListener(reactionListener)
            self.reactionListener = nil
        }
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
        roomBubbleData.removeAll()
        eventRoomBubbleDataMap.removeAll()
    }
    
    open func onEvent(_ event: MXEvent, _ direction: MXTimelineDirection, _ roomState: MXRoomState) {
        guard direction == .forwards || isPaging else {return}
        //print(event)
        if let type = event.type, type == Notification.Name.mxEventTypeStringTyping.rawValue {
            delegate?.onTypingUsersChange(room)
            return
        }
        
        if let type = event.type, type == kMXEventTypeStringReceipt, let ids = event.readReceiptEventIds() as? [String] {
            updateReadReceipts(eventIds: ids)
            return
        }
        
        if event.isEncrypedAndSentBeforeWeJoined() {
            // Ignore this one and disable backwards paging, we'll get nothing useful.
            backPagingDisabled = true
            return
        }
        
        if event.isRedactedEvent() {
            return
        }
        
        if let type = event.type, type == kMXEventTypeStringRoomRedaction {
            if let redacts = event.redacts, direction == .forwards {
                // This is a room redaction event. It redacts an event sent to the room earlier. If we already have that in our bubble map, we need to find it and mark the original event as redacted, so it is hidden.
                DispatchQueue.main.async {
                    if let redactedEntry = self.eventRoomBubbleDataMap.first(where: { (event, bubble) -> Bool in
                        event.eventId == redacts
                    }) {
                        // We found the original event. Next step is to replace
                        // the original event with a "pruned" one and then tell
                        // the delegate that the cell is updated.
                        let redactedEvent = redactedEntry.key
                        let bubble = redactedEntry.value
                        if !redactedEvent.isRedactedEvent(),
                            let indexPath = self.indexPath(for: bubble),
                            let idx = bubble.events.firstIndex(of: redactedEvent),
                            let cleanEvent = redactedEvent.prune()
                        {
                            cleanEvent.redactedBecause = event.jsonDictionary()
                            
                            // Not already redacted, do that now.
                            bubble.events.remove(at: idx)
                            bubble.events.insert(cleanEvent, at: idx)
                            self.delegate?.didChange(deleted: nil, added: nil, changed: [indexPath], live: false, disableScroll: true)
                        }
                    }
                }
            }
            return
        }
        
        var data = RoomBubbleData(self, state: roomState, events: [event], type:nil)
        
        // If we have a bubble processor installed, call that.
        if let bubbleProcessor = RoomDataSource.bubbleProcessor {
            data = bubbleProcessor(self, event, data, roomState)
        }

        if let data = data {
            
            var deletedIndexPaths:[IndexPath] = []
            var deletedBubbles:[RoomBubbleData] = []
            
            // If there's a local echo for this event, remove that
            if !event.isLocalEvent() {
                if let localEchoEvent = self.room.pendingLocalEchoRelated(to: event), let bubble = self.eventRoomBubbleDataMap[localEchoEvent] {
                    // Important - Copy over user data! So for echo when resending for the "devicesAdded", when we get here "event" is populated correctly, and the "devices added" bubble will be create below (if needed)
                    event.userData = localEchoEvent.userData
                    bubble.events.removeAll {$0 == localEchoEvent}
                    if bubble.events.count == 0 {
                        // Add this on to be deleted - NOTE: Delete inside the Main.async branch to keep table view consistency!
                        deletedBubbles.append(bubble)
                    }
                }
            }
            
            
            var bubbles:[RoomBubbleData] = [data]
            
            if let devicesAdded = event.userData?["devicesAdded"] as? [String:[String]] {
                // At the time this message was sent, the user had unknown devices. Checked if they still are.
                var devicesAddedFiltered:[String:[String]] = [:]
                if let session = room.mxSession {
                    for userId in devicesAdded.keys {
                        guard let userAddedDevices = devicesAdded[userId], userAddedDevices.count > 0 else {continue}
                        if let userStoredDevices = session.crypto.deviceList.storedDevices(forUser: userId) {
                            var stillUnknownDevices:[String] = []
                            for deviceId in userAddedDevices {
                                if !userStoredDevices.contains(
                                    where: { $0.deviceId == deviceId && $0.trustLevel?.localVerificationStatus != .unknown && $0.trustLevel?.localVerificationStatus != .unverified })
                                {
                                    stillUnknownDevices.append(deviceId)
                                }
                            }
                            if stillUnknownDevices.count > 0 {
                                devicesAddedFiltered[userId] = stillUnknownDevices
                            }
                        } else {
                            devicesAddedFiltered[userId] = userAddedDevices
                        }
                    }
                } else {
                    devicesAddedFiltered = devicesAdded
                }
                
                devicesAddedFiltered = devicesAddedFiltered.filter { (userId, devices) -> Bool in
                    if let member = roomState.members.member(withUserId: userId) {
                        return member.membership == .join
                    }
                    return true
                }
                
                if devicesAddedFiltered.count > 0,
                    let devicesAddedBubbleData = RoomBubbleData(self, state: roomState, events: [], type: RoomBubbleDataType.devicesAdded) {
                    devicesAddedBubbleData.userData = ["devicesAdded": devicesAddedFiltered]
                    bubbles.append(devicesAddedBubbleData)
                    data.linkedBubbles = [devicesAddedBubbleData]
                }
            }

            DispatchQueue.main.async {
                // Don't add dupes
                for bubble in bubbles {
                    bubble.events.removeAll { (event) -> Bool in
                        return self.eventRoomBubbleDataMap[event] != nil
                    }
                }
                
                // Delete the bubbles and gather the index paths
                for bubble in deletedBubbles {
                    deletedIndexPaths.appendOptional(contentsOf: self.deleteBubble(bubble: bubble, callDelegate: false))
                }
                
                var newIndexPaths:[IndexPath] = []
                var changedIndexPaths:[IndexPath] = []
                
                if self.pagingDirection != nil {
                    self.addBubbles(bubbleData: bubbles, toArray: &self.roomBubbleDataPaging, direction: direction, newIndexPaths: &newIndexPaths, changedIndexPaths: &changedIndexPaths)
                    if let delegate = self.delegate, deletedIndexPaths.count > 0 {
                        delegate.didChange(deleted: deletedIndexPaths, added: nil,changed: nil, live: false, disableScroll: false)
                    }
                } else {
                    self.addBubbles(bubbleData: bubbles, toArray: &self.roomBubbleData , direction: direction, newIndexPaths: &newIndexPaths, changedIndexPaths: &changedIndexPaths)
                    if let delegate = self.delegate {
                        let isLive:Bool = (direction == .forwards)
                        delegate.didChange(deleted: deletedIndexPaths, added: newIndexPaths, changed: changedIndexPaths, live: isLive, disableScroll: false)
                    }
                }
            }
        }
    }
    
    private func addBubbles(bubbleData:[RoomBubbleData], toArray array:inout [RoomBubbleData], direction: MXTimelineDirection, newIndexPaths:inout [IndexPath], changedIndexPaths:inout [IndexPath]) {
        
        var bubbles = bubbleData
        
        if bubbles.count > 0 {
            var nInserted = bubbles.count
            if direction == .backwards {
                if let last = bubbles.last {
                    bubbles.removeLast()
                    
                    let oldEventCount = array.first?.events.count
                    if !coalesceBubbles(existing: array.first, new: last, direction: direction) {
                        array.insert(last, at: 0)
                    } else {
                        // Nothing inserted = the bubble data was coalesced!
                        nInserted -= 1
                        
                        // Mark that index path (NOT where it will end up, please see tableView.reloadRows, the changed index paths are considered to be as they were before the update) as "changed"
                        if let oldEventCount = oldEventCount, oldEventCount != array.first?.events.count {
                            changedIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1) : 0, section: sectionMessages))
                        }
                    }
                }
                array.insert(contentsOf: bubbles, at: 0)
                for i in 0..<nInserted {
                    newIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1 - i) : i, section: sectionMessages))
                }
            } else {
                if let first = bubbles.first {
                    bubbles.removeFirst()
                    
                    let oldEventCount = array.last?.events.count
                    if !coalesceBubbles(existing: array.last, new: first, direction: direction) {
                        array.append(first)
                    } else {
                        // Nothing inserted = the bubble data was coalesced!
                        nInserted -= 1

                        // If the number of events in coalesced bubble has changed (it may not have in subclasses that could filter events) mark the cell as changed.
                        if let oldEventCount = oldEventCount, oldEventCount != array.last?.events.count {
                            changedIndexPaths.append(IndexPath(row: reverseOrder ? 0 : (array.count - 1), section: sectionMessages))
                        }
                    }
                }
                array.append(contentsOf: bubbles)
                for i in (array.count-nInserted)..<array.count {
                    newIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1 - i) : i, section: sectionMessages))
                }
            }
        }
    }
    
    /**
     Store the local echo event in the room. Also, stamp it with the current time, so we can later detect if we find events that seem "stuck" in sending.
     */
    public static func storeLocalEcho(room:MXRoom, localEcho: MXEvent?) {
        guard let event = localEcho else {return}
        if event.userData == nil {
            event.userData = [:]
        }
        event.userData?["echoAddedTs"] = Int64(Date().timeIntervalSince1970)
        room.storeOutgoingMessage(event)
    }
    
    public func insertLocalEcho(event:MXEvent?, roomState:MXRoomState) {
        guard let event = event else {return}
        RoomDataSource.storeLocalEcho(room: room, localEcho: event)
        onEvent(event, .forwards, roomState)
    }
    
    public var isPaging:Bool {
        get {
            return pagingDirection != nil
        }
    }
    
    //MARK: UITableViewDataSource
    /**
     We have three sections. In the first, optionally the "paging" header. In section 1 we have the actual data.
     */
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionHeader {
            return isPaging ? 1 : 0
        }
        return roomBubbleData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case sectionHeader:
            if let delegate = self.delegate, let cell = delegate.createCell(type:.paging, at: indexPath) {
                return cell
            }
        case sectionMessages:
            if let delegate = self.delegate, let bubble = roomBubbleData(at: indexPath), let cell = delegate.createCell(at: indexPath, for: bubble) {
                return cell
            }
        default: break
        }
        return UITableViewCell() // Fallback to avoid crash
    }
    
    //MARK: UITableViewDelegate
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let bubble = roomBubbleData(at: indexPath), bubble.isIncoming else { return }
        acknowledgeBubbleEvents(bubble: bubble)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.didScroll(offset: scrollView.contentOffset.y)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    //MARK: UICollectionViewDataSource
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == sectionHeader {
            return 0
        }
        return roomBubbleData.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let delegate = self.delegate, let bubble = roomBubbleData(at: indexPath), let cell = delegate.createCollectionViewCell(at: indexPath, for: bubble) {
            acknowledgeBubbleEvents(bubble: bubble)
            return cell
        }
        return UICollectionViewCell() // Fallback to avoid crash
    }
    
    //MARK: Bubble methods
    open func roomBubbleData(at indexPath: IndexPath) -> RoomBubbleData? {
        if indexPath.section == sectionMessages, indexPath.row >= 0, indexPath.row < roomBubbleData.count {
            return roomBubbleData[reverseOrder ? (roomBubbleData.count - 1 - indexPath.row) : indexPath.row]
        }
        return nil
    }
    
    public func roomBubbleData(before bubble: RoomBubbleData) -> RoomBubbleData? {
        if let index = roomBubbleData.firstIndex(of: bubble), index > 0 {
            return roomBubbleData[index - 1]
        }
        return nil
    }
    
    open func indexPath(for bubble: RoomBubbleData) -> IndexPath? {
        if let index = roomBubbleData.firstIndex(of: bubble) {
            return IndexPath(row: reverseOrder ? (roomBubbleData.count - 1 - index) : index, section: sectionMessages)
        }
        return nil
    }
    
    public var canPageBackwards:Bool {
        get {
            return !backPagingDisabled && (timeline?.canPaginate(.backwards) ?? false)
        }
    }
    
    public var canPageForwards:Bool {
        get {
            return timeline?.canPaginate(.forwards) ?? false
        }
    }
    
    private func page(direction:MXTimelineDirection, onlyFromStore:Bool) {
        
        if !self.isPaging, let timeline = self.timeline, timeline.canPaginate(direction) {
            pagingDirection = direction
            delegate?.pagingStarted(direction: direction)
            let completion = { (response:MXResponse<Void>) in
                DispatchQueue.main.async {
                    self.pagingDone(direction, success: response.isSuccess, scrollToEventId: nil)
                    self.updateReadReceipts(eventIds: nil)
                }
            }
            DispatchQueue.global().async {
                timeline.paginate(self.pagingSize, direction: direction, onlyFromStore: onlyFromStore, completion: completion)
            }
        }
    }
    
    /**
     Try to coalesce an existing cell with new bubble data. For some cells, like when room members are joining/leaving, we coalesce multiple MX events into the same Table view UI cell, displaying it like "x members joined, y members left". This function checked if we can join the two bubbles together by appending/prepending the events of one into the other.
     
     This function will also update the mapping between "MXEvent" objects and the "RoomBubbleData" objects displaying them.
     
     - Parameter existing: The bubble data for an existing cell (or nil, in which case the function returns nil).
     - Parameter new: The new bubble data.
     - Parameter direction: The direction, determines if we should append or prepend events when coalescing.
     - Returns: true if the two bubbles where joined, false otherwise.
     - Warning: This func has the side effect of updating the event mapping from event -> RoomBubbleData!
     */
    open func coalesceBubbles(existing:RoomBubbleData?, new:RoomBubbleData, direction:MXTimelineDirection) -> Bool {
        if let existing = existing, existing.bubbleType == .newMember, new.bubbleType == .newMember {
            // Coalesce the events!
            if direction == .backwards {
                existing.events.insert(contentsOf: new.events, at: 0)
            } else {
                existing.events.append(contentsOf: new.events)
            }
            new.events.forEach { self.eventRoomBubbleDataMap[$0] = existing }
            return true
        }
        new.events.forEach { self.eventRoomBubbleDataMap[$0] = new }
        return false
    }
    
    open func pagingDone(_ direction:MXTimelineDirection, success: Bool, scrollToEventId: String?) {
        var newIndexPaths:[IndexPath] = []
        var changedIndexPaths:[IndexPath] = []
        if roomBubbleDataPaging.count > 0 {
            if direction == .backwards,
                roomBubbleData.count > 0 {
                changedIndexPaths.append(IndexPath(row: reverseOrder ? (roomBubbleData.count - 1) : 0, section: sectionMessages))
            }
            addBubbles(bubbleData: roomBubbleDataPaging, toArray: &roomBubbleData, direction: direction, newIndexPaths: &newIndexPaths, changedIndexPaths: &changedIndexPaths)
        }
        roomBubbleDataPaging = []
        pagingDirection = nil
        if let delegate = self.delegate {
            var scrollToIndexPath: IndexPath? = nil
            if
                let scrollToEventId = scrollToEventId,
                let scrollToEventEntry = self.eventRoomBubbleDataMap.first(where: { (event, bubble) -> Bool in
                    event.eventId == scrollToEventId
                })
            {
                scrollToIndexPath = self.indexPath(for: scrollToEventEntry.value)
                if var scrollTo = scrollToIndexPath {
                    // We have an index path for the "last read" event. Now, this is only for incoming events, so make sure to "walk up" from this event to all the ones we might have sent ourselves after this point in time.
                    scrollTo = IndexPath(row: scrollTo.row, section: scrollTo.section)
                    while
                        let nextBubble = roomBubbleData(at: IndexPath(row: scrollTo.row + 1, section: scrollTo.section)),
                        !nextBubble.isIncoming {
                        scrollTo.row += 1
                    }
                    scrollToIndexPath = scrollTo
                }
            }
            
            delegate.pagingDone(direction: direction, newIndexPaths:newIndexPaths, changedIndexPaths: changedIndexPaths, success: success, scrollToIndexPath: scrollToIndexPath)
        }
    }
    
    public func pageBackwards() {
        page(direction: .backwards, onlyFromStore: false)
    }
    
    public func pageForwards() {
        page(direction: .forwards, onlyFromStore: false)
    }
    
    /**
     Delete an outgoing event. This means remove it from its containing bubble (removing the bubble altogether if it is left empty) and calling room.
     */
    func deleteOutgoingEvent(event:MXEvent) {
        guard let bubble = eventRoomBubbleDataMap[event] else { return }
        eventRoomBubbleDataMap.removeValue(forKey: event)
        bubble.events.removeAll { $0.eventId == event.eventId }
        room.removeOutgoingMessage(event.eventId)
        if bubble.events.count == 0 {
            let _ = deleteBubble(bubble: bubble)
        }
    }
    
    func deleteBubble(bubble:RoomBubbleData, callDelegate: Bool = true) -> [IndexPath]? {
        
        var removedIndexPaths:[IndexPath] = []
        
        var bubblesToDelete = [bubble]
        if let linkedBubbles = bubble.linkedBubbles {
            //TODO - currenly only 1 level, should this be recursive instead allowing arbitrary depths?
            bubblesToDelete.append(contentsOf: linkedBubbles)
        }
        
        // First get all affected IndexPaths
        bubblesToDelete.forEach { (bubble) in
            if let indexPath = indexPath(for: bubble) {
                removedIndexPaths.append(indexPath)
            }
        }
        
        // Then delete the bubbles from the array
        bubblesToDelete.forEach { (bubble) in
            bubble.events.forEach { eventRoomBubbleDataMap.removeValue(forKey: $0) }
            if let index = roomBubbleData.firstIndex(of: bubble) {
                roomBubbleData.remove(at: index)
            }
        }
        
        if callDelegate {
            delegate?.didChange(deleted: removedIndexPaths, added: nil, changed: nil, live: false, disableScroll: false)
        }
        
        //TODO - Check if we can coalesce the bubble before and after this one
        
        if removedIndexPaths.count == 0 {
            return nil
        }
        return removedIndexPaths
    }
    
    /**
     Delete all bubbles of the given type.
     */
    func deleteAllBubbles(ofType type:RoomBubbleDataType) {
        roomBubbleData.filter { $0.bubbleType == type }.forEach { _ = deleteBubble(bubble: $0) }
    }
    
    /**
     Go through all failed messages, resending them.
     */
    func resendAllFailedMessages() {
        if let failedMessages = room.outgoingMessages()?.filter({$0.sentState == MXEventSentStateFailed}) {
            resendMessage(inArray: failedMessages, atIndex: 0)
        }
    }
    
    /**
     Resend a given message. If the message is a failed one, try to resend. Otherwise create a copy of the message and send that.
     */
    func resendMessage(event:MXEvent) {
        resendMessage(inArray: [event], atIndex: 0)
    }

    /**
     Redact a given message.
     */
    func redactMessage(event:MXEvent) {
        room.redactEvent(event.eventId, reason: nil) { (response) in
            // Ignored for now
        }
    }

    
    func cancelMessage(event:MXEvent) {
        if event.isLocalEvent() {
            room.cancelSendingOperation(event.eventId)
        }
        deleteOutgoingEvent(event: event)
    }
    
    /**
     Iterative function to send failed messages.
     */
    private func resendMessage(inArray array:[MXEvent], atIndex index:Int) {
        guard index < array.count else {return} //done
        let event = array[index]
        
        // If the event was failed, remove it! We'll add a new local echo
        if event.sentState == MXEventSentStateFailed {
            deleteOutgoingEvent(event: event)
        }
        
        var localEcho:MXEvent?
        
        room.sendMessage(withContent: event.content, localEcho: &localEcho) { (response) in
            self.resendMessage(inArray: array, atIndex: index + 1)
        }

        // Copy user data properties over to new event object
        localEcho?.userData = event.userData

        // Add the local echo!
        insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
    }
    
    // Called for events that failed to send because there are unknown devices present in the room. This function is called on the main thread. Default implementation sets the devices to "known" and resend the message.
    open func didFailWithUnknownDevices(_ event: MXEvent) {
        if let devices = event.sentFailedUnknownDevices,
            let session = room.mxSession {
            
            // Implement our "Warn Only if Devices added" (WOIDA) scheme.
            //
            // We start by getting the truly new devices, i.e. if this is the first time EVER that we see devices for a user, they are considered "known".
            var devicesAdded:[String:[String]] = [:]
            for userId in devices.userIds() {
                if session.crypto.deviceList.storedDevices(forUser: userId) != nil,
                    let userDevices = devices.deviceIds(forUser: userId) {
                    devicesAdded[userId] = userDevices
                }
            }
            
            // Unknown devices now contain "devices added"
            // Store in user property of event
            if event.userData == nil {
                event.userData = [:]
            }
            event.userData?["devicesAdded"] = devicesAdded
            session.crypto.setDevicesKnown(devices) {
                self.resendMessage(event: event)
            }
        }
    }
    
    /**
     Checks the device lists for all joined members if we have any devices in state "unknown".
     */
    func checkForUnknownDevices() {
        // This seems to cause dead locks somehow.
//        guard
//            room.summary.isEncrypted,
//            let session = room.mxSession else {return}
//
//        room.members { (response) in
//            if response.isSuccess, let members = response.value {
//                if let joined = members?.joinedMembers {
//                    let memberIds = joined.compactMap({ $0.userId })
//                    session.crypto.downloadKeys(memberIds, forceDownload: false, success: { (usersDevicesMap) in
//                        guard let usersDevicesMap = usersDevicesMap else { return }
//
//                        var unknownDeviceUsers = 0
//                        var unknownDevices = 0
//                        var unverifiedDevices = 0
//
//                        for userId in usersDevicesMap.userIds() ?? [] {
//                            guard userId != session.myUser.userId else { continue }
//                            let tempUnknown = unknownDevices
//                            for deviceId in usersDevicesMap.deviceIds(forUser: userId) ?? [] {
//                                if let device = usersDevicesMap.object(forDevice: deviceId, forUser: userId) {
//                                    if device.verified == MXDeviceUnverified {
//                                        unverifiedDevices += 1
//                                    } else if device.verified == MXDeviceUnknown {
//                                        unknownDevices += 1
//                                    }
//                                }
//                            }
//                            if tempUnknown != unknownDevices {
//                                // Changed, so add +1 for this user
//                                unknownDeviceUsers += 1
//                            }
//                        }
//                        self.delegate?.unknownDevicesUpdate(unknownDevices: unknownDevices, unknownDeviceUsers: unknownDeviceUsers)
//                    }, failure: { (error) in
//
//                    })
//                }
//            }
//        }
    }
    
    /**
     Handle read receipts. A "bubble" is considered to be "received" if it is not incoming and if any of the events it contains has received a receipt from ANY other user than the current.
     Since bubbles are storted in time, if we find that a certain bubble is "received", then we just assume all other bubbles before it are received, and mark them as such.
     - parameter eventIds: Optional array of event ids that are received. We set this if we get a m.receipt event.
     */
    open func updateReadReceipts(eventIds:[String]?) {
        DispatchQueue.main.async {
            guard let store = self.room.mxSession?.store else { return }
            
            // Get index of first bubble that is marked as not received. All others we can ignore.
            if let unreceivedBubble = self.roomBubbleData.first(where: { !$0.isIncoming && !$0.isReceived } ),
                let index = self.roomBubbleData.firstIndex(of: unreceivedBubble)
            {
                var indexOfLastInEventIds = 0
                var indexOfLastWithReceipt = 0

                // If called with event IDs, get the max index of the corresponding bubble(s), if any
                if let eventIds = eventIds {
                    for eventId in eventIds {
                        if let event = store.event(withEventId: eventId, inRoom: self.room.roomId),
                            let bubble = self.eventRoomBubbleDataMap[event],
                            !bubble.isIncoming,
                             let index = self.roomBubbleData.firstIndex(of: bubble) {
                            indexOfLastInEventIds = max(indexOfLastInEventIds, index)
                        }
                    }
                }
                
                // Walk backwards though the bubbles, trying to find receipts attached to the events.
                for idxBubble in (index..<self.roomBubbleData.count).reversed() {
                    let bubble = self.roomBubbleData[idxBubble]
                    //guard !bubble.isIncoming else { continue }
                    if bubble.events.contains(where: { (event) -> Bool in
                        return (self.room.getEventReceipts(event.eventId, sorted: false)?.count ?? 0) > 0
                    }) {
                        indexOfLastWithReceipt = idxBubble
                    }
                }
                
                var changedIndexPaths:[IndexPath] = []
                let indexOfLast = max(indexOfLastWithReceipt, indexOfLastInEventIds)
                if indexOfLast >= index {
                    for i in index ... indexOfLast {
                        let b = self.roomBubbleData[i]
                        if !b.isIncoming, !b.isReceived {
                            b.isReceived = true
                            changedIndexPaths.appendOptional(self.indexPath(for: b))
                        }
                    }
                }
                if changedIndexPaths.count > 0 {
                    self.delegate?.didChange(deleted: nil, added: nil, changed: changedIndexPaths, live: false, disableScroll: true)
                }
            }
        }
    }
    
    /**
     Called when we are actually displaying a bubble containing events. These events are now considered "read", so we acknowledge them (and also update the read marker if needed).
     */
    private func acknowledgeBubbleEvents(bubble: RoomBubbleData) {
        // Get TS of read marker (if found)
        if tsLastRead == 0 {
            if let readMarkerEventId = room.accountData.readMarkerEventId,
                let readMarkerEvent = room.mxSession.store.event(withEventId: readMarkerEventId, inRoom: room.roomId) ?? eventRoomBubbleDataMap.first(where: { (event, bubble) -> Bool in
                        return event.eventId == readMarkerEventId
                })?.key {
                tsLastRead = readMarkerEvent.originServerTs
            }
        }
        
        // Acknowledge the events
        for event in bubble.events {
            var updateReadMarker = false
            
            // If the event has a later TS than our previous RM, update that as well.
            //
            if tsLastRead > 0, event.originServerTs >= tsLastRead {
                tsLastRead = event.originServerTs
                updateReadMarker = true
            }
            room.acknowledgeEvent(event, andUpdateReadMarker: updateReadMarker)
        }
    }
}
