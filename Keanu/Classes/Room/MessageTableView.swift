//
//  MessageTableView.swift
//  Keanu
//
//  Created by N-Pex on 07.01.20.
//

/**
 A UITableView that handles dynamic updates via the "didChange" callback.
 */
import UIKit

open class MessageTableView: UITableView {
    
    public var latestAtTop:Bool = false {
        didSet(value) {
            // If set to reverse order, make sure to initialize these differently
            viewingFirstMessage = !value
            viewingLastMessage = value
        }
    }
    public var isUpdating:Bool = false
    var viewingFirstMessage: Bool = true
    var viewingLastMessage: Bool = false
    
    private func updateTableView(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool) {
        UITableView.setAnimationsEnabled(false)
        beginUpdates()
        if let deleted = deleted, deleted.count > 0 {
            self.deleteRows(at: deleted, with: .none)
        }
        if let changed = changed, changed.count > 0 {
            self.reloadRows(at: changed, with: .none)
        }
        if let added = added, added.count > 0 {
            self.insertRows(at: added, with: (live ? .bottom : .none))
        }
        endUpdates()
        UITableView.setAnimationsEnabled(true)
    }
    
    open func didChange(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool) {
        
        let scrollToTop = viewingFirstMessage || !viewportIsFilled
        let scrollToBottom = viewingLastMessage || !viewportIsFilled
        
        isUpdating = true
        showsVerticalScrollIndicator = false
        let oldScrollEnabled = isScrollEnabled
        isScrollEnabled = false
        
        let oldOffset = contentOffset
        
        var deletedCellsHeight: CGFloat = 0
        var addedCellsHeight: CGFloat = 0
        var changedCellsOldHeight: CGFloat = 0
        var changedCellsNewHeight: CGFloat = 0
        
        deleted?.forEach {
            deletedCellsHeight += rectForRow(at: $0).height
        }
        // Store the old heights for the cells
        changed?.forEach {
            changedCellsOldHeight += rectForRow(at: $0).height
        }
        
        if let firstDeleted = deleted?.first,
            let firstAdded = added?.first,
            firstDeleted == firstAdded,
            (added?.count ?? 0) == 1,
            (deleted?.count ?? 0) == 1,
            (changed?.count ?? 0) == 0 {
            // Actually a change, not a remove/add.
            updateTableView(deleted: nil, added: nil, changed: [firstAdded], live: live)
        } else {
            updateTableView(deleted: deleted, added: added, changed: changed, live: live)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
        if live == latestAtTop {
            changed?.forEach {
                let offset = added?.count ?? 0
                let newIndexPath = IndexPath(row: $0.row + offset, section: $0.section)
                changedCellsNewHeight += rectForRow(at: newIndexPath).height
            }
            
            added?.forEach {
                addedCellsHeight += rectForRow(at: $0).height
            }
            
            var delta: CGFloat = 0
            delta = addedCellsHeight - deletedCellsHeight + changedCellsNewHeight - changedCellsOldHeight
            let newOffset = CGPoint(x: oldOffset.x, y: oldOffset.y + delta)
            if !disableScroll {
                setContentOffset(newOffset, animated: false)
            }
        }
        
        showsVerticalScrollIndicator = true
        isScrollEnabled = oldScrollEnabled
        isUpdating = false
        
        if !disableScroll {
            if latestAtTop, scrollToTop {
                if let first = firstIndexPath {
                    scrollToRow(at: first, at: .bottom, animated: live)
                }
            } else if !latestAtTop, scrollToBottom {
                if !viewportIsFilled {
                    setContentOffset(CGPoint.zero, animated: false)
                } else if let last = lastIndexPath {
                    DispatchQueue.main.async {
                        self.scrollToRow(at: last, at: .top, animated: live)
                    }
                }
            }
        }
    }

    public func didScroll() {
        if !isUpdating {
            let scrolledToTop = contentOffset.y == 0
            let scrolledToBottom = contentOffset.y >= (contentSize.height - frame.size.height)
            viewingFirstMessage = scrolledToTop
            viewingLastMessage = scrolledToBottom
        }
    }
}
