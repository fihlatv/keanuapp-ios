//
//  String+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 28.05.20.
//

import Foundation

extension String {

    /**
     Material icon.
     */
    public static var exit_to_app = "\u{e879}"

    /**
     Material icon.
     */
    public static var flash_on = "\u{e3e7}"

    /**
     Material icon.
     */
    public static var flash_off = "\u{e3e6}"

    /**
     Material icon.
     */
    public static var group_add = "\u{e7f0}"

    /**
     Material icon.
     */
    public static var language = "\u{e894}"

    /**
     Material icon.
     */
    public static var lock = "\u{e897}"

    /**
     Material icon.
     */
    public static var notifications_active = "\u{e7f7}"

    /**
     Material icon.
     */
    public static var person_add = "\u{e7fe}"

    /**
     Material icon.
     */
    public static var photo = "\u{e410}"

    /**
     Material icon.
     */
    public static var play_arrow = "\u{e037}"

    /**
     Material icon.
     */
    public static var playlist_play = "\u{e05f}"

    /**
     Material icon.
     */
    public static var storage = "\u{e1db}"

    /**
     Material icon.
     */
    public static var switch_camera = "\u{e41e}"

    /**
     Material icon.
     */
    public static var verified_user = "\u{e8e8}"

    /**
     Material icon.
     */
    public static var warning = "\u{e002}"

}
