//
//  UIFont+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 28.05.20.
//

import UIKit

extension UIFont {

    /**
     Returns the MaterialIcons font object in the specified size.

     - parameter fontSize: The size (in points) to which the font is scaled. This value must be greater than 0.0. Defaults to 24.0.
     - returns: The MaterialIcons font object of the specified size or the system font if it cannot be found.
     */
    open class func materialIcons(ofSize fontSize: CGFloat = 24) -> UIFont {
        return UIFont(name: "MaterialIcons-Regular", size: fontSize)
            ?? UIFont.systemFont(ofSize: fontSize)
    }
}
