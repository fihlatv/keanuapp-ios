//
//  MXKAccount+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 30.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

extension MXKAccount {

    /**
     Shortcut for `mxCredentials?.userId` with fallback to empty string.
     */
    public var matrixId: String {
        get {
            return mxCredentials?.userId ?? ""
        }
    }

    /**
     The `userDisplayName` if not empty or the local part of the `matrixId`.
     */
    public var friendlyName: String {
        get {
            return Friend.getFriendlyName(userDisplayName, matrixId)
        }
    }
    
    //MARK: Syntactic sugar - performOnSessionReady

    public typealias SessionReadyCallback = (MXSession) -> Void

    /**
     Convenience wrapper to execute a block when session reaches "ready" state.

     If session is not in that state, an observer is added waiting for it to reach the ready state.
     In the meantime the callback and observer is stored in an associated object.

     - parameter callback: The closure to call, when the session reaches state RUNNING.
     */
    public func performOnSessionReady(_ callback: @escaping SessionReadyCallback) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            DispatchQueue.main.sync {
                guard let self = self else {
                    return
                }

                self.sessionReadyCallbacks.append(callback)

                if self.mxSession?.state == MXSessionStateRunning {
                    self.sessionReadyWorker()
                    return
                }

                if self.sessionReadyObserver == nil {
                    self.sessionReadyObserver = NotificationCenter.default.addObserver(
                        forName: .mxSessionStateDidChange, object: nil, queue: .main,
                        using: self.sessionReadyWorker)
                }
            }
        }
    }

    /**
     The `sessionReadyCallbacks` queue worker.

     - parameter notification: Optional (unused) `Notification` object to be directly used as a `NotificationCenter` observer.
     */
    private func sessionReadyWorker(_ notification: Notification? = nil) {
        guard self.mxSession?.state == MXSessionStateRunning else {
            return
        }

        // Call all closures until list is empty.
        while self.sessionReadyCallbacks.count > 0 {
            self.sessionReadyCallbacks.removeFirst()(self.mxSession)
        }

        // Finally, remove ourselves.
        if let observer = self.sessionReadyObserver {
            NotificationCenter.default.removeObserver(observer)
            self.sessionReadyObserver = nil
        }
    }
    
    private struct AssociatedKeys {
        static var sessionReadyObserver = "sessionReadyObserver"
        static var sessionReadyCallbacks = "sessionReadyCallbacks"
    }
    
    /**
     Associated property to store information about state observer/callback, see performOnSessionReady.
     */
    private var sessionReadyObserver: NSObjectProtocol? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.sessionReadyObserver) as? NSObjectProtocol
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.sessionReadyObserver, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    private var sessionReadyCallbacks: [SessionReadyCallback] {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.sessionReadyCallbacks) as? [SessionReadyCallback] ?? []
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.sessionReadyCallbacks, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
