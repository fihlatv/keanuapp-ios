//
//  FriendsManager.swift
//  Keanu
//
//  Created by Benjamin Erhart on 29.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 Central manager to handle all friend related stuff:
 
 - Convert between different objects representing a friend or parts thereof.
 - Keep a persisted list of friends, even, when the user doesn't share a room
   with the friend (anymore).
 - Automatically a-/detach all available sessions.
 -
 */
public class FriendsManager {
    
    /**
     Singleton instance. Will load the friends list from disk on first access.
    */
    public static let shared: FriendsManager = {
        return FriendsManager().load()
    }()
    
    public static let changedNotification = Notification.Name("FriendsManagerChanged")

    /**
     A set of friends.

     Will not contain duplicates, due to set nature.

     Should not contain the user him-/herself.
    */
    public private(set) var friends = Set<Friend>() {
        didSet {
            sort()
        }
    }

    /**
     An ascendingly sorted (by name) and grouped (by first letter of name) list
     of friends.

     Will not contain duplicates, as it's backed by a set.

     Should not contain the user him-/herself.
     */
    public private(set) var sortedFriends = [String : [Friend]]()

    /**
     A sorted index of the friends groups (sections).
    */
    public private(set) var sortedIndex = [String]()

    
    // MARK: Internal
    
    private var sessions = [MXSession]()
    
    private init() {
        let nc = NotificationCenter.default
        
        nc.addObserver(self,
                       selector: #selector(sessionStateChanged(_:)),
                       name: .mxSessionStateDidChange, object: nil)
        
        nc.addObserver(self,
                       selector: #selector(roomSyncedInitially(_:)),
                       name: .mxRoomInitialSync, object: nil)

        nc.addObserver(self,
                       selector: #selector(willLeaveRoom(_:)),
                       name: .mxSessionWillLeaveRoom, object: nil)

        nc.addObserver(self, selector: #selector(userChanged(_:)),
                       name: .mxkAccountUserInfoDidChange, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: Persistence
    
    private static let fileStore: URL = {
        var baseUrl: URL?
        var fm = FileManager.default

        if let appGroupId = Config.appGroupId {
            baseUrl = fm.containerURL(forSecurityApplicationGroupIdentifier: appGroupId)
        }

        return (baseUrl ?? fm.urls(for: .cachesDirectory, in: .userDomainMask)[0]
            .appendingPathComponent(String(describing: FriendsManager.self)))
    }()
    
    /**
     Try to load the friends list from disk in the background.
     
     The data is contained in a serialized object file.
     
     - returns: self for fluency.
     */
    private func load() -> FriendsManager {
//        print("[\(String(describing: type(of: self)))] fileStore=\(FriendsManager.fileStore)")

        friends.removeAll(keepingCapacity: true)

        // ATTENTION: Loading the data as Set<Friend> will actually multiply (!!)
        // friends, for an unkown reason.
        DispatchQueue.global(qos: .background).async {
            if let data = try? Data(contentsOf: FriendsManager.fileStore),
                let friends = NSKeyedUnarchiver.unarchiveObject(with: data) as? Set<Friend> {

                // Attach sessions to friends, if already available.
                for friend in friends {
                    for session in self.sessions {
                        if session.user(withUserId: friend.matrixId) != nil {
                            friend.session = session
                            break
                        }
                    }

                    // Override already loaded friends from 1:1 sessions, as
                    // our stored here may contain user-defined names and avatars.
                    self.friends.update(with: friend)
                }

                self.notifyChange()
            }
        }
        
        return self
    }
    
    /**
     Store the friends list to disk as a serialized object file.
     
     - parameter async: Optional, default false. Do asynchronously in a background thread, if true.
     */
    public func store(async: Bool = false) {
        if async {
            DispatchQueue.global(qos: .background).async {
                self.store()
            }
        }
        else {
            try? NSKeyedArchiver.archivedData(withRootObject: friends)
                .write(to: FriendsManager.fileStore, options: [.atomic])
        }
    }
    
    
    // MARK: Change Handling
    
    /**
     Callback for `mxSessionStateDidChange` notification.
     
     Handles changes with sessions: When a session is usable, attaches the
     session object to all available friends and updates the friends list from
     1:1 rooms.
     
     When a session becomes unusable, removes the session object from all
     available friends and persists the friends list in the background.
     
     - parameter notification: A `mxSessionStateDidChange` notification.
    */
    @objc func sessionStateChanged(_ notification: Notification) {
        if let session = notification.object as? MXSession {
            switch session.state {
            case MXSessionStateClosed, MXSessionStatePaused, MXSessionStateUnknownToken:
                sessions.removeAll() { $0 == session }
                
                for friend in friends {
                    if friend.session == session {
                        friend.session = nil
                    }
                }
                
            case MXSessionStateStoreDataReady, MXSessionStateRunning:
                if !sessions.contains(session) {
                    sessions.append(session)

                    _ = refresh()
                }
                
            default:
                break
            }
        }
    }
    
    /**
     Callback for `mxRoomInitialSync` notification.
     
     Handles newly created rooms: If a 1:1 room, adds the other user as a friend.
     
     - parameter notification: A `mxRoomInitialSync` notification.
     */
    @objc func roomSyncedInitially(_ notification: Notification) {
        if let room = notification.object as? MXRoom {

            room.members() { response in
                if response.isSuccess {
                    self.addOtherOfPrivateRoom(room, response.value??.members)
                }
            }
        }
    }
    
    /**
     Callback for `mxSessionWillLeaveRoom` notification.

     Handles leaving of rooms: If a 1:1 room was left, the friend who was in
     there is marked as having no private room anymore.

     This will make a difference when deleting that friend.

     - parameter notification: A `mxSessionWillLeaveRoom` notification.
     */
    @objc func willLeaveRoom(_ notification: Notification) {
        if let roomId = notification.userInfo?[kMXSessionNotificationRoomIdKey] as? String {
            for session in sessions {
                if let room = session.room(withRoomId: roomId) {
                    room.members() { response in

                        if response.isSuccess,
                            let members = response.value??.members,
                            members.count == 2 {

                            for member in members {
                                if let friend = self.get(member) {
                                    friend.privateRoom = nil
                                }
                            }
                        }
                    }

                    break
                }
            }
        }
    }

    /**
     Callback for `mxkAccountUserInfoDidChange` notification.

     - parameter notification: A `mxkAccountUserInfoDidChange` notification.
     */
    @objc func userChanged(_ notification: Notification) {
        if let matrixId = notification.object as? String,
            let friend = get(matrixId) {

            // Reset, will be fetched fresh from session on next access.
            friend.user = nil

            notifyChange()
        }
    }

    /**
     Grabs the list of 1:1 rooms of every available session and adds these users
     to the friends list.
     
     Attach appropriate `MXSession` objects to all `Friend` objects.
     
     Re-sort friends list by name.
     
     - returns: self for fluency.
    */
    func refresh() -> FriendsManager {
        for session in sessions {
            
            for room in session.rooms {
                room.members() { response in
                    if response.isSuccess {
                        self.addOtherOfPrivateRoom(room, response.value??.members)
                    }
                }
            }
        }

        return self
    }
    
    /**
     Considers other room members of 1:1 chat rooms as friends and adds these
     to the friends list.
     
     Will re-sort the list and send a notification, *only* if anything changed.
     
     Will *not* add the user itself to the friends list. By definition one is
     not the friend of oneself.
     
     - parameter room: The room.
     - parameter members: The room members to add to the friends list.
     */
    private func addOtherOfPrivateRoom(_ room: MXRoom, _ members: [MXRoomMember]?) {
        if let members = members,
            members.count == 2 {
            
            var changed = false
            
            for member in members {
                if let friend = self.get(member) {
                    // Attach room and session to deserialized friends.
                    friend.privateRoom = room
                    friend.session = room.mxSession
                }
                else {
                    // Don't add the user themselves to the friends list.
                    if member.userId != room.mxSession.myUser.userId {
                        self.friends.insert(Friend(member, privateRoom: room))
                        changed = true
                    }
                }
            }
            
            if changed {
                notifyChange()
            }
        }
    }
    
    /**
     Send a `changedNotification` to listeners.
    */
    private func notifyChange() {
        NotificationCenter.default.post(name: FriendsManager.changedNotification,
                                        object: self)
    }
    
    
    // MARK: Friend Handling

    /**
     Returns a list of friends available in a section identified by an index.

     The friends are grouped in sections by the first letter of their friendly
     name and are sorted by that, too.

     - returns: a list of sorted friends for a given section index.
    */
    public func get(_ section: Int) -> [Friend]? {
        return section < sortedIndex.count ? sortedFriends[sortedIndex[section]] : nil
    }

    /**
     Tries to finds a `Friend` in the stored friends list by section and row
     of the sorted and grouped friends list.

     - parameter section: The section index.
     - parameter row: The index in the section list.
     - returns: a stored `Friend` or `nil` if not found.
    */
    public func get(_ section: Int, _ row: Int) -> Friend? {
        if let friends = get(section) {
            return row < friends.count ? friends[row] : nil
        }

        return nil
    }

    /**
     Tries to finds a `Friend` in the stored friends list by section and row
     of the sorted and grouped friends list.

     - parameter indexPath: The section and row identifying the friend.
     - returns: a stored `Friend` or `nil` if not found.
    */
    public func get(_ indexPath: IndexPath) -> Friend? {
        return get(indexPath.section, indexPath.row)
    }
    
    /**
     Tries to finds a `Friend` in the stored friends list by a given Matrix ID.
     
     - parameter matrixId: The searched for Matrix ID.
     - returns: a stored `Friend` or `nil` if not found.
    */
    public func get(_ matrixId: String) -> Friend? {
        return friends.first() { $0.matrixId == matrixId }
    }
    
    /**
     Tries to finds a `Friend` in the stored friends list by a given `MXUser`.
     
     This is just a convenience method: The Matrix ID is the single important
     piece of information used.
     
     - parameter user: The searched for `MXUser`.
     - returns: a stored `Friend` or `nil` if not found.
     */
    public func get(_ user: MXUser) -> Friend? {
        if let matrixId = user.userId {
            return get(matrixId)
        }
        
        return nil
    }
    
    /**
     Tries to finds a `Friend` in the stored friends list by a given `MXRoomMember`.
     
     This is just a convenience method: The Matrix ID is the single important
     piece of information used.
     
     - parameter roomMember: The searched for `MXRoomMember`.
     - returns: a stored `Friend` or `nil` if not found.
     */
    public func get(_ roomMember: MXRoomMember) -> Friend? {
        if let matrixId = roomMember.userId {
            return get(matrixId)
        }
        
        return nil
    }
    
    /**
     Search a user in the friends list or create a new `Friend` object from
     the given information, if none found.

     **ATTENTION**: A newly created user **is not added to the friends list**!
     You have to do that yourself, if you need to, using `#add(:)`!

     - parameter user: The `MXUser` to get a `Friend` for.
     - parameter session: Optional. The session the `MXUser` belongs to.
     - returns: a stored `Friend` or newly created one, if no stored found.
    */
    public func getOrCreate(_ user: MXUser, _ session: MXSession? = nil) -> Friend {
        let friend = get(user) ?? Friend(user.userId, session)

        if friend.session == nil {
            friend.session = session
        }

        return friend
    }

    /**
     Search a user in the friends list or create a new `Friend` object from
     the given information, if none found.

     **ATTENTION**: A newly created user **is not added to the friends list**!
     You have to do that yourself, if you need to, using `#add(:)`!

     - parameter user: The `MXRoomMember` to get a `Friend` for.
     - parameter session: Optional. The session the `MXUser` belongs to.
     - returns: a stored `Friend` or newly created one, if no stored found.
     */
    public func getOrCreate(_ roomMember: MXRoomMember, _ session: MXSession? = nil) -> Friend {
        let friend = get(roomMember) ?? Friend(roomMember)

        if friend.session == nil {
            friend.session = session
        }

        return friend
    }
    
    /**
     Adds a friend to our list of friends, *if* not already in it *and* not the
     user themselves.
     
     Re-sorts and persists the list and notifies change listeners.

     Tries to find a matching `MXUser` in all available sessions and attaches
     that to the `Friend` object, if none attached, yet.
     
     - parameter friend: A newly created `Friend` object.
    */
    public func add(_ friend: Friend) {
        var session: MXSession?
        
        for s in sessions {
            // Don't add the user themselves to the friends list.
            if friend.matrixId == s.myUser.userId {
                return
            }
            
            // Maybe we have that `MXUser` cached from a public room already?
            if s.user(withUserId: friend.matrixId) != nil {
                session = s
            }
        }

        // Friend is already in list: update Friend object with newer info.
        if let friendFromList = get(friend.matrixId) {
            if friendFromList.session == nil {
                friendFromList.session = session
            }

            if friendFromList.privateRoom == nil {
                friendFromList.privateRoom = friend.privateRoom
            }
        }
        else {
            // Friend is not in the list: Insert newly provided Friend object.
            if friend.session == nil {
                friend.session = session
            }

            friends.insert(friend)

            // Persist right away in background.
            store(async: true)

            notifyChange()
        }
    }
    
    /**
     Adds a friend to our list of friends, *if* not already in it.
     
     Re-sorts the list and notifies change listeners.
     
     Tries to find a matching `MXUser` in all available sessions and attaches
     that to the `Friend` object, if none attached, yet.
     
     - parameter matrixId: The Matrix ID of the friend.
     - parameter name: The (friendly) name of the friend. Optional. Will fall
        back to a matching `MXUser` object, after a session containing that
        `MXUser` is attached, or the local part of the Matrix ID.
     - parameter avatarUrl: The avatar URL. Optional. Will fall
        back to a matching `MXUser` object, after a session containing that
        `MXUser` is attached.
    */
    public func add(_ matrixId: String, _ name: String? = nil, _ avatarUrl: String? = nil) {
        add(Friend(matrixId, name, avatarUrl))
    }
    
    /**
     Removes a friend from the friends list, *if* in it.
     
     Re-sorts and persists the list and notifies change listeners.

     ATTENTION: This doesn't care, if the user still has a 1:1 room with the
     friend. If so, the friend will likely be recreated later! Therefore don't
     remove friends, where you didn't also leave the 1:1 room with them!

     - parameter matrixId: The Matrix ID of the friend.
     - returns: `true` if friend was in list and removed, `false` if nothing changed.
    */
    func remove(_ matrixId: String) -> Bool {
        if let friend = get(matrixId) {
            return remove(friend)
        }
        
        return false
    }
    
    /**
     Removes a friend from the friends list, *if* in it.
     
     Re-sorts and persists the list and notifies change listeners.
     
     ATTENTION: This doesn't care, if the user still has a 1:1 room with the
     friend. If so, the friend will likely be recreated later! Therefore don't
     remove friends, where you didn't also leave the 1:1 room with them!
     
     - parameter friend: The friend.
     - returns: `true` if friend was in list and removed, `false` if nothing changed.
     */
    public func remove(_ friend: Friend) -> Bool {
        if friends.contains(friend) {
            friends.remove(friend)

            // Persist right away in background.
            store(async: true)

            notifyChange()

            return true
        }

        return false
    }

    /**
     Re-sorts the friends list ascending by (friendly) name.
     */
    public func sort() {
        sortedFriends.removeAll(keepingCapacity: true)

        for friend in friends {
            let idx = String(friend.name.uppercased().first ?? Character(""))

            if var list = sortedFriends[idx] {
                list.append(friend)
                list.sort(by: <)

                sortedFriends[idx] = list
            }
            else {
                sortedFriends[idx] = [friend]
            }
        }

        sortedIndex = sortedFriends.keys.sorted(by: <)
    }
}
