//
//  Friend.swift
//  Keanu
//
//  Created by Benjamin Erhart on 29.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK
import Regex

/**
 A `Friend` is a contact, who the user of the app considers a friend.
 
 The friend is uniquely identified by their Matrix ID.
 
 It is mainly a proxy object to `MXUser` (and its Keanu extension), but can
 live without one to offer basic functionality.
 
 Therefore a friend can be added without having to open a 1:1 room with another
 user first.

 A real-world friend could actually exist multiple times in here, if
 they use multiple Matrix IDs. However, since there's no way of identifying this
 situation without the use of "Third-Party IDs", and 3PIDs are not easily
 resolved when only having a Matrix ID, we avoid adding this complexity
 like `MXKContactsManager` does.
 
 It is planned to add address book support, in which case, a `Friend` object will
 also keep a reference to that.
 */
public class Friend: NSObject, NSCoding {

    /**
     A regular expression to extract the local part of a `matrixId`.
     */
    private static let localPartRegex = "^@(.*?):".r

    /**
     - parameter name: The name of a user or friend. Can be nil or empty.
     - parameter matrixId: The Matrix ID of a user or friend.
     - returns: the `name` if not empty or the local part of the `matrixId` or the `matrixId` if the regex does't work.
    */
    public class func getFriendlyName(_ name: String?, _ matrixId: String) -> String {
        if let name = name, !name.isEmpty {
            return name
        }

        return Friend.localPartRegex?.findFirst(in: matrixId)?.group(at: 1)
            ?? matrixId
    }
    

    /**
     The unique identifier.
    */
    public let matrixId: String
    
    /**
     A working `MXSession` object which can resolve the friend's Matrix ID to
     a `MXUser`. If not available, usability of the proxy properties is degraded.
     
     Keep its reference weak, so we don't accidentally inhibit the destruction.
    */
    public weak var session: MXSession?

    /**
     Cache MXUser object, but only weakly, to get a fresh one, if the session
     updates.
    */
    weak private var _user: MXUser?
    
    /**
     Proxy property to the adequate `MXUser` object. `nil` if no `session`
     and no `privateRoom` is set.
    */
    public var user: MXUser? {
        get {
            if _user == nil {
                _user = (session ?? privateRoom?.mxSession)?.getOrCreateUser(matrixId)
            }
            
            return _user
        }
        set {
            // Allow reset but no injection of foreign user objects.
            if newValue == nil || newValue?.userId == matrixId {
                _user = newValue
            }
        }
    }
    
    /**
     Caches and stores our own, local, name for the friend, so the user can give
     their own (more memorable/better known) name to the friend.
     
     Also useful in cases, where the user added a friend, but didn't start a
     1:1 room with them, yet.
    */
    private var _name: String?

    /**
     A friendly name which normally should show `MXUser#displayname` but has
     several fallbacks to generate something meaningful, if that is not available.
    */
    public var name: String {
        get {
            return _name ?? Friend.getFriendlyName(user?.displayname, matrixId)
        }
        set {
            let newName = newValue.trimmingCharacters(in: .whitespacesAndNewlines)

            if newName.isEmpty || newName == user?.friendlyName || newValue == user?.friendlyName {
                _name = nil
            }
            else {
                _name = newName

                if isStored {
                    FriendsManager.shared.store(async: true)
                }
            }
        }
    }
    
    /**
     Caches and stores our own, local, avatar URL for the friend, so we can
     show an avatar for friends, which don't have a `MXUser` object, yet,
     because the user added the friend, but didn't start a 1:1 room with them, yet.
     */
    private var _avatarUrl: String?

    /**
     Proxy to `MXUser#avatarUrl`.
    */
    public var avatarUrl: String? {
        get {
            return _avatarUrl ?? user?.avatarUrl
        }
        set {


            if newValue?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ?? false
                || newValue == user?.avatarUrl {

                _avatarUrl = nil
            }
            else {
                _avatarUrl = newValue

                if isStored {
                    FriendsManager.shared.store(async: true)
                }
            }
        }
    }
    
    /**
     Proxy to `MXUser#presence`.
    */
    public var presence: MXPresence {
        get {
            return user?.presence ?? MXPresenceUnknown
        }
    }
    
    /**
     Proxy to `MXuser#friendlyPresence`.
    */
    public var friendlyPresence: String {
        get {
            return user?.friendlyPresence ?? "presence unclear".localize()
        }
    }
    
    /**
     Proxy to `MXUser#status`.
    */
    public var status: String? {
        get {
            return user?.statusMsg
        }
    }
    
    /**
     Proxy to `MXUser#lastActiveAgo`.
    */
    public var lastActiveAgo: UInt {
        get {
            return user?.lastActiveAgo ?? UInt.max
        }
    }
    
    /**
     Localized, human readable representation of `lastActiveAgo`.
     */
    public var friendlyLastActiveAgo: String {
        get {
            return user?.friendlyLastActiveAgo ?? "years".localize()
        }
    }
    
    /**
     Proxy to `MXUser#currentlyActive`.
    */
    public var currentlyActive: Bool {
        get {
            return user?.currentlyActive ?? false
        }
    }

    /**
     The 1:1 room with the user.

     This property is not computed!
     The `FriendsManager` shall update this, when a change happens.
     */
    public weak var privateRoom: MXRoom?

    /**
     The list of devices belonging to this friend.
    */
    public var devices: [MXDeviceInfo] {
        get {
            if let devices = session?.crypto?.store?.devices(forUser: matrixId) {
                return devices.map() { $1 }
            }

            return []
        }
    }

    /**
     True, if object is persisted, false if not.
    */
    public var isStored: Bool {
        get {
            return FriendsManager.shared.friends.contains(self)
        }
    }

    override public var debugDescription: String {
        return "matrixId=\(matrixId), name=\(name), avatarUrl=\(avatarUrl ?? "nil") session=\(session?.debugDescription ?? "nil")"
    }
    
    /**
     Create a Friend object from a Matrix ID and a session.

     - parameter matrixId: The globally unique Matrix ID of the friend.
     - parameter session: A session, where the matching `MXUser` object can be found. Optional.
     */
    public init(_ matrixId: String, _ session: MXSession? = nil) {
        self.matrixId = matrixId
        self.session = session
    }

    /**
     Create a Friend object from a Matrix ID and a room.

     - parameter matrixId: The globally unique Matrix ID of the friend.
     - parameter privateRoom: The 1:1 room with the friend. Optional.
     */
    public init(_ matrixId: String, privateRoom: MXRoom? = nil) {
        self.matrixId = matrixId
        self.privateRoom = privateRoom
        self.session = privateRoom?.mxSession
    }

    /**
     Convenience initializer which extracts the Matrix ID from the
     `MXRoomMember` object.
     
     - parameter roomMember: A `MXRoomMember` object.
     - parameter privateRoom: The 1:1 room with the friend.
     */
    public convenience init(_ roomMember: MXRoomMember, privateRoom: MXRoom? = nil) {
        self.init(roomMember.userId, privateRoom: privateRoom)
    }
    
    /**
     Create a Friend object from a Matrix ID, and optionally a name and an
     avatar URL.

     - parameter matrixId: The globally unique Matrix ID of the friend.
     - parameter name: The (friendly) name of the friend. Optional. Will fall
        back to a matching `MXUser` object, after a session containing that
        `MXUser` is attached, or the local part of the Matrix ID.
     - parameter avatarUrl: The avatar URL. Optional. Will fall
        back to a matching `MXUser` object, after a session containing that
        `MXUser` is attached.
    */
    public init(_ matrixId: String, _ name: String? = nil, _ avatarUrl: String? = nil) {
        self.matrixId = matrixId
        _name = name
        _avatarUrl = avatarUrl
    }
    
    
    // MARK: NSCoding
    
    required public init?(coder aDecoder: NSCoder) {
        matrixId = aDecoder.decodeObject(forKey: "matrixId") as! String
        _name = aDecoder.decodeObject(forKey: "name") as? String
        _avatarUrl = aDecoder.decodeObject(forKey: "avatarUrl") as? String
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(matrixId, forKey: "matrixId")
        aCoder.encode(_name, forKey: "name")
        aCoder.encode(_avatarUrl, forKey: "avatarUrl")
    }


    // MARK: Device Handling

    /**
     Force loading all device infos from the Matrix server.

     - parameter callback: Callback receiving the latest `MXDeviceInfo`s for
        this friend. Will be equal to `#devices` if an error happens.
    */
    public func loadDevices(_ callback: @escaping ([MXDeviceInfo]) -> ()) {
        session?.crypto?.downloadKeys([matrixId], forceDownload: true, success: { [weak self] map, crossSigningInfo in
            guard let matrixId = self?.matrixId else {
                return
            }

            var devices = [MXDeviceInfo]()

            for deviceId in map?.deviceIds(forUser: matrixId) ?? [] {
                if let device = map?.object(forDevice: deviceId, forUser: matrixId) {
                    devices.append(device)
                }
            }

            callback(devices)
        }, failure: { [weak self] error in
            if let devices = self?.devices {
                callback(devices)
            }
        })
    }

    /**
     Store a given `MXDeviceInfo` for this friend.

     - parameter device: The `MXDeviceInfo` to store.
    */
    public func storeDevice(_ device: MXDeviceInfo) {
        session?.crypto?.store?.storeDevice(forUser: matrixId, device: device)
    }

    
    // MARK: Equatable
    
    /**
     Friend objects are equal, when their Matrix ID is equal.
    */
    public static func == (lhs: Friend, rhs: Friend) -> Bool {
        return lhs.matrixId == rhs.matrixId
    }
    
    /**
     Ordering/sorting is done by the (friendly) name.
    */
    public static func < (lhs: Friend, rhs: Friend) -> Bool {
        return lhs.name.localizedCompare(rhs.name) == .orderedAscending
    }
    
    /**
     Ordering/sorting is done by the (friendly) name.
     */
    public static func > (lhs: Friend, rhs: Friend) -> Bool {
        return lhs.name.localizedCompare(rhs.name) == .orderedDescending
    }


    // MARK: NSObjectProtocol

    override public func isEqual(_ object: Any?) -> Bool {
        if let o = object as? Friend {
            return matrixId.isEqual(o.matrixId)
        }

        return false
    }

    override public var hash: Int {
        return matrixId.hash
    }
}
