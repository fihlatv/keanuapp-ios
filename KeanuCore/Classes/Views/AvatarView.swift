//
//  AvatarView.swift
//  Keanu
//
//  Created by N-Pex on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 A custom UIImageView for avatars. Handles loading (and caching) of avatar
 images from the Matrix server if needed.
 */
open class AvatarView: UIImageView {

    /**
     Avatars will be displayed as circles by default.

     Set to false *before* view gets layouted to disable this.
    */
    open var rounded = true

    private var _size: CGFloat?

    /**
     Set this value *before* calling `load` to enforce a certain size.
     Otherwise, the bigger of `frame.width` or 44 will be used.
    */
    open var size: CGFloat {
        get {
            return _size ?? max(frame.width, 44)
        }
        set {
            _size = newValue
        }
    }

    /**
     A default image to show, when all other means fail.

     TODO: This needs implementation.
     */
    private static let defaultImage: UIImage? = {
        return nil
    }()

    private var id: String?
    private var displayName: String?
    private var isForRoom = false

    open override func layoutSubviews() {
        super.layoutSubviews()

        if rounded && layer.cornerRadius != bounds.width / 2 {
            layer.cornerRadius = bounds.width / 2
            clipsToBounds = true
            layer.masksToBounds = true
            setNeedsLayout()
        }
    }
    
    /**
     Load an avatar for a room. If already cached, just use that. If no avatar
     url is given, try to generate one from the room name.

     If the argument is nil, use a default image.

     - parameter room: The `MXRoomSummary` for a room.
     */
    open func load(for room: MXRoomSummary?) {
        guard let room = room else {
            image = AvatarView.defaultImage
            return
        }

        load(session: room.mxSession, id: room.roomId, avatarUrl: room.avatar,
             displayName: room.displayname, isForRoom: true)
    }
    
    /**
     Load an avatar for a friend. If already cached, just use that. If no avatar
     url is given, try to generate one from the friend name.

     If no argument given or the argument is nil, use a default image.

     - parameter friend: The `Friend`.
     */
    open func load(friend: Friend? = nil) {
        guard let friend = friend else {
            image = AvatarView.defaultImage
            return
        }

        load(session: friend.session, id: friend.matrixId, avatarUrl: friend.avatarUrl,
             displayName: friend.name, isForRoom: false)
    }

    /**
     Load an avatar for a user. If already cached, just use that. If no avatar
     url is given, try to generate one from the user name.

     If the user argument is nil, use a default image.

     - parameter user: The `MXUser`.
     - parameter session: A `MXSession`. Can be nil, but then no download is possible.
     */
    open func load(user: MXUser?, session: MXSession? = nil) {
        guard let user = user else {
            image = AvatarView.defaultImage
            return
        }

        load(session: session, id: user.userId, avatarUrl: user.avatarUrl,
             displayName: user.friendlyName, isForRoom: false)
    }

    /**
     Load an avatar for an account. If already cached, just use that. If no avatar
     url is given, try to generate one from the user name.

     If the user argument is nil, use a default image.

     - parameter account: The `MXKAccount`.
     */
    open func load(account: MXKAccount?) {
        if let account = account {
            load(session: account.mxSession, id: account.matrixId,
                 avatarUrl: account.userAvatarUrl,
                 displayName: account.friendlyName, isForRoom: false)
        }
        else {
            image = AvatarView.defaultImage
        }
    }


    /**
     Load an avatar from the given url, or else generate one from the given display name.

     - parameter session: The Matrix session from where to load the avatar, if given.
     - parameter id: Used for caching af any GENERATED avatars. Set to room ID or Friend Matrix ID, based on use case.
     - parameter avatarUrl: The Avatar url to load. If nil we generate an avatar from the display name.
     - parameter displayName: The display name to use for generated avatars.
     */
    open func load(session: MXSession?, id: String, avatarUrl: String?, displayName: String?) {
        load(session: session, id: id, avatarUrl: avatarUrl,
             displayName: displayName, isForRoom: false)
    }
    
    /**
     Load an avatar with the given url. If already cached, just use that.

     - parameter session: The Matrix session from where to load the avatar, if given.
     - parameter id: Used for caching af any GENERATED avatars. Set to room ID or Friend Matrix ID, based on use case.
     - parameter avatarUrl: The Avatar url to load. If nil we generate an avatar from the display name.
     - parameter displayName: The display name to use for generated avatars.
     - parameter isForRoom: true, if this is an avatar for a room.
     */
    open func load(session: MXSession?, id: String, avatarUrl: String?, displayName: String?, isForRoom: Bool) {

        self.id = id
        self.displayName = displayName
        self.isForRoom = isForRoom

        let viewSize = CGSize(width: size, height: size)
        
        // Nothing to load, bail
        if (avatarUrl == nil) {
            setDefaultImage()
            return
        }

        // Nothing downloaded currently. Try to load avatar from cache.
        let cacheFilePath = MXMediaManager.thumbnailCachePath(
            forMatrixContentURI: avatarUrl, andType: nil,
            inFolder: kMXMediaManagerAvatarThumbnailFolder,
            toFitViewSize: viewSize,
            with: MXThumbnailingMethodCrop)

        if let image = MXMediaManager.loadThroughCache(withFilePath: cacheFilePath) {
            self.image = image
            return
        }

        // Nothing there. Set default image meanwhile and then try to download real avatar.
        setDefaultImage()
        
        session?.mediaManager.downloadThumbnail(fromMatrixContentURI: avatarUrl, withType: nil, inFolder: kMXMediaManagerAvatarThumbnailFolder, toFitViewSize: viewSize, with: MXThumbnailingMethodCrop, success: { (path) in
            DispatchQueue.main.async {
                if let image = MXMediaManager.loadThroughCache(withFilePath: path) {
                    self.image = image
                } else {
                    self.setDefaultImage()
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                self.setDefaultImage()
            }
        })
    }

    /**
     Set a default image, if/while real avatar image cannot be found.

     Try to generate a placeholder from the `displayName`, if we have that.
     Generated avatars will be cached as well.
    */
    private func setDefaultImage() {
        if let id = id,
            let cachePathGeneratedAvatar = MXMediaManager.cachePathForGeneratedThumbnail(id: id, size: Int(size)) {
            if let cachedGeneratedImage = MXMediaManager.loadThroughCache(withFilePath: cachePathGeneratedAvatar) {
                image = cachedGeneratedImage
                return
            }

            if let displayName = displayName {
                if isForRoom {
                    if let image = AvatarGenerator.roomAvatarImage(
                        id: id, displayName: displayName, width: Int(size), height: Int(size)) {

                        self.image = image
                        MXMediaManager.cacheImage(image, withCachePath: cachePathGeneratedAvatar)

                        return
                    }
                }
                else {
                    if let image = AvatarGenerator.avatarImage(
                        displayName: displayName, width: Int(size), height: Int(size)) {

                        self.image = image
                        MXMediaManager.cacheImage(image, withCachePath: cachePathGeneratedAvatar)

                        return
                    }
                }
            }
        }

        image = AvatarView.defaultImage
    }
}
