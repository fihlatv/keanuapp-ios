//
//  Scrubber.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 12.03.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import UIKit

/**
 Helper to remove **all** user files for a fresh restart.

 If you want to use this after an upgrade, you could do the following:

 - Check a `UserDefaults` flag, if the upgrade was already done. (And set it aftewards!)
 - Check `Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String`
   or a similar entry.
 */
open class Scrubber: NSObject {

    /**
     All folders, an app can store data in.

     Should contain:

     - The app group root folder, if any configured.
     - The app's document directory.
     - The app's library directory.
     - The app's tmp directory.
    */
    public static var folders: [URL] = {
        var fm = FileManager.default
        var folders = [URL]()

        if let appGroupId = Config.appGroupId,
            let folder = fm.containerURL(forSecurityApplicationGroupIdentifier: appGroupId) {
            folders.append(folder)
        }

        folders.append(contentsOf: fm.urls(for: .documentDirectory, in: .userDomainMask))
        folders.append(contentsOf: fm.urls(for: .libraryDirectory, in: .userDomainMask))

        if #available(iOSApplicationExtension 10.0, *) {
            folders.append(fm.temporaryDirectory)
        } else {
            folders.append(URL(fileURLWithPath: NSTemporaryDirectory()))
        }

        return folders
    }()

    /**
     Helper to remove all data from all locations an iOS app can store stuff to.

     The root folders are kept intact, all containing subfolders and files are
     deleted.

     ATTENTION: Afterwards, the app will be squeaky clean similar to a fresh install!

     Don't have other code interact with these files until it's done!
     (E.g. don't read user defaults, initialize databases, etc.)
     Otherwise you could end up with a crashed app or old files which couldn't
     be removed.

     Errors will be silently ignored.
    */
    public class func scrub() {
        let fm = FileManager.default

        for folder in folders {
            for item in (try? fm.contentsOfDirectory(
                at: folder, includingPropertiesForKeys: nil,
                options: .skipsHiddenFiles)) ?? [] {

                // Don't delete hidden files, esp. not a certain file named
                // `.com.apple.mobile_container_manager.metadata.plist`.
                // iOS will delete all our storage, if we do.

                print("[\(String(describing: self))] try removing \(item.path)")
                try? fm.removeItem(at: item)
            }
        }
    }
}
