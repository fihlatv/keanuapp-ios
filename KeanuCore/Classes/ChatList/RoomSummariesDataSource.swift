//
//  RoomSummariesDataSource.swift
//  Keanu
//
//  Created by N-Pex on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import MatrixSDK

/**
 Data source for all room summaries.
 
 Basically this handles an array of MXRoomSummary objects. The data source handles all
 underlying logic of listening to MX session changes, room adds/removes etc.
 
 Use the delegate to customize cell rendering and table view animations.
 */
open class RoomSummariesDataSource: AggregatedRoomSummaries, UITableViewDataSource {
    
    public var delegate: RoomSummariesDataSourceDelegate?
    
    open override func didAddRoomSummary(_ roomSummary: MXRoomSummary) {
        resort()
        delegate?.didChangeAllRows()
    }

    open override func didRemoveRoomSummary(_ roomSummary: MXRoomSummary) {
        resort()
        delegate?.didChangeAllRows()
    }

    open override func didChangeRoomSummary(_ roomSummary: MXRoomSummary) {
        resort()
        delegate?.didChangeAllRows()
    }
    
    open override func didChangeAllRoomSummaries() {
        resort()
        delegate?.didChangeAllRows()
    }
    
    /**
     For all sessions, order invites at the top, and otherwise by last message's timestamp.
     */
    open func resort() {
        roomSummaries.sort { (room1, room2) -> Bool in
            if room1.isInvite, !room2.isInvite {
                return true
            } else if !room1.isInvite, room2.isInvite {
                return false
            }
            return room1.lastMessageOriginServerTs > room2.lastMessageOriginServerTs
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomSummaries.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let delegate = self.delegate, let cell = delegate.createCell(at: indexPath, for: roomSummaries[indexPath.row]) {
            return cell
        }
        return UITableViewCell() // Fallback to avoid crash
    }
    
    public func roomSummary(at indexPath: IndexPath) -> MXRoomSummary? {
        if indexPath.section == 0, indexPath.row >= 0, indexPath.row < roomSummaries.count {
            return roomSummaries[indexPath.row]
        }
        return nil
    }
}


